#!/usr/bin/env python
# coding: utf-8

# In[1]:


import yaml
import os
import datetime
import csv
import numpy as np
import statistics 

import math
import pickle
import joblib
import statsmodels.api as sm
import joblib


# In[2]:


filenames=os.listdir('ipl')
filedataofThismatch =yaml.safe_load(open('ipl/1254078.yaml','r'))


# In[3]:


filedataofThismatch


# In[4]:


filenames=os.listdir('ipl')
per_match_info=[]
for f in filenames:
    if f[-5:]=='.yaml':
        filedataofThismatch =yaml.safe_load(open('ipl/'+f,'r'))
        
        matchNo=f
        toss=filedataofThismatch['info']['toss']
        teamA=filedataofThismatch['info']['teams'][0]
        teamB=filedataofThismatch['info']['teams'][1]
        venue=filedataofThismatch['info']['venue']
        result=filedataofThismatch['info']['outcome']
        date=filedataofThismatch['info']['dates'][0]
        per_ball_info=[]
        for i in range(len(filedataofThismatch['innings'])):
            if i<=1:
                innings_key=list(filedataofThismatch['innings'][i].keys())[0]
                team_batting_first=filedataofThismatch['innings'][0]['1st innings']['team']
                for j in filedataofThismatch['innings'][i][innings_key]['deliveries']:
                    balls_key=list(j.keys())[0]

                    if 'extras' not in list(j[balls_key].keys())                    and 'wicket' not in list(j[balls_key].keys()):
                        per_ball_info.append([innings_key,balls_key,j[balls_key]['non_striker'],j[balls_key]['bowler'],                                              j[balls_key]['batsman'],j[balls_key]['runs']['total'],j[balls_key]['runs']['batsman'],                                              'na','na'])

                    elif 'extras' in list(j[balls_key].keys()):
                        per_ball_info.append([innings_key,balls_key,j[balls_key]['non_striker'],j[balls_key]['bowler'],                                              j[balls_key]['batsman'],j[balls_key]['runs']['total'],j[balls_key]['runs']['batsman'],                                              j[balls_key]['extras'],'na'])

                    elif 'wicket' in list(j[balls_key].keys()):

                        per_ball_info.append([innings_key,balls_key,j[balls_key]['non_striker'],j[balls_key]['bowler'],                                              j[balls_key]['batsman'],j[balls_key]['runs']['total'],j[balls_key]['runs']['batsman'],                                              'na',j[balls_key]['wicket']])

                    elif 'extras' in list(j[balls_key].keys())                    and 'wicket' in list(j[balls_key].keys()):

                        per_ball_info.append([innings_key,balls_key,j[balls_key]['non_striker'],j[balls_key]['bowler'],                              j[balls_key]['batsman'],j[balls_key]['runs']['total'],j[balls_key]['runs']['batsman'],                              j[balls_key]['extras'],j[balls_key]['wicket']])


        per_match_info.append([matchNo,toss,teamA,teamB,team_batting_first,result,venue,date])
          
    
        filecursor=open('csv_of_all_matches/'+f+'.csv','w',newline='')
        csvWriter=csv.writer(filecursor)
        csvWriter.writerow(['innings','deliveries','non_striker','bowler','batsman','total_runs','batsman_runs','extras','wickets'])
        for i in per_ball_info:
            csvWriter.writerow([j for j in i])
        filecursor.close()


# In[5]:


filecursor=open('all_match_summary.csv','w',newline='')
csvWriter=csv.writer(filecursor)
csvWriter.writerow(['matchNo','tosswinner','toss_winner_decision','teamA','teamB','winner','winner_by_runs','winner_by_wickets','tie_yes','tie_no','venue','date'])
for i in per_match_info:
    csvWriter.writerow([j for j in i])
filecursor.close()
    

