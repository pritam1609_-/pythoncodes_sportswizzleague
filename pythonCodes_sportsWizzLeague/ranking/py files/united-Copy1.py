#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pickle
import yaml
import os
import datetime
import numpy as np
from IPython.core.display import display, HTML
display(HTML("<style>.container { width:100% !important; }</style>"))


# In[2]:


with open('data_Of_allMatches.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    allmatchInfo = unpickler.load() 
    file.close()


# In[3]:


with open('teamList_allMatches.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    allmatchteamlist = unpickler.load() 
    file.close()


# First Index

# In[4]:


matches=[]

for i in allmatchInfo:
    

    
    
    if isinstance(i['info']['dates'][0],str):
        thisMatchDate=datetime.datetime.strptime(i['info']['dates'][0], '%Y-%m-%d')
    else:
        thisMatchDate=datetime.datetime.strptime(                            datetime.datetime.strftime(i['info']['dates'][0], '%Y-%m-%d'),                            '%Y-%m-%d')
    if thisMatchDate.year>=2017 and thisMatchDate.year<=2020:
        thismatchdata={}
        thismatchdata[i['matchId']]={}
        for j in range(len(i['innings'])):
            ballIndex=120
            inningstotal=0
            wickets=10
            batsmaninthisinng=[]
            if j<=1:
                innings_key=list(i['innings'][j].keys())[0]
                thismatchdata[i['matchId']][innings_key]={}
                for balls in i['innings'][j][innings_key]['deliveries']:
                    
                    balls_key=list(balls.keys())[0]
                    
                    
                    validBall=True
                    if 'extras' in list(balls[balls_key].keys()):
                        if 'wides' in list(balls[balls_key]['extras'].keys()) or                         'noballs' in list(balls[balls_key]['extras'].keys()):
                            validBall=False
                                
                    if validBall:
                        ballIndex=ballIndex-1
                        
                    inningstotal+=(balls[balls_key]['runs']['batsman']+balls[balls_key]['runs']['extras'])
                     
                    thismatchdata[i['matchId']][innings_key][ballIndex]={}
                    thismatchdata[i['matchId']][innings_key][ballIndex]['score']=inningstotal
                    
                    if validBall:
                        thismatchdata[i['matchId']][innings_key][ballIndex]['thisballrun']=balls[balls_key]['runs']['batsman']
                    elif 'extras' in list(balls[balls_key].keys()):
                        if 'legbyes' not in list(balls[balls_key]['extras'].keys()) or                         'byes' not in list(balls[balls_key]['extras'].keys()):
                            thismatchdata[i['matchId']][innings_key][ballIndex]['thisballrun']=(balls[balls_key]['runs']['batsman']+balls[balls_key]['runs']['extras'])
                    bowler=balls[balls_key]['bowler']
                    batsman=balls[balls_key]['batsman']
 
                    if 'wicket' in list(balls[balls_key].keys()):
                        
                       wickets=wickets-1
                       if balls[balls_key]['wicket']['kind']!='runout':
                            thismatchdata[i['matchId']][innings_key][ballIndex]['bowlerwicket']='yes'
                    else:
                        thismatchdata[i['matchId']][innings_key][ballIndex]['bowlerwicket']='no'
                    thismatchdata[i['matchId']][innings_key][ballIndex]['Rem_wicket']=wickets
                    thismatchdata[i['matchId']][innings_key][ballIndex]['bowler']=bowler

                    if batsman not in batsmaninthisinng:
                        batsmaninthisinng.append(batsman)
                        
 
                        
                    thismatchdata[i['matchId']][innings_key][ballIndex]['batting_order_of_batsman']=batsmaninthisinng.index(batsman)+1
                    
        matches.append(thismatchdata)      
                    


# In[5]:


import joblib

fitModel=joblib.load('../prediction/ols1stInngs.z')

def getWickRemCategorical(w):
    x=[0]*11
    x[w]=1
    return x
def get_OLS_ballByBallDict_catWickRem(ballRem=119,wicketRem=10):
    x=getWickRemCategorical(wicketRem)
    x.insert(0,1)
    return round(fitModel[ballRem].predict(x)[0],0)

winLossDistribution1stInngs=joblib.load('../prediction/winLossDistribution1stInngs.z')
winDistribution,lossDistribution=winLossDistribution1stInngs[0],winLossDistribution1stInngs[1]

def getWinProb(run):
    return round((winDistribution.pdf(run)/(winDistribution.pdf(run)+lossDistribution.pdf(run)))*100,1)


# Death Overs

# In[6]:



deathoverslist=[]
for i in matches:
    thismatchdata={}
    match_key=list(i.keys())[0]
    thismatchdata[match_key]={}
    for innings in list(i[match_key].keys()):
        
        thismatchdata[match_key][innings]={}
        thismatchdata[match_key][innings]['death_overs_ballbyball']={}
        balls_key=list(i[match_key][innings].keys())
        for balls in balls_key:
            if balls==24:
                runs_before_death_overs=i[match_key][innings][balls]['score']
                Rem_wickets_before_death_overs=i[match_key][innings][balls]['Rem_wicket']
                
            if balls==min(balls_key):
                runs_after_death_overs=i[match_key][innings][balls]['score']
                Rem_wickets_after_death_overs=i[match_key][innings][balls]['Rem_wicket']
                
            
                
            bowler=i[match_key][innings][balls]['bowler']
            score=i[match_key][innings][balls]['score']
            Rem_wicket=i[match_key][innings][balls]['Rem_wicket']
            batting_order=i[match_key][innings][balls]['batting_order_of_batsman']
            thisballrun=i[match_key][innings][balls]['thisballrun']
            if balls == 24:
                before_death_winProb_1stBatTeam=getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=24,wicketRem=Rem_wicket))
            if min(balls_key)<24:
                if balls<24 and balls>=min(balls_key):
                    thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]={}
                    thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['runs']=score
                    thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['thisballrun']=thisballrun
                    thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['Rem_wkts']=Rem_wicket
                    thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['bowler']=bowler
                    thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['batting_order_bowled']=batting_order
                    if i[match_key][innings][balls]['bowlerwicket']=='yes':
                        thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['bowlerwicket']='yes'
                    else:
                        thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['bowlerwicket']='no'
                    thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['winProb_1stBatTeam']=                                                        getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=balls,wicketRem=Rem_wicket))
                    thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['before_death_winProb_1stBatTeam']=before_death_winProb_1stBatTeam
                    if balls==23:
                        thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']=                        round((before_death_winProb_1stBatTeam-getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=23,wicketRem=Rem_wicket))),2)
                        if thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']>=0:
                            thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                            round(((before_death_winProb_1stBatTeam-                                    getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=23,wicketRem=Rem_wicket)))*before_death_winProb_1stBatTeam),2)
                            
                        else:
                            thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                            round(((before_death_winProb_1stBatTeam-                                    getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=23,wicketRem=Rem_wicket)))*(100-before_death_winProb_1stBatTeam)),2)
                    else:
                        thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']=                            round((getWinProb(i[match_key][innings][balls+1]['score']+                                          get_OLS_ballByBallDict_catWickRem(ballRem=balls+1,wicketRem=i[match_key][innings][balls+1]['Rem_wicket']))-                                           getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=balls,wicketRem=Rem_wicket))),2)
                        if thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']>=0:
                            thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                                thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']*                                thismatchdata[match_key][innings]['death_overs_ballbyball'][balls+1]['winProb_1stBatTeam']
                        else:
                            thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                                thismatchdata[match_key][innings]['death_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']*                                (100-thismatchdata[match_key][innings]['death_overs_ballbyball'][balls+1]['winProb_1stBatTeam'])
                            
                            
#                             round(((getWinProb(i[match_key][innings][balls+1]['score']+\
#                                            get_OLS_ballByBallDict_catWickRem(ballRem=balls+1,wicketRem=i[match_key][innings][balls+1]['Rem_wicket']))-\
#                                 getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=balls,wicketRem=Rem_wicket)))*((getWinProb(i[match_key][innings][balls+1]['score']+\
#                         get_OLS_ballByBallDict_catWickRem(ballRem=balls+1,wicketRem=i[match_key][innings][balls+1]['Rem_wicket'])))),2)
            else:
                thismatchdata[match_key][innings]['death_overs_ballbyball']='inngs over before 16th over'
            if balls==24:
                thismatchdata[match_key][innings]['run_rate_before_deathovers']=round((runs_before_death_overs/96),2)
        
        if min(balls_key)<24:
            if (24-min(balls_key))!=0:
                thismatchdata[match_key][innings]['run_rate_in_deathovers']=round((runs_after_death_overs-runs_before_death_overs)/(24-min(balls_key)),2)
        else:
            thismatchdata[match_key][innings]['run_rate_in_deathovers']='NA'
            
        if min(balls_key)<24:
            if (Rem_wickets_before_death_overs-Rem_wickets_after_death_overs)!=0:
                thismatchdata[match_key][innings]['runs_per_wicket_during_death']=round(((runs_after_death_overs-runs_before_death_overs)/                                                                                         (Rem_wickets_before_death_overs-Rem_wickets_after_death_overs)),2)
            else:
                thismatchdata[match_key][innings]['runs_per_wicket_during_death']='NA'
        else:
            thismatchdata[match_key][innings]['runs_per_wicket_during_death']='NA'
            
    deathoverslist.append(thismatchdata)


# In[7]:


#deathoverslist


# In[8]:


for i in deathoverslist:
    for j in list(i.keys()) :
        innings=list(i[j].keys())
#        print(i[j][innings[0]]['run_rate_before_deathovers'],j)
#        if j=='1082644.yaml':
#            print(i['1082646.yaml'])
            


# In[9]:


battingorderseverity={}
battingorderseverity['top_order']={}
battingorderseverity['top_order']['balls']=0
battingorderseverity['top_order']['runs']=0
battingorderseverity['top_order']['dot_balls']=0
battingorderseverity['top_order']['wickets']=0
battingorderseverity['middle_order']={}
battingorderseverity['middle_order']['balls']=0
battingorderseverity['middle_order']['runs']=0
battingorderseverity['middle_order']['dot_balls']=0
battingorderseverity['middle_order']['wickets']=0
battingorderseverity['tail_ender']={}
battingorderseverity['tail_ender']['balls']=0
battingorderseverity['tail_ender']['runs']=0
battingorderseverity['tail_ender']['dot_balls']=0
battingorderseverity['tail_ender']['wickets']=0
for i in deathoverslist:
    match_key=list(i.keys())[0]
    for x in list(i[match_key].keys()):
        
        if i[match_key][x]['death_overs_ballbyball']=='inngs over before 16th over':
            continue
        balls_key=list(i[match_key][x]['death_overs_ballbyball'].keys())

        for balls in balls_key: 
            if i[match_key][x]['death_overs_ballbyball'][balls]['batting_order_bowled']<=3:
                battingorderseverity['top_order']['balls']+=1
                battingorderseverity['top_order']['runs']+=i[match_key][x]['death_overs_ballbyball'][balls]['thisballrun']
                battingorderseverity['top_order']['runs_per_ball']=round((battingorderseverity['top_order']['runs']/battingorderseverity['top_order']['balls']),2)
                if i[match_key][x]['death_overs_ballbyball'][balls]['thisballrun']==0:
                    battingorderseverity['top_order']['dot_balls']+=1
                if i[match_key][x]['death_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                    battingorderseverity['top_order']['wickets']+=1
            if i[match_key][x]['death_overs_ballbyball'][balls]['batting_order_bowled']>=4 and i[match_key][x]['death_overs_ballbyball'][balls]['batting_order_bowled']<=7:

                battingorderseverity['middle_order']['balls']+=1
                battingorderseverity['middle_order']['runs']+=i[match_key][x]['death_overs_ballbyball'][balls]['thisballrun']
                battingorderseverity['middle_order']['runs_per_ball']=round((battingorderseverity['middle_order']['runs']/battingorderseverity['middle_order']['balls']),2)
                if i[match_key][x]['death_overs_ballbyball'][balls]['thisballrun']==0:
                    battingorderseverity['middle_order']['dot_balls']+=1
                if i[match_key][x]['death_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                    battingorderseverity['middle_order']['wickets']+=1
            if i[match_key][x]['death_overs_ballbyball'][balls]['batting_order_bowled']>=8 and i[match_key][x]['death_overs_ballbyball'][balls]['batting_order_bowled']<=11:

                battingorderseverity['tail_ender']['balls']+=1
                battingorderseverity['tail_ender']['runs']+=i[match_key][x]['death_overs_ballbyball'][balls]['thisballrun']
                battingorderseverity['tail_ender']['runs_per_ball']=round((battingorderseverity['tail_ender']['runs']/battingorderseverity['tail_ender']['balls']),2)
                if i[match_key][x]['death_overs_ballbyball'][balls]['thisballrun']==0:
                    battingorderseverity['tail_ender']['dot_balls']+=1
                if i[match_key][x]['death_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                    battingorderseverity['tail_ender']['wickets']+=1
#print(battingorderseverity)
               


# In[10]:


battingorderseverity_ratio={}
totalruns_in_death_overs=battingorderseverity['top_order']['runs']+battingorderseverity['middle_order']['runs']+battingorderseverity['tail_ender']['runs']
totalballs_in_death_overs=battingorderseverity['top_order']['balls']+battingorderseverity['middle_order']['balls']+battingorderseverity['tail_ender']['balls']
totaruns_per_ball=round((totalruns_in_death_overs/totalballs_in_death_overs),2)
battingorderseverity_ratio['top_order']=round((battingorderseverity['top_order']['runs_per_ball']/totaruns_per_ball),2)
battingorderseverity_ratio['middle_order']=round((battingorderseverity['middle_order']['runs_per_ball']/totaruns_per_ball),2)
battingorderseverity_ratio['tail_ender']=round((battingorderseverity['tail_ender']['runs_per_ball']/totaruns_per_ball),2)

battingorderseverity_ratio['balls_per_dot_top_order']=round((battingorderseverity['top_order']['balls']/battingorderseverity['top_order']['dot_balls']),2)
battingorderseverity_ratio['balls_per_dot_middle_order']=round((battingorderseverity['middle_order']['balls']/battingorderseverity['middle_order']['dot_balls']),2)
battingorderseverity_ratio['balls_per_dot_tail_ender']=round((battingorderseverity['tail_ender']['balls']/battingorderseverity['tail_ender']['dot_balls']),2)

battingorderseverity_ratio['balls_per_wicket_top_order']=round((battingorderseverity['top_order']['balls']/battingorderseverity['top_order']['wickets']),2)
battingorderseverity_ratio['balls_per_wicket_middle_order']=round((battingorderseverity['middle_order']['balls']/battingorderseverity['middle_order']['wickets']),2)
battingorderseverity_ratio['balls_per_wicket_tail_ender']=round((battingorderseverity['tail_ender']['balls']/battingorderseverity['tail_ender']['wickets']),2)

total_dot_balls=battingorderseverity_ratio['balls_per_dot_top_order']+battingorderseverity_ratio['balls_per_dot_middle_order']+battingorderseverity_ratio['balls_per_dot_tail_ender']
total_balls_per_dot_in_death=round((totalballs_in_death_overs/total_dot_balls),2)
total_balls_per_wicket=battingorderseverity_ratio['balls_per_wicket_top_order']+battingorderseverity_ratio['balls_per_wicket_middle_order']+battingorderseverity_ratio['balls_per_wicket_tail_ender']
battingorderseverity_ratio['dot_ball_ratio_top_order']=round((battingorderseverity_ratio['balls_per_dot_top_order']/total_dot_balls),2)
battingorderseverity_ratio['dot_ball_ratio_middle_order']=round((battingorderseverity_ratio['balls_per_dot_middle_order']/total_dot_balls),2)
battingorderseverity_ratio['dot_ball_ratio_tailender']=round((battingorderseverity_ratio['balls_per_dot_tail_ender']/total_dot_balls),2)

battingorderseverity_ratio['bpw_ratio_top_order']=round((battingorderseverity_ratio['balls_per_wicket_top_order']/total_balls_per_wicket),2)
battingorderseverity_ratio['bpw_ratio_middle_order']=round((battingorderseverity_ratio['balls_per_wicket_middle_order']/total_balls_per_wicket),2)
battingorderseverity_ratio['bpw_ratio_tailender']=round((battingorderseverity_ratio['balls_per_wicket_tail_ender']/total_balls_per_wicket),2)


#print(battingorderseverity_ratio)


# In[11]:


bowlers_in_death_overs={}
for i in deathoverslist:
    match_keys=list(i.keys())[0]
    for x in list(i[match_keys].keys()):
        if i[match_keys][x]['death_overs_ballbyball']=='inngs over before 16th over':
            continue
        balls_key=list(i[match_keys][x]['death_overs_ballbyball'].keys())

        for balls in balls_key:

            bowler=i[match_keys][x]['death_overs_ballbyball'][balls]['bowler']
            if bowler not in list(bowlers_in_death_overs.keys()):
                bowlers_in_death_overs[bowler]={}
                bowlers_in_death_overs[bowler]['balls']=0
                bowlers_in_death_overs[bowler]['change_in_WP']=0
                bowlers_in_death_overs[bowler]['weightedchange_in_WP']=0

            for bowlers in list(bowlers_in_death_overs.keys()):
                if bowlers==bowler:
                    bowlers_in_death_overs[bowler]['balls']+=1
                    bowlers_in_death_overs[bowler]['change_in_WP']+=i[match_keys][x]['death_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']
                    bowlers_in_death_overs[bowler]['weightedchange_in_WP']+=i[match_keys][x]['death_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']
                    bowlers_in_death_overs[bowler]['change_in_WP_perball']=round((bowlers_in_death_overs[bowler]['change_in_WP']/bowlers_in_death_overs[bowler]['balls']),2)
                    bowlers_in_death_overs[bowler]['weightedchange_in_WP_perball']=round((bowlers_in_death_overs[bowler]['weightedchange_in_WP']/bowlers_in_death_overs[bowler]['balls']),2)
significantbowlers=[]
for x in list(bowlers_in_death_overs.keys()):
    if bowlers_in_death_overs[x]['balls']>=100:
        significantbowlers.append([x,bowlers_in_death_overs[x]['change_in_WP_perball'],(bowlers_in_death_overs[x]['weightedchange_in_WP_perball'])/100,bowlers_in_death_overs[x]['balls']])
        


# In[12]:


#significantbowlers


# In[13]:


filtered_significantbowlers=[i for i in significantbowlers if i[3]>=100]
#filtered_significantbowlers


# In[14]:


situationindex={}
for i in filtered_significantbowlers:
    situationindex[i[0]]={}
    situationindex[i[0]]['situation_rating']=i[2]
#print(situationindex)


# In[15]:


#pip install tabulate


# In[16]:


from tabulate import tabulate
filtered_significantbowlers.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(filtered_significantbowlers))


# In[17]:


filtered_significantbowlers.sort(key=lambda x:x[2],reverse=True)
#print(tabulate(filtered_significantbowlers))


# In[18]:


matches_for_runrate_drop_in_deathovers_infirstinnings=[]
matches_for_runrate_drop_in_deathovers_insecondinnings=[]
for i in deathoverslist:

    for j in list(i.keys()):
        innings_key=list(i[j].keys())
        if i[j][innings_key[0]]['run_rate_in_deathovers']!='NA':
            if i[j][innings_key[0]]['run_rate_before_deathovers']>i[j][innings_key[0]]['run_rate_in_deathovers']:
                matches_for_runrate_drop_in_deathovers_infirstinnings.append(i)
            
        if i[j][innings_key[1]]['run_rate_in_deathovers']!='NA':
            if i[j][innings_key[1]]['run_rate_before_deathovers']>i[j][innings_key[1]]['run_rate_in_deathovers']:
                matches_for_runrate_drop_in_deathovers_insecondinnings.append(i)
                
runratedrop={}
runratedrop['1st innings']=matches_for_runrate_drop_in_deathovers_infirstinnings
runratedrop['2nd innings']=matches_for_runrate_drop_in_deathovers_insecondinnings


# In[19]:


#len(matches_for_runrate_drop_in_deathovers_insecondinnings)


# In[20]:


#runratedrop


# In[21]:


bowlersdata={}

for i in runratedrop['1st innings']:
    match_key=list(i.keys())[0]
    for j in list(i[match_key]['1st innings']['death_overs_ballbyball'].keys()):
        bowler=i[match_key]['1st innings']['death_overs_ballbyball'][j]['bowler']
        if bowler not in list(bowlersdata.keys()):
            bowlersdata[bowler]={}
            bowlersdata[bowler]['balls']=0
            bowlersdata[bowler]['balls_against_top_order']=0
            bowlersdata[bowler]['balls_against_middle_order']=0
            bowlersdata[bowler]['balls_against_tailender']=0
            bowlersdata[bowler]['dotballs']=0
            bowlersdata[bowler]['dotballs_against_top_order']=0
            bowlersdata[bowler]['dotballs_against_middle_order']=0
            bowlersdata[bowler]['dotballs_against_tailender']=0
            
            bowlersdata[bowler]['runs']=0
            bowlersdata[bowler]['runsagainst_top_order']=0
            bowlersdata[bowler]['runsaginst_middle_order']=0
            bowlersdata[bowler]['runsagainst_tailender']=0
            bowlersdata[bowler]['wickets']=0
            bowlersdata[bowler]['wickets_toporder']=0
            bowlersdata[bowler]['wickets_middleorder']=0
            bowlersdata[bowler]['wickets_tailender']=0
            bowlersdata[bowler]['batting_order_dismissed']=[]
            
        for bowlers in list(bowlersdata.keys()):
            if bowlers==bowler:
                bowlersdata[bowler]['balls']+=1
                bowlersdata[bowler]['runs']+=i[match_key]['1st innings']['death_overs_ballbyball'][j]['thisballrun']
                if i[match_key]['1st innings']['death_overs_ballbyball'][j]['bowlerwicket']=='yes':
                    bowlersdata[bowler]['wickets']+=1
                    bowlersdata[bowler]['batting_order_dismissed'].append(i[match_key]['1st innings']['death_overs_ballbyball'][j]['batting_order_bowled'])
                if i[match_key]['1st innings']['death_overs_ballbyball'][j]['thisballrun']==0:
                    bowlersdata[bowler]['dotballs']+=1
                if i[match_key]['1st innings']['death_overs_ballbyball'][j]['batting_order_bowled']>=1                and i[match_key]['1st innings']['death_overs_ballbyball'][j]['batting_order_bowled']<=3:
                    bowlersdata[bowler]['balls_against_top_order']+=1
                    bowlersdata[bowler]['runsagainst_top_order']+=i[match_key]['1st innings']['death_overs_ballbyball'][j]['thisballrun']
                    if i[match_key]['1st innings']['death_overs_ballbyball'][j]['bowlerwicket']=='yes':
                        bowlersdata[bowler]['wickets_toporder']+=1
                    if i[match_key]['1st innings']['death_overs_ballbyball'][j]['thisballrun']==0:
                        bowlersdata[bowler]['dotballs_against_top_order']+=1
                        
                if i[match_key]['1st innings']['death_overs_ballbyball'][j]['batting_order_bowled']>=4                and i[match_key]['1st innings']['death_overs_ballbyball'][j]['batting_order_bowled']<=7:
                    bowlersdata[bowler]['balls_against_middle_order']+=1
                    bowlersdata[bowler]['runsaginst_middle_order']+=i[match_key]['1st innings']['death_overs_ballbyball'][j]['thisballrun']
                    if i[match_key]['1st innings']['death_overs_ballbyball'][j]['bowlerwicket']=='yes':
                        bowlersdata[bowler]['wickets_middleorder']+=1
                    if i[match_key]['1st innings']['death_overs_ballbyball'][j]['thisballrun']==0:
                        bowlersdata[bowler]['dotballs_against_middle_order']+=1
                        
                if i[match_key]['1st innings']['death_overs_ballbyball'][j]['batting_order_bowled']>=8                and i[match_key]['1st innings']['death_overs_ballbyball'][j]['batting_order_bowled']<=11:
                    bowlersdata[bowler]['balls_against_tailender']+=1
                    bowlersdata[bowler]['runsagainst_tailender']+=i[match_key]['1st innings']['death_overs_ballbyball'][j]['thisballrun']
                    if i[match_key]['1st innings']['death_overs_ballbyball'][j]['bowlerwicket']=='yes':
                        bowlersdata[bowler]['wickets_tailender']+=1
                    if i[match_key]['1st innings']['death_overs_ballbyball'][j]['thisballrun']==0:
                        bowlersdata[bowler]['dotballs_against_tailender']+=1
#print(bowlersdata)


# In[22]:


indexbowler={}
for bowler in list(bowlersdata.keys()):
    
    if bowlersdata[bowler]['balls']>=60:
        indexbowler[bowler]={}
        indexbowler[bowler]=bowlersdata[bowler]
#print(indexbowler)


# In[23]:


indexdata={}
for bowlers in list(indexbowler.keys()):
    indexdata[bowlers]={}
    indexdata[bowlers]['runs_per_ball']={}
    indexdata[bowlers]['runs_per_ball']['overall']=round((indexbowler[bowlers]['runs']/indexbowler[bowlers]['balls']),2)
    if indexbowler[bowlers]['balls_against_top_order']!=0:
        indexdata[bowlers]['runs_per_ball']['v_toporder']=round((indexbowler[bowlers]['runsagainst_top_order']/indexbowler[bowlers]['balls_against_top_order']),2)
    else:
        indexdata[bowlers]['runs_per_ball']['v_toporder']='NA'
    
    if indexbowler[bowlers]['balls_against_middle_order']!=0:
        indexdata[bowlers]['runs_per_ball']['v_middleorder']=round((indexbowler[bowlers]['runsaginst_middle_order']/indexbowler[bowlers]['balls_against_middle_order']),2)
    else:
        indexdata[bowlers]['runs_per_ball']['v_middleorder']='NA'
        
    if indexbowler[bowlers]['balls_against_tailender']!=0:
        indexdata[bowlers]['runs_per_ball']['v_tailender']=round((indexbowler[bowlers]['runsagainst_tailender']/indexbowler[bowlers]['balls_against_tailender']),2)
    else:
        indexdata[bowlers]['runs_per_ball']['v_tailender']='NA'
        
    indexdata[bowlers]['wickets']={}
    indexdata[bowlers]['wickets']['overall']=indexbowler[bowlers]['wickets']
    indexdata[bowlers]['wickets']['v_toporder']=indexbowler[bowlers]['wickets_toporder']
    indexdata[bowlers]['wickets']['v_middleorder']=indexbowler[bowlers]['wickets_middleorder']
    indexdata[bowlers]['wickets']['v_tailender']=indexbowler[bowlers]['wickets_tailender']
    
    indexdata[bowlers]['dotballs']={}
    indexdata[bowlers]['dotballs']['overall']=indexbowler[bowlers]['dotballs']
    indexdata[bowlers]['dotballs']['v_toporder']=indexbowler[bowlers]['dotballs_against_top_order']
    indexdata[bowlers]['dotballs']['v_middleorder']=indexbowler[bowlers]['dotballs_against_middle_order']
    indexdata[bowlers]['dotballs']['v_tailender']=indexbowler[bowlers]['dotballs_against_tailender']
    
#for x in list(indexdata.keys()):
    
#    print(x,indexdata[x])


# In[24]:


runs_per_ball_vtoporder=[]
runs_per_ball_vmiddleorder=[]
runs_per_ball_vtailender=[]
dotVtoporder=[]
dotVmiddleorder=[]
dotVtailender=[]

for x in list(indexdata.keys()):
    if indexdata[x]['runs_per_ball']['v_toporder']!='NA':
        runs_per_ball_vtoporder.append(indexdata[x]['runs_per_ball']['v_toporder'])
    if indexdata[x]['runs_per_ball']['v_middleorder']!='NA':
        runs_per_ball_vmiddleorder.append(indexdata[x]['runs_per_ball']['v_middleorder'])
    if indexdata[x]['runs_per_ball']['v_tailender']!='NA':
        runs_per_ball_vtailender.append(indexdata[x]['runs_per_ball']['v_tailender'])
        
    dotVtoporder.append(indexdata[x]['dotballs']['v_toporder'])
    dotVmiddleorder.append(indexdata[x]['dotballs']['v_middleorder'])
    dotVtailender.append(indexdata[x]['dotballs']['v_tailender'])
    
maximumTOR=max(runs_per_ball_vtoporder)
maximumMOR=max(runs_per_ball_vmiddleorder)
maximumTER=max(runs_per_ball_vtailender)

maximumdotTOR=max(dotVtoporder)
maximumdotMOR=max(dotVmiddleorder)
maximumdotTER=max(dotVtailender)

index1={}
for bowlers in list(indexdata.keys()):
    index1[bowlers]={}
    index1[bowlers]['rating_pts_wickets']=round((2/3)*(indexdata[bowlers]['wickets']['v_toporder'])+(1/3)*(indexdata[bowlers]['wickets']['v_middleorder'])+(1/3)*(indexdata[bowlers]['wickets']['v_tailender']),2)
    if indexdata[bowlers]['runs_per_ball']['v_toporder']=='NA':
        index1[bowlers]['rating_pts_runs']=round(((1/3)*(indexdata[bowlers]['runs_per_ball']['v_middleorder']/maximumMOR)+(2/3)*(indexdata[bowlers]['runs_per_ball']['v_tailender']/maximumTER)),2)
    else:
        index1[bowlers]['rating_pts_runs']=round(((1/3)*(indexdata[bowlers]['runs_per_ball']['v_toporder']/maximumTOR)+(1/3)*(indexdata[bowlers]['runs_per_ball']['v_middleorder']/maximumMOR)+(2/3)*(indexdata[bowlers]['runs_per_ball']['v_tailender']/maximumTER)),2)
    index1[bowlers]['rating_pts_dotballs']=round(((1/3)*(indexdata[bowlers]['dotballs']['v_toporder']/maximumdotTOR)+(1/3)*(indexdata[bowlers]['dotballs']['v_middleorder']/maximumdotMOR)+(1/3)*(indexdata[bowlers]['dotballs']['v_tailender']/maximumdotTER)),2)
    
    index1[bowlers]['totalrating']=round((index1[bowlers]['rating_pts_wickets']+index1[bowlers]['rating_pts_dotballs']-index1[bowlers]['rating_pts_runs']),2)
#print(index1)


# In[25]:


totalrating=[]
for bowlers in list(index1.keys()):
    totalrating.append([bowlers,index1[bowlers]['totalrating']])
    
totalrating.sort(key=lambda x:x[1],reverse=True)
#print(totalrating)


# In[26]:


matchdataofdeathoversfirstinnings=[]
matchdataofdeathoverssecondinnings=[]
for i in deathoverslist:

    for j in list(i.keys()):
        innings_key=list(i[j].keys())
        if i[j][innings_key[0]]['run_rate_in_deathovers']!='NA':
            
            matchdataofdeathoversfirstinnings.append(i)
            
        if i[j][innings_key[1]]['run_rate_in_deathovers']!='NA':
            
            matchdataofdeathoverssecondinnings.append(i)
                
matchdata={}
matchdata['1st innings']=matchdataofdeathoversfirstinnings
matchdata['2nd innings']=matchdataofdeathoverssecondinnings


# In[27]:


#matchdata


# In[28]:


bowlersdatawithoutrunratechange={}

for i in deathoverslist:
    match_key=list(i.keys())[0]
    for y in list(i[match_key].keys()):
        if i[match_key][y]['death_overs_ballbyball']=='inngs over before 16th over':
            continue
        balls_key=list(i[match_key][y]['death_overs_ballbyball'].keys())
        for balls in balls_key:
            bowler=i[match_key][y]['death_overs_ballbyball'][balls]['bowler']

            if bowler not in list(bowlersdatawithoutrunratechange.keys()):
                bowlersdatawithoutrunratechange[bowler]={}
                bowlersdatawithoutrunratechange[bowler]['balls']=0
                bowlersdatawithoutrunratechange[bowler]['balls_against_top_order']=0
                bowlersdatawithoutrunratechange[bowler]['balls_against_middle_order']=0
                bowlersdatawithoutrunratechange[bowler]['balls_against_tailender']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs_against_top_order']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs_against_middle_order']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs_against_tailender']=0

                bowlersdatawithoutrunratechange[bowler]['runs']=0
                bowlersdatawithoutrunratechange[bowler]['runsagainst_top_order']=0
                bowlersdatawithoutrunratechange[bowler]['runsaginst_middle_order']=0
                bowlersdatawithoutrunratechange[bowler]['runsagainst_tailender']=0
                bowlersdatawithoutrunratechange[bowler]['wickets']=0
                bowlersdatawithoutrunratechange[bowler]['wickets_toporder']=0
                bowlersdatawithoutrunratechange[bowler]['wickets_middleorder']=0
                bowlersdatawithoutrunratechange[bowler]['wickets_tailender']=0
                bowlersdatawithoutrunratechange[bowler]['batting_order_dismissed']=[]

            for bowlers in list(bowlersdatawithoutrunratechange.keys()):
                if bowlers==bowler:
                    bowlersdatawithoutrunratechange[bowler]['balls']+=1
                    bowlersdatawithoutrunratechange[bowler]['runs']+=i[match_key][y]['death_overs_ballbyball'][balls]['thisballrun']
                    if i[match_key][y]['death_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                        bowlersdatawithoutrunratechange[bowler]['wickets']+=1
                        bowlersdatawithoutrunratechange[bowler]['batting_order_dismissed'].append(i[match_key][y]['death_overs_ballbyball'][balls]['batting_order_bowled'])
                    if i[match_key][y]['death_overs_ballbyball'][balls]['thisballrun']==0:
                        bowlersdatawithoutrunratechange[bowler]['dotballs']+=1
                    if i[match_key][y]['death_overs_ballbyball'][balls]['batting_order_bowled']>=1                    and i[match_key][y]['death_overs_ballbyball'][balls]['batting_order_bowled']<=3:
                        bowlersdatawithoutrunratechange[bowler]['balls_against_top_order']+=1
                        bowlersdatawithoutrunratechange[bowler]['runsagainst_top_order']+=i[match_key][y]['death_overs_ballbyball'][balls]['thisballrun']
                        if i[match_key][y]['death_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                            bowlersdatawithoutrunratechange[bowler]['wickets_toporder']+=1
                        if i[match_key][y]['death_overs_ballbyball'][balls]['thisballrun']==0:
                            bowlersdatawithoutrunratechange[bowler]['dotballs_against_top_order']+=1

                    if i[match_key][y]['death_overs_ballbyball'][balls]['batting_order_bowled']>=4                    and i[match_key][y]['death_overs_ballbyball'][balls]['batting_order_bowled']<=7:
                        bowlersdatawithoutrunratechange[bowler]['balls_against_middle_order']+=1
                        bowlersdatawithoutrunratechange[bowler]['runsaginst_middle_order']+=i[match_key][y]['death_overs_ballbyball'][balls]['thisballrun']
                        if i[match_key][y]['death_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                            bowlersdatawithoutrunratechange[bowler]['wickets_middleorder']+=1
                        if i[match_key][y]['death_overs_ballbyball'][balls]['thisballrun']==0:
                            bowlersdatawithoutrunratechange[bowler]['dotballs_against_middle_order']+=1

                    if i[match_key][y]['death_overs_ballbyball'][balls]['batting_order_bowled']>=8                    and i[match_key][y]['death_overs_ballbyball'][balls]['batting_order_bowled']<=11:
                        bowlersdatawithoutrunratechange[bowler]['balls_against_tailender']+=1
                        bowlersdatawithoutrunratechange[bowler]['runsagainst_tailender']+=i[match_key][y]['death_overs_ballbyball'][balls]['thisballrun']
                        if i[match_key][y]['death_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                            bowlersdatawithoutrunratechange[bowler]['wickets_tailender']+=1
                        if i[match_key][y]['death_overs_ballbyball'][balls]['thisballrun']==0:
                            bowlersdatawithoutrunratechange[bowler]['dotballs_against_tailender']+=1
#print(bowlersdatawithoutrunratechange)


# In[29]:


totalballs_per_bowler=[]
for i in list(bowlersdatawithoutrunratechange.keys()):
    totalballs_per_bowler.append(bowlersdatawithoutrunratechange[i]['balls'])
totalballs_per_bowler.sort(reverse=True)
#print((totalballs_per_bowler))


# In[30]:


indexbowlerwithoutrunratechange={}
for bowler in list(bowlersdatawithoutrunratechange.keys()):
    
    if bowlersdatawithoutrunratechange[bowler]['balls']>=100:
        indexbowlerwithoutrunratechange[bowler]={}
        indexbowlerwithoutrunratechange[bowler]=bowlersdatawithoutrunratechange[bowler]
#print(indexbowlerwithoutrunratechange)


# In[31]:


totalballs_per_bowler=[]
totalballs_per_bowler_toporder=[]
totalballs_per_bowler_middleorder=[]
totalballs_per_bowler_tailender=[]
for i in list(indexbowlerwithoutrunratechange.keys()):
    totalballs_per_bowler.append(indexbowlerwithoutrunratechange[i]['balls'])
    totalballs_per_bowler_toporder.append(indexbowlerwithoutrunratechange[i]['balls_against_top_order'])
    totalballs_per_bowler_middleorder.append(indexbowlerwithoutrunratechange[i]['balls_against_middle_order'])
    totalballs_per_bowler_tailender.append(indexbowlerwithoutrunratechange[i]['balls_against_tailender'])
#print(np.mean(totalballs_per_bowler),np.mean(totalballs_per_bowler_toporder),np.mean(totalballs_per_bowler_middleorder),np.mean(totalballs_per_bowler_tailender))
#print(totalballs_per_bowler)


# In[32]:


bowlersballratio={}
total_to_balls=sum(totalballs_per_bowler_toporder)
#print(total_to_balls)
total_mo_balls=sum(totalballs_per_bowler_middleorder)
total_te_balls=sum(totalballs_per_bowler_tailender)
for i in list(indexbowlerwithoutrunratechange.keys()):
    bb_toporder=indexbowlerwithoutrunratechange[i]['balls_against_top_order']
    bb_midorder=indexbowlerwithoutrunratechange[i]['balls_against_middle_order']
    bb_tailender=indexbowlerwithoutrunratechange[i]['balls_against_tailender']
    bb_total=indexbowlerwithoutrunratechange[i]['balls']
    TO_mean=np.mean(totalballs_per_bowler_toporder)
    MO_mean=np.mean(totalballs_per_bowler_middleorder)
    TE_mean=np.mean(totalballs_per_bowler_tailender)
    TB_mean=np.mean(totalballs_per_bowler)
    bowlersballratio[i]={}
    bowlersballratio[i]['to']=(bb_toporder/total_to_balls)*(bb_toporder/bb_total)/0.15
    bowlersballratio[i]['mo']=(bb_midorder/total_mo_balls)*(bb_midorder/bb_total)/0.68
    bowlersballratio[i]['te']=(bb_tailender/total_te_balls)*(bb_tailender/bb_total)/0.17
    bowlersballratio[i]['difficulty_rpb']=0.43*(bowlersballratio[i]['to'])+0.45*bowlersballratio[i]['mo']+0.12*bowlersballratio[i]['te']
    bowlersballratio[i]['difficulty_bpw']=0.41*(bowlersballratio[i]['to'])+0.48*bowlersballratio[i]['mo']+0.11*bowlersballratio[i]['te']
    bowlersballratio[i]['difficulty_bpd']=0.48*(bowlersballratio[i]['to'])+0.40*bowlersballratio[i]['mo']+0.12*bowlersballratio[i]['te']
    bowlersballratio[i]['difficulty']=bowlersballratio[i]['difficulty_rpb']+bowlersballratio[i]['difficulty_bpw']+bowlersballratio[i]['difficulty_bpd']
#     bowlersballratio[i]['br_tor']=round(((bb_toporder-TO_mean)/(bb_total-TB_mean)),2)
#     bowlersballratio[i]['br_mor']=round(((bb_midorder-MO_mean)/(bb_total-TB_mean)),2)
#     bowlersballratio[i]['br_ter']=round(((bb_tailender-TE_mean)/(bb_total-TB_mean)),2)
#for x in list(bowlersballratio.keys()) :   
#    print(x,bowlersballratio[x])


# In[33]:


indexdatawithoutrunratechange={}
for bowlers in list(indexbowlerwithoutrunratechange.keys()):
    indexdatawithoutrunratechange[bowlers]={}
    indexdatawithoutrunratechange[bowlers]['runs_per_ball']={}
    indexdatawithoutrunratechange[bowlers]['runs_per_ball']['overall']=round((indexbowlerwithoutrunratechange[bowlers]['runs']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
    if indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']!=0:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['runsagainst_top_order']/indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_toporder']='NA'
    
    if indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']!=0:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['runsaginst_middle_order']/indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_middleorder']='NA'
        
    if indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']!=0:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['runsagainst_tailender']/indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_tailender']='NA'
        
    indexdatawithoutrunratechange[bowlers]['wickets']={}
    indexdatawithoutrunratechange[bowlers]['wickets']['overall']=indexbowlerwithoutrunratechange[bowlers]['wickets']
    indexdatawithoutrunratechange[bowlers]['wickets']['v_toporder']=indexbowlerwithoutrunratechange[bowlers]['wickets_toporder']
    indexdatawithoutrunratechange[bowlers]['wickets']['v_middleorder']=indexbowlerwithoutrunratechange[bowlers]['wickets_middleorder']
    indexdatawithoutrunratechange[bowlers]['wickets']['v_tailender']=indexbowlerwithoutrunratechange[bowlers]['wickets_tailender']
    
    indexdatawithoutrunratechange[bowlers]['balls_per_wickets']={}
    indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['overall']=round((indexbowlerwithoutrunratechange[bowlers]['balls']/indexbowlerwithoutrunratechange[bowlers]['wickets']),2)
    if indexbowlerwithoutrunratechange[bowlers]['wickets_toporder']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']/indexbowlerwithoutrunratechange[bowlers]['wickets_toporder']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_toporder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['wickets_middleorder']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']/indexbowlerwithoutrunratechange[bowlers]['wickets_middleorder']),2)
    else:   
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_middleorder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['wickets_tailender']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']/indexbowlerwithoutrunratechange[bowlers]['wickets_tailender']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_tailender']='NA'
    
    indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']={}
    if indexbowlerwithoutrunratechange[bowlers]['dotballs']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['overall']=round((indexbowlerwithoutrunratechange[bowlers]['balls']/indexbowlerwithoutrunratechange[bowlers]['dotballs']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['overall']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['dotballs_against_top_order']!=0:   
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']/indexbowlerwithoutrunratechange[bowlers]['dotballs_against_top_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_toporder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['dotballs_against_middle_order']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']/indexbowlerwithoutrunratechange[bowlers]['dotballs_against_middle_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_middleorder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['dotballs_against_tailender']!=0:    
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']/indexbowlerwithoutrunratechange[bowlers]['dotballs_against_tailender']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_tailender']='NA'
    
    indexdatawithoutrunratechange[bowlers]['balls_ratio']={}
    indexdatawithoutrunratechange[bowlers]['balls_ratio']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
    indexdatawithoutrunratechange[bowlers]['balls_ratio']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
    indexdatawithoutrunratechange[bowlers]['balls_ratio']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
#for x in list(indexdatawithoutrunratechange.keys()):
    
#    print(x,indexdatawithoutrunratechange[x])


# In[34]:


def rpbrating(rpb,mean_rpb):
    if rpb=='NA':
        return(mean_rpb)
    else:
        return(rpb)
    
def bpwrating(bpw,rpb,mean_bpw):
    if bpw=='NA' and rpb=='NA':
        return(mean_bpw)
    elif bpw=='NA':
        return(2*mean_bpw)
    else:
        return(bpw)
    
def bpdrating(bpd,rpb,mean_bpd):
    if bpd=='NA'and rpb=='NA':
        return(mean_bpd)
    elif bpd=='NA':
        return(2*mean_bpd)
    else:
        return(bpd)


# In[35]:


def f_rpb(rpb,order):
    if order=='v_toporder':
        b_rpb_to.append(rpb)
    elif order=='v_middleorder':
        b_rpb_mo.append(rpb)
    elif order=='v_tailender':
        b_rpb_te.append(rpb)
        
def f_bpw(bpw,order):
    if order=='v_toporder':
        b_bpw_to.append(bpw)
    elif order=='v_middleorder':
        b_bpw_mo.append(bpw)
    elif order=='v_tailender':
        b_bpw_te.append(bpw)
        
def f_bpd(bpd,order):
    if order=='v_toporder':
        b_bpd_to.append(bpd)
    elif order=='v_middleorder':
        b_bpd_mo.append(bpd)
    elif order=='v_tailender':
        b_bpd_te.append(bpd)




rpb_to=0.402
rpb_mo=0.354
rpb_te=0.244
bpw_to=0.39
bpw_mo=0.35
bpw_te=0.26
bpd_to=0.42
bpd_mo=0.34
bpd_te=0.24

b_rpb_to=[]
b_rpb_mo=[]
b_rpb_te=[]
b_bpw_to=[]
b_bpw_mo=[]
b_bpw_te=[]
b_bpd_to=[]
b_bpd_mo=[]
b_bpd_te=[]

bowlerList=list(indexdatawithoutrunratechange.keys())
for bowlers in bowlerList:
    to=bowlersballratio[bowlers]['to']
    mo=bowlersballratio[bowlers]['mo']
    te=bowlersballratio[bowlers]['te']
    
    brpb_to=indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_toporder']
    
    
    brpb_mo=indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_middleorder']
    brpb_te=indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_tailender']
    
    bbpw_to=indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_toporder']
    bbpw_mo=indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_middleorder']
    bbpw_te=indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_tailender']
    
    bbpd_to=indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_toporder']
    bbpd_mo=indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_middleorder']
    bbpd_te=indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_tailender']
    
    f_rpb(brpb_to,'v_toporder')
    f_rpb(brpb_mo,'v_middleorder')
    f_rpb(brpb_te,'v_tailender')
    
    f_bpw(bbpw_to,'v_toporder')
    f_bpw(bbpw_mo,'v_middleorder')
    f_bpw(bbpw_te,'v_tailender')
    
    f_bpd(bbpd_to,'v_toporder')
    f_bpd(bbpd_mo,'v_middleorder')
    f_bpd(bbpd_te,'v_tailender')
    
    mean_rpb_to=np.mean([val for val in b_rpb_to if val!='NA'])
    mean_rpb_mo=np.mean([val for val in b_rpb_mo if val!='NA'])
    mean_rpb_te=np.mean([val for val in b_rpb_te if val!='NA'])
    
    mean_bpw_to=np.mean([val for val in b_bpw_to if val!='NA'])
    mean_bpw_mo=np.mean([val for val in b_bpw_mo if val!='NA'])
    mean_bpw_te=np.mean([val for val in b_bpw_te if val!='NA'])
    
    mean_bpd_to=np.mean([val for val in b_bpd_to if val!='NA'])
    mean_bpd_mo=np.mean([val for val in b_bpd_mo if val!='NA'])
    mean_bpd_te=np.mean([val for val in b_bpd_te if val!='NA'])
    
    max_rpb_to=max([val for val in b_rpb_to if val!='NA'])
    max_rpb_mo=max([val for val in b_rpb_mo if val!='NA'])
    max_rpb_te=max([val for val in b_rpb_te if val!='NA'])
    if len(list(set(b_bpw_to).difference(set(['NA']))))>0:
        max_bpw_to=max([val for val in b_bpw_to if val!='NA'])
    max_bpw_mo=max([val for val in b_bpw_mo if val!='NA'])
    max_bpw_te=max([val for val in b_bpw_te if val!='NA'])
    if len(list(set(b_bpd_to).difference(set(['NA']))))>0:
        max_bpd_to=max([val for val in b_bpd_to if val!='NA'])
    max_bpd_mo=max([val for val in b_bpd_mo if val!='NA'])
    max_bpd_te=max([val for val in b_bpd_te if val!='NA'])
    
    min_rpb_to=min([val for val in b_rpb_to if val!='NA'])
    min_rpb_mo=min([val for val in b_rpb_mo if val!='NA'])
    min_rpb_te=min([val for val in b_rpb_te if val!='NA'])
    if len(list(set(b_bpw_to).difference(set(['NA']))))>0:
        min_bpw_to=min([val for val in b_bpw_to if val!='NA'])
    min_bpw_mo=min([val for val in b_bpw_mo if val!='NA'])
    min_bpw_te=min([val for val in b_bpw_te if val!='NA'])
    if len(list(set(b_bpd_to).difference(set(['NA']))))>0:
        min_bpd_to=min([val for val in b_bpd_to if val!='NA'])
    min_bpd_mo=min([val for val in b_bpd_mo if val!='NA'])
    min_bpd_te=min([val for val in b_bpd_te if val!='NA'])
    
#     finalperformancerating[bowlers]={}
#     finalperformancerating[bowlers]['rating_rpb']=round((rpbrating(brpb_to)*rpb_to*to+rpbrating(brpb_mo)*rpb_mo*mo+rpbrating(brpb_te)*rpb_te*te),3)
#     finalperformancerating[bowlers]['rating_bpw']=round((bpwrating(bbpw_to)*bpw_to*to+bpwrating(bbpw_mo)*bpw_mo*mo+bpwrating(bbpw_te)*bpw_te*te),3)
#     finalperformancerating[bowlers]['rating_bpd']=round((bpdrating(bbpd_to)*bpd_to*to+rpbrating(bbpd_mo)*bpd_mo*mo+rpbrating(bbpd_te)*bpd_te*te),3)
#     finalperformancerating[bowlers]['total_rating']=round((finalperformancerating[bowlers]['rating_rpb']+finalperformancerating[bowlers]['rating_bpw']+finalperformancerating[bowlers]['rating_bpd']),3)

    

#print(mean_bpw_to,mean_bpw_mo,mean_bpw_te,max_rpb_to)


# In[36]:


def weightageRPB(mean_rpb,bowler_rpb,order):
    if order=='v_toporder':
        if bowler_rpb<=mean_rpb:
            return(0.41)
        else:
            return(0.59)
        
    elif order=='v_middleorder':
        if bowler_rpb<=mean_rpb:
            return(0.44)
        else:
            return(0.56)
        
    elif order=='v_tailender':
        if bowler_rpb<=mean_rpb:
            return(0.15)
        else:
            return(0.85)
    
def weightageBPW(mean_bpw,bowler_bpw,order):
    if order=='v_toporder':
        if bowler_bpw<=mean_bpw:
            return(0.40)
        else:
            return(0.60)
        
    elif order=='v_middleorder':
        if bowler_bpw<=mean_bpw:
            return(0.46)
        else:
            return(0.54)
        
    elif order=='v_tailender':
        if bowler_bpw<=mean_bpw:
            return(0.14)
        else:
            return(0.86)
    
def weightageDPB(mean_dpb,bowler_dpb,order):
    if order=='v_toporder':
        if bowler_dpb<=mean_dpb:
            return(0.48)
        else:
            return(0.52)
        
    elif order=='v_middleorder':
        if bowler_dpb<=mean_dpb:
            return(0.45)
        else:
            return(0.55)
        
    elif order=='v_tailender':
        if bowler_dpb<=mean_dpb:
            return(0.07)
        else:
            return(0.93)


# In[37]:


finalperformancerating={}
for b in range(len(bowlerList)):
    
    tor=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_toporder']
    mor=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_middleorder']
    ter=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_tailender']
    
    tow=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_toporder']
    mow=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_middleorder']
    tew=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_tailender']
    
    tod=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_toporder']
    mod=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_middleorder']
    ted=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_tailender']
    
    finalperformancerating[bowlerList[b]]={}
    finalperformancerating[bowlerList[b]]['rating_rpb']=round((((rpbrating(tor,mean_rpb_to)-mean_rpb_to)/(max_rpb_to-min_rpb_to))*weightageRPB(mean_rpb_to,rpbrating(tor,mean_rpb_to),'v_toporder')                                                               +((rpbrating(mor,mean_rpb_mo)-mean_rpb_mo)/(max_rpb_mo-min_rpb_mo))*weightageRPB(mean_rpb_mo,rpbrating(mor,mean_rpb_mo),'v_middleorder')                                                               +((rpbrating(ter,mean_rpb_te)-mean_rpb_te)/(max_rpb_te-min_rpb_te))*weightageRPB(mean_rpb_te,rpbrating(ter,mean_rpb_te),'v_tailender')),3)
    finalperformancerating[bowlerList[b]]['rating_bpw']=round((((bpwrating(tow,tor,mean_bpw_to)-mean_bpw_to)/(max_bpw_to-min_bpw_to))*weightageBPW(mean_bpw_to,bpwrating(tow,tor,mean_bpw_to),'v_toporder')                                                               +((bpwrating(mow,mor,mean_bpw_mo)-mean_bpw_mo)/(max_bpw_mo-min_bpw_mo))*weightageBPW(mean_bpw_mo,bpwrating(mow,mor,mean_bpw_mo),'v_middleorder')                                                               +((bpwrating(tew,ter,mean_bpw_te)-mean_bpw_te)/(max_bpw_te-min_bpw_te))*weightageBPW(mean_bpw_te,bpwrating(tew,ter,mean_bpw_te),'v_tailender')),3)
    finalperformancerating[bowlerList[b]]['rating_bpd']=round((((bpdrating(tod,tor,mean_bpd_to)-mean_bpd_to)/(max_bpd_to-min_bpd_to))*weightageDPB(mean_bpd_to,bpdrating(tod,tor,mean_bpd_to),'v_toporder')                                                               +((bpdrating(mod,mor,mean_bpd_mo)-mean_bpd_mo)/(max_bpd_mo-min_bpd_mo))*weightageDPB(mean_bpd_mo,bpdrating(mod,mor,mean_bpd_mo),'v_middleorder')                                                               +((bpdrating(ted,ter,mean_bpd_te)-mean_bpd_te)/(max_bpd_te-min_bpd_te))*weightageDPB(mean_bpd_te,bpdrating(ted,ter,mean_bpd_te),'v_tailender')),3)
    finalperformancerating[bowlerList[b]]['total_rating']=round(((-1)*finalperformancerating[bowlerList[b]]['rating_rpb']                                                                 +(-1)*finalperformancerating[bowlerList[b]]['rating_bpw']                                                                 +(-1)*finalperformancerating[bowlerList[b]]['rating_bpd']),3)
    
    
#for bowlers in list(finalperformancerating.keys()):
#    print(bowlers,finalperformancerating[bowlers]['total_rating'])


# In[38]:


WC_totalrating=[]
for bowlers in list(finalperformancerating.keys()):
    WC_totalrating.append([bowlers,finalperformancerating[bowlers]['total_rating']])
    
WC_totalrating.sort(key=lambda x:x[1],reverse=True)
#print(WC_totalrating)


# In[39]:


performancerating={}
for i in WC_totalrating:
    performancerating[i[0]]={}
    performancerating[i[0]]['performancerating']=i[1]
#print(performancerating)


# In[40]:


scaledrating={}
Lsituation=[]
Lperformance=[]
Ldifficulty=[]

for i in list(situationindex.keys()):
    Lsituation.append(situationindex[i]['situation_rating'])
    Lperformance.append(performancerating[i]['performancerating'])
    Ldifficulty.append(bowlersballratio[i]['difficulty'])
for j in list(situationindex.keys()):
    scaledrating[j]={}
    scaledrating[j]['situation']=round(((situationindex[j]['situation_rating']-min(Lsituation))/(max(Lsituation)-min(Lsituation))),2)
    scaledrating[j]['performance']=round(((performancerating[j]['performancerating']-min(Lperformance))/(max(Lperformance)-min(Lperformance))),2)
    scaledrating[j]['difficulty']=round(((bowlersballratio[j]['difficulty']-min(Ldifficulty))/(max(Ldifficulty)-min(Ldifficulty))),2)

#print(scaledrating)


# In[41]:


totalrating={}
for i in list(situationindex.keys()):
    totalrating[i]={}
    totalrating[i]['ratingpts']=(0.25)*scaledrating[i]['situation']+(0.5)*scaledrating[i]['performance']+(0.25)*scaledrating[i]['difficulty']
    totalrating[i]['overall_stats']=indexbowlerwithoutrunratechange[i]
    totalrating[i]['perball_overall_stats']=indexdatawithoutrunratechange[i]
#print(totalrating)


# In[42]:


overallrating=[]
for i in list(totalrating.keys()):
    overallrating.append([i,totalrating[i]['ratingpts'],totalrating[i]['overall_stats'],totalrating[i]['perball_overall_stats']])
    
#print(tabulate(overallrating))
#print(overallrating[1][0])


# In[43]:


overallrating.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(overallrating))


# In[44]:


import csv
filecursor=open('deathranking_outfile6.csv','w')
csvWriter=csv.writer(filecursor)
csvWriter.writerow(['bowler','rating','total_balls','ballsVtoporder','ballsVmiddleorder','ballsVtailender','overallRPB','RPBvtoporder','RPBvmiddleorder',                   'RPBvtailender','wickets','wktVtoporder','wktVmiddleorder','wktVtailender','overallBPW','BPWvToporder','BPWvmiddleorder','BPWvtailender',                   'performancerating','situationrating','difficultyfaced'])
for bowler in overallrating:
    csvWriter.writerow([bowler[0],totalrating[bowler[0]]['ratingpts'],indexbowlerwithoutrunratechange[bowler[0]]['balls'],                       indexbowlerwithoutrunratechange[bowler[0]]['balls_against_top_order'],indexbowlerwithoutrunratechange[bowler[0]]['balls_against_middle_order'],                      indexbowlerwithoutrunratechange[bowler[0]]['balls_against_tailender'],indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['overall'],                       indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['v_toporder'],indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['v_middleorder'],                       indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['v_tailender'],indexbowlerwithoutrunratechange[bowler[0]]['wickets'],                       indexbowlerwithoutrunratechange[bowler[0]]['wickets_toporder'],indexbowlerwithoutrunratechange[bowler[0]]['wickets_middleorder'],                        indexbowlerwithoutrunratechange[bowler[0]]['wickets_tailender'],indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['overall'],                       indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['v_toporder'],indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['v_middleorder'],                       indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['v_tailender'],scaledrating[bowler[0]]['performance'],scaledrating[bowler[0]]['situation'],                       scaledrating[bowler[0]]['difficulty']])
filecursor.close()


# In[45]:


overallratingfinal=[]
for i in list(totalrating.keys()):
    overallratingfinal.append([i,totalrating[i]['ratingpts']])
    
#print(tabulate(overallratingfinal))


# In[46]:


overallratingfinal.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(overallratingfinal))


# In[47]:


for i in overallratingfinal:
    i.append(overallratingfinal.index(i)+1)
    
overallratingfinal.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(overallratingfinal))


# In[48]:


situation=[]
performance=[]
difficulty=[]
for i in list(situationindex.keys()):
    situation.append([i,situationindex[i]['situation_rating']])
    performance.append([i,performancerating[i]['performancerating']])
    difficulty.append([i,bowlersballratio[i]['difficulty']])
situation.sort(key=lambda x:x[1],reverse=True)
performance.sort(key=lambda x:x[1],reverse=True)
difficulty.sort(key=lambda x:x[1],reverse=True)


# In[49]:


#print(tabulate(situation),tabulate(performance),tabulate(difficulty))


# In[50]:


with open('bowlerdatabase__allMatches.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    allBowlersInfo = unpickler.load() 
    file.close()


# Second Index

# Middle Overs

# In[51]:



middleoverslist=[]
for i in matches:
    thismatchdata={}
    match_key=list(i.keys())[0]
    thismatchdata[match_key]={}
    for innings in list(i[match_key].keys()):
        
        thismatchdata[match_key][innings]={}
        thismatchdata[match_key][innings]['middle_overs_ballbyball']={}
        balls_key=list(i[match_key][innings].keys())
        for balls in balls_key:
            if balls==84:
                runs_before_middle_overs=i[match_key][innings][balls]['score']
                Rem_wickets_before_middle_overs=i[match_key][innings][balls]['Rem_wicket']
                
            if balls==max(24,min(balls_key)):
                runs_after_middle_overs=i[match_key][innings][balls]['score']
                Rem_wickets_after_middle_overs=i[match_key][innings][balls]['Rem_wicket']
                
            
                
            bowler=i[match_key][innings][balls]['bowler']
            score=i[match_key][innings][balls]['score']
            Rem_wicket=i[match_key][innings][balls]['Rem_wicket']
            batting_order=i[match_key][innings][balls]['batting_order_of_batsman']
            thisballrun=i[match_key][innings][balls]['thisballrun']
            if balls == 84:
                before_death_winProb_1stBatTeam=getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=84,wicketRem=Rem_wicket))
            if min(balls_key)<84:
                if balls<84 and balls>=max(24,min(balls_key)):
                    thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]={}
                    thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['runs']=score
                    thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['thisballrun']=thisballrun
                    thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['Rem_wkts']=Rem_wicket
                    thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['bowler']=bowler
                    thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['batting_order_bowled']=batting_order
                    if i[match_key][innings][balls]['bowlerwicket']=='yes':
                        thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['bowlerwicket']='yes'
                    else:
                        thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['bowlerwicket']='no'
                    thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['winProb_1stBatTeam']=                                                        getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=balls,wicketRem=Rem_wicket))
                    thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['before_death_winProb_1stBatTeam']=before_death_winProb_1stBatTeam
                    if balls==83:
                        thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']=                        round((before_death_winProb_1stBatTeam-getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=83,wicketRem=Rem_wicket))),2)
                        if thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']>=0:
                            thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                            round(((before_death_winProb_1stBatTeam-                                    getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=83,wicketRem=Rem_wicket)))*before_death_winProb_1stBatTeam),2)
                            
                        else:
                            thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                            round(((before_death_winProb_1stBatTeam-                                    getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=83,wicketRem=Rem_wicket)))*(100-before_death_winProb_1stBatTeam)),2)
                    else:
                        thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']=                            round((getWinProb(i[match_key][innings][balls+1]['score']+                                          get_OLS_ballByBallDict_catWickRem(ballRem=balls+1,wicketRem=i[match_key][innings][balls+1]['Rem_wicket']))-                                           getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=balls,wicketRem=Rem_wicket))),2)
                        if thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']>=0:
                            thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                                thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']*                                thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls+1]['winProb_1stBatTeam']
                        else:
                            thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                                thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']*                                (100-thismatchdata[match_key][innings]['middle_overs_ballbyball'][balls+1]['winProb_1stBatTeam'])
                            
                            
#                             round(((getWinProb(i[match_key][innings][balls+1]['score']+\
#                                            get_OLS_ballByBallDict_catWickRem(ballRem=balls+1,wicketRem=i[match_key][innings][balls+1]['Rem_wicket']))-\
#                                 getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=balls,wicketRem=Rem_wicket)))*((getWinProb(i[match_key][innings][balls+1]['score']+\
#                         get_OLS_ballByBallDict_catWickRem(ballRem=balls+1,wicketRem=i[match_key][innings][balls+1]['Rem_wicket'])))),2)
            else:
                thismatchdata[match_key][innings]['middle_overs_ballbyball']='inngs over before 7th over'
            if balls==84:
                thismatchdata[match_key][innings]['run_rate_before_middleovers']=round((runs_before_middle_overs/36),2)
        
        if min(balls_key)<84:
            if (84-min(balls_key))!=0:
                thismatchdata[match_key][innings]['run_rate_in_middleovers']=round((runs_after_middle_overs-runs_before_middle_overs)/(84-max(24,min(balls_key))),2)
        else:
            thismatchdata[match_key][innings]['run_rate_in_middleovers']='NA'
            
        if min(balls_key)<84:
            if (Rem_wickets_before_middle_overs-Rem_wickets_after_middle_overs)!=0:
                thismatchdata[match_key][innings]['runs_per_wicket_during_middle']=round(((runs_after_middle_overs-runs_before_middle_overs)/                                                                                         (Rem_wickets_before_middle_overs-Rem_wickets_after_middle_overs)),2)
            else:
                thismatchdata[match_key][innings]['runs_per_wicket_during_middle']='NA'
        else:
            thismatchdata[match_key][innings]['runs_per_wicket_during_middle']='NA'
            
    middleoverslist.append(thismatchdata)


# In[52]:


#middleoverslist


# In[53]:


battingorderseverity={}
battingorderseverity['top_order']={}
battingorderseverity['top_order']['balls']=0
battingorderseverity['top_order']['runs']=0
battingorderseverity['top_order']['dot_balls']=0
battingorderseverity['top_order']['wickets']=0
battingorderseverity['middle_order']={}
battingorderseverity['middle_order']['balls']=0
battingorderseverity['middle_order']['runs']=0
battingorderseverity['middle_order']['dot_balls']=0
battingorderseverity['middle_order']['wickets']=0
battingorderseverity['tail_ender']={}
battingorderseverity['tail_ender']['balls']=0
battingorderseverity['tail_ender']['runs']=0
battingorderseverity['tail_ender']['dot_balls']=0
battingorderseverity['tail_ender']['wickets']=0
for i in middleoverslist:
    match_key=list(i.keys())[0]
    for x in list(i[match_key].keys()):
    
        if i[match_key][x]['middle_overs_ballbyball']=='inngs over before 7th over':
            continue
        balls_key=list(i[match_key][x]['middle_overs_ballbyball'].keys())

        for balls in balls_key: 
            if i[match_key][x]['middle_overs_ballbyball'][balls]['batting_order_bowled']<=3:
                battingorderseverity['top_order']['balls']+=1
                battingorderseverity['top_order']['runs']+=i[match_key][x]['middle_overs_ballbyball'][balls]['thisballrun']
                battingorderseverity['top_order']['runs_per_ball']=round((battingorderseverity['top_order']['runs']/battingorderseverity['top_order']['balls']),2)
                if i[match_key][x]['middle_overs_ballbyball'][balls]['thisballrun']==0:
                    battingorderseverity['top_order']['dot_balls']+=1
                if i[match_key][x]['middle_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                    battingorderseverity['top_order']['wickets']+=1
            if i[match_key][x]['middle_overs_ballbyball'][balls]['batting_order_bowled']>=4 and i[match_key][x]['middle_overs_ballbyball'][balls]['batting_order_bowled']<=7:

                battingorderseverity['middle_order']['balls']+=1
                battingorderseverity['middle_order']['runs']+=i[match_key][x]['middle_overs_ballbyball'][balls]['thisballrun']
                battingorderseverity['middle_order']['runs_per_ball']=round((battingorderseverity['middle_order']['runs']/battingorderseverity['middle_order']['balls']),2)
                if i[match_key][x]['middle_overs_ballbyball'][balls]['thisballrun']==0:
                    battingorderseverity['middle_order']['dot_balls']+=1
                if i[match_key][x]['middle_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                    battingorderseverity['middle_order']['wickets']+=1
            if i[match_key][x]['middle_overs_ballbyball'][balls]['batting_order_bowled']>=8 and i[match_key][x]['middle_overs_ballbyball'][balls]['batting_order_bowled']<=11:

                battingorderseverity['tail_ender']['balls']+=1
                battingorderseverity['tail_ender']['runs']+=i[match_key]['1st innings']['middle_overs_ballbyball'][balls]['thisballrun']
                battingorderseverity['tail_ender']['runs_per_ball']=round((battingorderseverity['tail_ender']['runs']/battingorderseverity['tail_ender']['balls']),2)
                if i[match_key][x]['middle_overs_ballbyball'][balls]['thisballrun']==0:
                    battingorderseverity['tail_ender']['dot_balls']+=1
                if i[match_key][x]['middle_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                    battingorderseverity['tail_ender']['wickets']+=1
#print(battingorderseverity)


# In[54]:


battingorderseverity_ratio={}
totalruns_in_death_overs=battingorderseverity['top_order']['runs']+battingorderseverity['middle_order']['runs']+battingorderseverity['tail_ender']['runs']
totalballs_in_death_overs=battingorderseverity['top_order']['balls']+battingorderseverity['middle_order']['balls']+battingorderseverity['tail_ender']['balls']
totaruns_per_ball=round((totalruns_in_death_overs/totalballs_in_death_overs),2)
battingorderseverity_ratio['top_order']=round((battingorderseverity['top_order']['runs_per_ball']/totaruns_per_ball),2)
battingorderseverity_ratio['middle_order']=round((battingorderseverity['middle_order']['runs_per_ball']/totaruns_per_ball),2)
battingorderseverity_ratio['tail_ender']=round((battingorderseverity['tail_ender']['runs_per_ball']/totaruns_per_ball),2)

battingorderseverity_ratio['balls_per_dot_top_order']=round((battingorderseverity['top_order']['balls']/battingorderseverity['top_order']['dot_balls']),2)
battingorderseverity_ratio['balls_per_dot_middle_order']=round((battingorderseverity['middle_order']['balls']/battingorderseverity['middle_order']['dot_balls']),2)
battingorderseverity_ratio['balls_per_dot_tail_ender']=round((battingorderseverity['tail_ender']['balls']/battingorderseverity['tail_ender']['dot_balls']),2)

battingorderseverity_ratio['balls_per_wicket_top_order']=round((battingorderseverity['top_order']['balls']/battingorderseverity['top_order']['wickets']),2)
battingorderseverity_ratio['balls_per_wicket_middle_order']=round((battingorderseverity['middle_order']['balls']/battingorderseverity['middle_order']['wickets']),2)
battingorderseverity_ratio['balls_per_wicket_tail_ender']=round((battingorderseverity['tail_ender']['balls']/battingorderseverity['tail_ender']['wickets']),2)

total_dot_balls=battingorderseverity_ratio['balls_per_dot_top_order']+battingorderseverity_ratio['balls_per_dot_middle_order']+battingorderseverity_ratio['balls_per_dot_tail_ender']
total_balls_per_dot_in_death=round((totalballs_in_death_overs/total_dot_balls),2)
total_balls_per_wicket=battingorderseverity_ratio['balls_per_wicket_top_order']+battingorderseverity_ratio['balls_per_wicket_middle_order']+battingorderseverity_ratio['balls_per_wicket_tail_ender']
battingorderseverity_ratio['dot_ball_ratio_top_order']=round((battingorderseverity_ratio['balls_per_dot_top_order']/total_dot_balls),2)
battingorderseverity_ratio['dot_ball_ratio_middle_order']=round((battingorderseverity_ratio['balls_per_dot_middle_order']/total_dot_balls),2)
battingorderseverity_ratio['dot_ball_ratio_tailender']=round((battingorderseverity_ratio['balls_per_dot_tail_ender']/total_dot_balls),2)

battingorderseverity_ratio['bpw_ratio_top_order']=round((battingorderseverity_ratio['balls_per_wicket_top_order']/total_balls_per_wicket),2)
battingorderseverity_ratio['bpw_ratio_middle_order']=round((battingorderseverity_ratio['balls_per_wicket_middle_order']/total_balls_per_wicket),2)
battingorderseverity_ratio['bpw_ratio_tailender']=round((battingorderseverity_ratio['balls_per_wicket_tail_ender']/total_balls_per_wicket),2)


#print(battingorderseverity_ratio)


# In[55]:


bowlers_in_middle_overs={}
for i in middleoverslist:
    match_keys=list(i.keys())[0]
    for y in list(i[match_keys].keys()):
        if i[match_keys][y]['middle_overs_ballbyball']=='inngs over before 7th over':
            continue
        balls_key=list(i[match_keys][y]['middle_overs_ballbyball'].keys())

        for balls in balls_key:

            bowler=i[match_keys][y]['middle_overs_ballbyball'][balls]['bowler']
            if bowler not in list(bowlers_in_middle_overs.keys()):
                bowlers_in_middle_overs[bowler]={}
                bowlers_in_middle_overs[bowler]['balls']=0
                bowlers_in_middle_overs[bowler]['change_in_WP']=0
                bowlers_in_middle_overs[bowler]['weightedchange_in_WP']=0

            for bowlers in list(bowlers_in_middle_overs.keys()):
                if bowlers==bowler:
                    bowlers_in_middle_overs[bowler]['balls']+=1
                    bowlers_in_middle_overs[bowler]['change_in_WP']+=i[match_keys][y]['middle_overs_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']
                    bowlers_in_middle_overs[bowler]['weightedchange_in_WP']+=i[match_keys][y]['middle_overs_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']
                    bowlers_in_middle_overs[bowler]['change_in_WP_perball']=round((bowlers_in_middle_overs[bowler]['change_in_WP']/bowlers_in_middle_overs[bowler]['balls']),2)
                    bowlers_in_middle_overs[bowler]['weightedchange_in_WP_perball']=round((bowlers_in_middle_overs[bowler]['weightedchange_in_WP']/bowlers_in_middle_overs[bowler]['balls']),2)
    significantbowlers=[]
for x in list(bowlers_in_middle_overs.keys()):
    if bowlers_in_middle_overs[x]['balls']>=120:
        significantbowlers.append([x,bowlers_in_middle_overs[x]['change_in_WP_perball'],(bowlers_in_middle_overs[x]['weightedchange_in_WP_perball'])/100,bowlers_in_middle_overs[x]['balls']])
        


# In[56]:


#significantbowlers


# In[57]:


filtered_significantbowlers=[i for i in significantbowlers if i[3]>=120]
#filtered_significantbowlers


# In[58]:


situationindex={}
for i in filtered_significantbowlers:
    situationindex[i[0]]={}
    situationindex[i[0]]['situation_rating']=i[2]
#print(situationindex)


# In[59]:


from tabulate import tabulate
filtered_significantbowlers.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(filtered_significantbowlers))


# In[60]:


filtered_significantbowlers.sort(key=lambda x:x[2],reverse=True)
#print(tabulate(filtered_significantbowlers))


# In[61]:


matches_for_runrate_drop_in_middleovers_infirstinnings=[]
matches_for_runrate_drop_in_middleovers_insecondinnings=[]
for i in middleoverslist:

    for j in list(i.keys()):
        innings_key=list(i[j].keys())
        if i[j][innings_key[0]]['run_rate_in_middleovers']!='NA':
            if i[j][innings_key[0]]['run_rate_before_middleovers']>i[j][innings_key[0]]['run_rate_in_middleovers']:
                matches_for_runrate_drop_in_middleovers_infirstinnings.append(i)
            
        if i[j][innings_key[1]]['run_rate_in_middleovers']!='NA':
            if i[j][innings_key[1]]['run_rate_before_middleovers']>i[j][innings_key[1]]['run_rate_in_middleovers']:
                matches_for_runrate_drop_in_middleovers_insecondinnings.append(i)
                
runratedrop={}
runratedrop['1st innings']=matches_for_runrate_drop_in_middleovers_infirstinnings
runratedrop['2nd innings']=matches_for_runrate_drop_in_middleovers_insecondinnings


# In[62]:


#len(matches_for_runrate_drop_in_middleovers_insecondinnings)


# In[63]:


matchdataofmiddleoversfirstinnings=[]
matchdataofmiddleoverssecondinnings=[]
for i in middleoverslist:

    for j in list(i.keys()):
        innings_key=list(i[j].keys())
        if i[j][innings_key[0]]['run_rate_in_middleovers']!='NA':
            
            matchdataofmiddleoversfirstinnings.append(i)
            
        if i[j][innings_key[1]]['run_rate_in_middleovers']!='NA':
            
            matchdataofmiddleoverssecondinnings.append(i)
                
matchdata={}
matchdata['1st innings']=matchdataofmiddleoversfirstinnings
matchdata['2nd innings']=matchdataofmiddleoverssecondinnings


# In[64]:


#middleoverslist


# In[65]:


bowlersdatawithoutrunratechange={}

for i in middleoverslist:
    match_keys=list(i.keys())[0]
    for y in list(i[match_keys].keys()):
        if i[match_keys][y]['middle_overs_ballbyball']=='inngs over before 7th over':
            continue
        
        balls_key=list(i[match_keys][y]['middle_overs_ballbyball'].keys())

        for balls in balls_key:

            bowler=i[match_keys][y]['middle_overs_ballbyball'][balls]['bowler']
  
            if bowler not in list(bowlersdatawithoutrunratechange.keys()):
                bowlersdatawithoutrunratechange[bowler]={}
                bowlersdatawithoutrunratechange[bowler]['balls']=0
                bowlersdatawithoutrunratechange[bowler]['balls_against_top_order']=0
                bowlersdatawithoutrunratechange[bowler]['balls_against_middle_order']=0
                bowlersdatawithoutrunratechange[bowler]['balls_against_tailender']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs_against_top_order']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs_against_middle_order']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs_against_tailender']=0

                bowlersdatawithoutrunratechange[bowler]['runs']=0
                bowlersdatawithoutrunratechange[bowler]['runsagainst_top_order']=0
                bowlersdatawithoutrunratechange[bowler]['runsaginst_middle_order']=0
                bowlersdatawithoutrunratechange[bowler]['runsagainst_tailender']=0
                bowlersdatawithoutrunratechange[bowler]['wickets']=0
                bowlersdatawithoutrunratechange[bowler]['wickets_toporder']=0
                bowlersdatawithoutrunratechange[bowler]['wickets_middleorder']=0
                bowlersdatawithoutrunratechange[bowler]['wickets_tailender']=0
                bowlersdatawithoutrunratechange[bowler]['batting_order_dismissed']=[]

            for bowlers in list(bowlersdatawithoutrunratechange.keys()):
                if bowlers==bowler:
                    bowlersdatawithoutrunratechange[bowler]['balls']+=1
                    bowlersdatawithoutrunratechange[bowler]['runs']+=i[match_keys][y]['middle_overs_ballbyball'][balls]['thisballrun']
                    if i[match_keys][y]['middle_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                        bowlersdatawithoutrunratechange[bowler]['wickets']+=1
                        bowlersdatawithoutrunratechange[bowler]['batting_order_dismissed'].append(i[match_keys][y]['middle_overs_ballbyball'][balls]['batting_order_bowled'])
                    if i[match_keys][y]['middle_overs_ballbyball'][balls]['thisballrun']==0:
                        bowlersdatawithoutrunratechange[bowler]['dotballs']+=1
                    if i[match_keys][y]['middle_overs_ballbyball'][balls]['batting_order_bowled']>=1                    and i[match_keys][y]['middle_overs_ballbyball'][balls]['batting_order_bowled']<=3:
                        bowlersdatawithoutrunratechange[bowler]['balls_against_top_order']+=1
                        bowlersdatawithoutrunratechange[bowler]['runsagainst_top_order']+=i[match_keys][y]['middle_overs_ballbyball'][balls]['thisballrun']
                        if i[match_keys][y]['middle_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                            bowlersdatawithoutrunratechange[bowler]['wickets_toporder']+=1
                        if i[match_keys][y]['middle_overs_ballbyball'][balls]['thisballrun']==0:
                            bowlersdatawithoutrunratechange[bowler]['dotballs_against_top_order']+=1

                    if i[match_keys][y]['middle_overs_ballbyball'][balls]['batting_order_bowled']>=4                    and i[match_keys][y]['middle_overs_ballbyball'][balls]['batting_order_bowled']<=7:
                        bowlersdatawithoutrunratechange[bowler]['balls_against_middle_order']+=1
                        bowlersdatawithoutrunratechange[bowler]['runsaginst_middle_order']+=i[match_keys][y]['middle_overs_ballbyball'][balls]['thisballrun']
                        if i[match_keys][y]['middle_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                            bowlersdatawithoutrunratechange[bowler]['wickets_middleorder']+=1
                        if i[match_keys][y]['middle_overs_ballbyball'][balls]['thisballrun']==0:
                            bowlersdatawithoutrunratechange[bowler]['dotballs_against_middle_order']+=1

                    if i[match_keys][y]['middle_overs_ballbyball'][balls]['batting_order_bowled']>=8                    and i[match_keys][y]['middle_overs_ballbyball'][balls]['batting_order_bowled']<=11:
                        bowlersdatawithoutrunratechange[bowler]['balls_against_tailender']+=1
                        bowlersdatawithoutrunratechange[bowler]['runsagainst_tailender']+=i[match_keys][y]['middle_overs_ballbyball'][balls]['thisballrun']
                        if i[match_keys][y]['middle_overs_ballbyball'][balls]['bowlerwicket']=='yes':
                            bowlersdatawithoutrunratechange[bowler]['wickets_tailender']+=1
                        if i[match_keys][y]['middle_overs_ballbyball'][balls]['thisballrun']==0:
                            bowlersdatawithoutrunratechange[bowler]['dotballs_against_tailender']+=1
#print(bowlersdatawithoutrunratechange)


# In[66]:


indexbowlerwithoutrunratechange={}
for bowler in list(bowlersdatawithoutrunratechange.keys()):
    
    if bowlersdatawithoutrunratechange[bowler]['balls']>=120:
        indexbowlerwithoutrunratechange[bowler]={}
        indexbowlerwithoutrunratechange[bowler]=bowlersdatawithoutrunratechange[bowler]
#print(indexbowlerwithoutrunratechange)


# In[67]:


totalballs_per_bowler=[]
totalballs_per_bowler_toporder=[]
totalballs_per_bowler_middleorder=[]
totalballs_per_bowler_tailender=[]
for i in list(indexbowlerwithoutrunratechange.keys()):
    totalballs_per_bowler.append(indexbowlerwithoutrunratechange[i]['balls'])
    totalballs_per_bowler_toporder.append(indexbowlerwithoutrunratechange[i]['balls_against_top_order'])
    totalballs_per_bowler_middleorder.append(indexbowlerwithoutrunratechange[i]['balls_against_middle_order'])
    totalballs_per_bowler_tailender.append(indexbowlerwithoutrunratechange[i]['balls_against_tailender'])
#print(np.mean(totalballs_per_bowler),np.mean(totalballs_per_bowler_toporder),np.mean(totalballs_per_bowler_middleorder),np.mean(totalballs_per_bowler_tailender))
#print(totalballs_per_bowler)


# In[68]:


bowlersballratio={}
total_to_balls=sum(totalballs_per_bowler_toporder)
print(total_to_balls)
total_mo_balls=sum(totalballs_per_bowler_middleorder)
total_te_balls=sum(totalballs_per_bowler_tailender)
for i in list(indexbowlerwithoutrunratechange.keys()):
    bb_toporder=indexbowlerwithoutrunratechange[i]['balls_against_top_order']
    bb_midorder=indexbowlerwithoutrunratechange[i]['balls_against_middle_order']
    bb_tailender=indexbowlerwithoutrunratechange[i]['balls_against_tailender']
    bb_total=indexbowlerwithoutrunratechange[i]['balls']
    TO_mean=np.mean(totalballs_per_bowler_toporder)
    MO_mean=np.mean(totalballs_per_bowler_middleorder)
    TE_mean=np.mean(totalballs_per_bowler_tailender)
    TB_mean=np.mean(totalballs_per_bowler)
    bowlersballratio[i]={}
    bowlersballratio[i]['to']=(bb_toporder/total_to_balls)*(bb_toporder/bb_total)/0.47
    bowlersballratio[i]['mo']=(bb_midorder/total_mo_balls)*(bb_midorder/bb_total)/0.50
    bowlersballratio[i]['te']=(bb_tailender/total_te_balls)*(bb_tailender/bb_total)/0.03
    bowlersballratio[i]['difficulty_rpb']=0.48*(bowlersballratio[i]['to'])+0.47*bowlersballratio[i]['mo']+0.05*bowlersballratio[i]['te']
    bowlersballratio[i]['difficulty_bpw']=0.49*(bowlersballratio[i]['to'])+0.44*bowlersballratio[i]['mo']+0.07*bowlersballratio[i]['te']
    bowlersballratio[i]['difficulty_bpd']=0.48*(bowlersballratio[i]['to'])+0.49*bowlersballratio[i]['mo']+0.03*bowlersballratio[i]['te']
    bowlersballratio[i]['difficulty']=bowlersballratio[i]['difficulty_rpb']+bowlersballratio[i]['difficulty_bpw']+bowlersballratio[i]['difficulty_bpd']
#     bowlersballratio[i]['br_tor']=round(((bb_toporder-TO_mean)/(bb_total-TB_mean)),2)
#     bowlersballratio[i]['br_mor']=round(((bb_midorder-MO_mean)/(bb_total-TB_mean)),2)
#     bowlersballratio[i]['br_ter']=round(((bb_tailender-TE_mean)/(bb_total-TB_mean)),2)
#for x in list(bowlersballratio.keys()) :   
#    print(x,bowlersballratio[x])


# In[69]:


indexdatawithoutrunratechange={}
for bowlers in list(indexbowlerwithoutrunratechange.keys()):
    indexdatawithoutrunratechange[bowlers]={}
    indexdatawithoutrunratechange[bowlers]['runs_per_ball']={}
    indexdatawithoutrunratechange[bowlers]['runs_per_ball']['overall']=round((indexbowlerwithoutrunratechange[bowlers]['runs']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
    if indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']!=0:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['runsagainst_top_order']/indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_toporder']='NA'
    
    if indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']!=0:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['runsaginst_middle_order']/indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_middleorder']='NA'
        
    if indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']!=0:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['runsagainst_tailender']/indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_tailender']='NA'
        
    indexdatawithoutrunratechange[bowlers]['wickets']={}
    indexdatawithoutrunratechange[bowlers]['wickets']['overall']=indexbowlerwithoutrunratechange[bowlers]['wickets']
    indexdatawithoutrunratechange[bowlers]['wickets']['v_toporder']=indexbowlerwithoutrunratechange[bowlers]['wickets_toporder']
    indexdatawithoutrunratechange[bowlers]['wickets']['v_middleorder']=indexbowlerwithoutrunratechange[bowlers]['wickets_middleorder']
    indexdatawithoutrunratechange[bowlers]['wickets']['v_tailender']=indexbowlerwithoutrunratechange[bowlers]['wickets_tailender']
    
    indexdatawithoutrunratechange[bowlers]['balls_per_wickets']={}
    indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['overall']=round((indexbowlerwithoutrunratechange[bowlers]['balls']/indexbowlerwithoutrunratechange[bowlers]['wickets']),2)
    if indexbowlerwithoutrunratechange[bowlers]['wickets_toporder']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']/indexbowlerwithoutrunratechange[bowlers]['wickets_toporder']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_toporder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['wickets_middleorder']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']/indexbowlerwithoutrunratechange[bowlers]['wickets_middleorder']),2)
    else:   
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_middleorder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['wickets_tailender']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']/indexbowlerwithoutrunratechange[bowlers]['wickets_tailender']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_tailender']='NA'
    
    indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']={}
    if indexbowlerwithoutrunratechange[bowlers]['dotballs']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['overall']=round((indexbowlerwithoutrunratechange[bowlers]['balls']/indexbowlerwithoutrunratechange[bowlers]['dotballs']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['overall']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['dotballs_against_top_order']!=0:   
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']/indexbowlerwithoutrunratechange[bowlers]['dotballs_against_top_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_toporder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['dotballs_against_middle_order']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']/indexbowlerwithoutrunratechange[bowlers]['dotballs_against_middle_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_middleorder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['dotballs_against_tailender']!=0:    
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']/indexbowlerwithoutrunratechange[bowlers]['dotballs_against_tailender']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_tailender']='NA'
    
    indexdatawithoutrunratechange[bowlers]['balls_ratio']={}
    indexdatawithoutrunratechange[bowlers]['balls_ratio']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
    indexdatawithoutrunratechange[bowlers]['balls_ratio']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
    indexdatawithoutrunratechange[bowlers]['balls_ratio']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
#for x in list(indexdatawithoutrunratechange.keys()):
    
#    print(x,indexdatawithoutrunratechange[x])


# In[70]:


def rpbrating(rpb,mean_rpb):
    if rpb=='NA':
        return(mean_rpb)
    else:
        return(rpb)
    
def bpwrating(bpw,rpb,mean_bpw):
    if bpw=='NA' and rpb=='NA':
        return(mean_bpw)
    elif bpw=='NA':
        return(2*mean_bpw)
    else:
        return(bpw)
    
def bpdrating(bpd,rpb,mean_bpd):
    if bpd=='NA'and rpb=='NA':
        return(mean_bpd)
    elif bpd=='NA':
        return(2*mean_bpd)
    else:
        return(bpd)
def f_rpb(rpb,order):
    if order=='v_toporder':
        b_rpb_to.append(rpb)
    elif order=='v_middleorder':
        b_rpb_mo.append(rpb)
    elif order=='v_tailender':
        b_rpb_te.append(rpb)
        
def f_bpw(bpw,order):
    if order=='v_toporder':
        b_bpw_to.append(bpw)
    elif order=='v_middleorder':
        b_bpw_mo.append(bpw)
    elif order=='v_tailender':
        b_bpw_te.append(bpw)
        
def f_bpd(bpd,order):
    if order=='v_toporder':
        b_bpd_to.append(bpd)
    elif order=='v_middleorder':
        b_bpd_mo.append(bpd)
    elif order=='v_tailender':
        b_bpd_te.append(bpd)




rpb_to=0.54
rpb_mo=0.44
rpb_te=0.02
bpw_to=0.52
bpw_mo=0.47
bpw_te=0.01
bpd_to=0.57
bpd_mo=0.42
bpd_te=0.01

b_rpb_to=[]
b_rpb_mo=[]
b_rpb_te=[]
b_bpw_to=[]
b_bpw_mo=[]
b_bpw_te=[]
b_bpd_to=[]
b_bpd_mo=[]
b_bpd_te=[]

bowlerList=list(indexdatawithoutrunratechange.keys())
for bowlers in bowlerList:
    to=bowlersballratio[bowlers]['to']
    mo=bowlersballratio[bowlers]['mo']
    te=bowlersballratio[bowlers]['te']
    
    brpb_to=indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_toporder']
    
    
    brpb_mo=indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_middleorder']
    brpb_te=indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_tailender']
    
    bbpw_to=indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_toporder']
    bbpw_mo=indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_middleorder']
    bbpw_te=indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_tailender']
    
    bbpd_to=indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_toporder']
    bbpd_mo=indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_middleorder']
    bbpd_te=indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_tailender']
    
    f_rpb(brpb_to,'v_toporder')
    f_rpb(brpb_mo,'v_middleorder')
    f_rpb(brpb_te,'v_tailender')
    
    f_bpw(bbpw_to,'v_toporder')
    f_bpw(bbpw_mo,'v_middleorder')
    f_bpw(bbpw_te,'v_tailender')
    
    f_bpd(bbpd_to,'v_toporder')
    f_bpd(bbpd_mo,'v_middleorder')
    f_bpd(bbpd_te,'v_tailender')
    
#     print(b_bpw_te)
    
    mean_rpb_to=np.mean([val for val in b_rpb_to if val!='NA'])
    mean_rpb_mo=np.mean([val for val in b_rpb_mo if val!='NA'])
    mean_rpb_te=np.mean([val for val in b_rpb_te if val!='NA'])
    
    mean_bpw_to=np.mean([val for val in b_bpw_to if val!='NA'])
    mean_bpw_mo=np.mean([val for val in b_bpw_mo if val!='NA'])
#     print([val for val in b_bpw_te if val!='NA'])
    mean_bpw_te=np.mean([val for val in b_bpw_te if val!='NA'])
    
    mean_bpd_to=np.mean([val for val in b_bpd_to if val!='NA'])
    mean_bpd_mo=np.mean([val for val in b_bpd_mo if val!='NA'])
    mean_bpd_te=np.mean([val for val in b_bpd_te if val!='NA'])
    
    max_rpb_to=max([val for val in b_rpb_to if val!='NA'])
    max_rpb_mo=max([val for val in b_rpb_mo if val!='NA'])
    max_rpb_te=max([val for val in b_rpb_te if val!='NA'])
    
    max_bpw_to=max([val for val in b_bpw_to if val!='NA'])
    max_bpw_mo=max([val for val in b_bpw_mo if val!='NA'])
    if len(list(set(b_bpw_te).difference(set(['NA']))))>0:
        max_bpw_te=max([val for val in b_bpw_te if val!='NA'])
    
    max_bpd_to=max([val for val in b_bpd_to if val!='NA'])
    max_bpd_mo=max([val for val in b_bpd_mo if val!='NA'])
    max_bpd_te=max([val for val in b_bpd_te if val!='NA'])
    
    min_rpb_to=min([val for val in b_rpb_to if val!='NA'])
    min_rpb_mo=min([val for val in b_rpb_mo if val!='NA'])
    min_rpb_te=min([val for val in b_rpb_te if val!='NA'])
    
    min_bpw_to=min([val for val in b_bpw_to if val!='NA'])
    min_bpw_mo=min([val for val in b_bpw_mo if val!='NA'])
    if len(list(set(b_bpw_te).difference(set(['NA']))))>0:
        min_bpw_te=min([val for val in b_bpw_te if val!='NA'])
    
    min_bpd_to=min([val for val in b_bpd_to if val!='NA'])
    min_bpd_mo=min([val for val in b_bpd_mo if val!='NA'])
    min_bpd_te=min([val for val in b_bpd_te if val!='NA'])
    
#     finalperformancerating[bowlers]={}
#     finalperformancerating[bowlers]['rating_rpb']=round((rpbrating(brpb_to)*rpb_to*to+rpbrating(brpb_mo)*rpb_mo*mo+rpbrating(brpb_te)*rpb_te*te),3)
#     finalperformancerating[bowlers]['rating_bpw']=round((bpwrating(bbpw_to)*bpw_to*to+bpwrating(bbpw_mo)*bpw_mo*mo+bpwrating(bbpw_te)*bpw_te*te),3)
#     finalperformancerating[bowlers]['rating_bpd']=round((bpdrating(bbpd_to)*bpd_to*to+rpbrating(bbpd_mo)*bpd_mo*mo+rpbrating(bbpd_te)*bpd_te*te),3)
#     finalperformancerating[bowlers]['total_rating']=round((finalperformancerating[bowlers]['rating_rpb']+finalperformancerating[bowlers]['rating_bpw']+finalperformancerating[bowlers]['rating_bpd']),3)

    

#print(mean_bpw_to,mean_bpw_mo,mean_bpw_te)


# In[71]:


def weightageRPB(mean_rpb,bowler_rpb,order):
    if order=='v_toporder':
        if bowler_rpb<=mean_rpb:
            return(0.54)
        else:
            return(0.46)
        
    elif order=='v_middleorder':
        if bowler_rpb<=mean_rpb:
            return(0.44)
        else:
            return(0.56)
        
    elif order=='v_tailender':
        if bowler_rpb<=mean_rpb:
            return(0.02)
        else:
            return(0.98)
    
def weightageBPW(mean_bpw,bowler_bpw,order):
    if order=='v_toporder':
        if bowler_bpw<=mean_bpw:
            return(0.52)
        else:
            return(0.48)
        
    elif order=='v_middleorder':
        if bowler_bpw<=mean_bpw:
            return(0.47)
        else:
            return(0.53)
        
    elif order=='v_tailender':
        if bowler_bpw<=mean_bpw:
            return(0.01)
        else:
            return(0.99)
    
def weightageDPB(mean_dpb,bowler_dpb,order):
    if order=='v_toporder':
        if bowler_dpb<=mean_dpb:
            return(0.49)
        else:
            return(0.51)
        
    elif order=='v_middleorder':
        if bowler_dpb<=mean_dpb:
            return(0.46)
        else:
            return(0.54)
        
    elif order=='v_tailender':
        if bowler_dpb<=mean_dpb:
            return(0.05)
        else:
            return(0.95)


# In[72]:


finalperformancerating={}
for b in range(len(bowlerList)):
    
    tor=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_toporder']
    mor=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_middleorder']
    ter=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_tailender']
    
    tow=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_toporder']
    mow=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_middleorder']
    tew=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_tailender']
    
    tod=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_toporder']
    mod=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_middleorder']
    ted=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_tailender']
    
    finalperformancerating[bowlerList[b]]={}
    finalperformancerating[bowlerList[b]]['rating_rpb']=round((((rpbrating(tor,mean_rpb_to)-mean_rpb_to)/(max_rpb_to-min_rpb_to))*weightageRPB(mean_rpb_to,rpbrating(tor,mean_rpb_to),'v_toporder')                                                               +((rpbrating(mor,mean_rpb_mo)-mean_rpb_mo)/(max_rpb_mo-min_rpb_mo))*weightageRPB(mean_rpb_mo,rpbrating(mor,mean_rpb_mo),'v_middleorder')                                                               +((rpbrating(ter,mean_rpb_te)-mean_rpb_te)/(max_rpb_te-min_rpb_te))*weightageRPB(mean_rpb_te,rpbrating(ter,mean_rpb_te),'v_tailender')),3)
    finalperformancerating[bowlerList[b]]['rating_bpw']=round((((bpwrating(tow,tor,mean_bpw_to)-mean_bpw_to)/(max_bpw_to-min_bpw_to))*weightageBPW(mean_bpw_to,bpwrating(tow,tor,mean_bpw_to),'v_toporder')                                                               +((bpwrating(mow,mor,mean_bpw_mo)-mean_bpw_mo)/(max_bpw_mo-min_bpw_mo))*weightageBPW(mean_bpw_mo,bpwrating(mow,mor,mean_bpw_mo),'v_middleorder')                                                               +((bpwrating(tew,ter,mean_bpw_te)-mean_bpw_te)/(max_bpw_te-min_bpw_te))*weightageBPW(mean_bpw_te,bpwrating(tew,ter,mean_bpw_te),'v_tailender')),3)
    finalperformancerating[bowlerList[b]]['rating_bpd']=round((((bpdrating(tod,tor,mean_bpd_to)-mean_bpd_to)/(max_bpd_to-min_bpd_to))*weightageDPB(mean_bpd_to,bpdrating(tod,tor,mean_bpd_to),'v_toporder')                                                               +((bpdrating(mod,mor,mean_bpd_mo)-mean_bpd_mo)/(max_bpd_mo-min_bpd_mo))*weightageDPB(mean_bpd_mo,bpdrating(mod,mor,mean_bpd_mo),'v_middleorder')                                                               +((bpdrating(ted,ter,mean_bpd_te)-mean_bpd_te)/(max_bpd_te-min_bpd_te))*weightageDPB(mean_bpd_te,bpdrating(ted,ter,mean_bpd_te),'v_tailender')),3)
    finalperformancerating[bowlerList[b]]['total_rating']=round(((-1)*finalperformancerating[bowlerList[b]]['rating_rpb']                                                                 +(-1)*finalperformancerating[bowlerList[b]]['rating_bpw']                                                                 +(-1)*finalperformancerating[bowlerList[b]]['rating_bpd']),3)
    
    
#for bowlers in list(finalperformancerating.keys()):
#    print(bowlers,finalperformancerating[bowlers]['total_rating'])


# In[73]:


finalperformancerating={}
for b in range(len(bowlerList)):
    
    tor=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_toporder']
    mor=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_middleorder']
    ter=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_tailender']
    
    tow=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_toporder']
    mow=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_middleorder']
    tew=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_tailender']
    
    tod=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_toporder']
    mod=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_middleorder']
    ted=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_tailender']
    
    finalperformancerating[bowlerList[b]]={}
    finalperformancerating[bowlerList[b]]['rating_rpb']=round((((rpbrating(tor,mean_rpb_to)-mean_rpb_to)/(max_rpb_to-min_rpb_to))*weightageRPB(mean_rpb_to,rpbrating(tor,mean_rpb_to),'v_toporder')                                                               +((rpbrating(mor,mean_rpb_mo)-mean_rpb_mo)/(max_rpb_mo-min_rpb_mo))*weightageRPB(mean_rpb_mo,rpbrating(mor,mean_rpb_mo),'v_middleorder')                                                               +((rpbrating(ter,mean_rpb_te)-mean_rpb_te)/(max_rpb_te-min_rpb_te))*weightageRPB(mean_rpb_te,rpbrating(ter,mean_rpb_te),'v_tailender')),3)
    finalperformancerating[bowlerList[b]]['rating_bpw']=round((((bpwrating(tow,tor,mean_bpw_to)-mean_bpw_to)/(max_bpw_to-min_bpw_to))*weightageBPW(mean_bpw_to,bpwrating(tow,tor,mean_bpw_to),'v_toporder')                                                               +((bpwrating(mow,mor,mean_bpw_mo)-mean_bpw_mo)/(max_bpw_mo-min_bpw_mo))*weightageBPW(mean_bpw_mo,bpwrating(mow,mor,mean_bpw_mo),'v_middleorder')                                                               +((bpwrating(tew,ter,mean_bpw_te)-mean_bpw_te)/(max_bpw_te-min_bpw_te))*weightageBPW(mean_bpw_te,bpwrating(tew,ter,mean_bpw_te),'v_tailender')),3)
    finalperformancerating[bowlerList[b]]['rating_bpd']=round((((bpdrating(tod,tor,mean_bpd_to)-mean_bpd_to)/(max_bpd_to-min_bpd_to))*weightageDPB(mean_bpd_to,bpdrating(tod,tor,mean_bpd_to),'v_toporder')                                                               +((bpdrating(mod,mor,mean_bpd_mo)-mean_bpd_mo)/(max_bpd_mo-min_bpd_mo))*weightageDPB(mean_bpd_mo,bpdrating(mod,mor,mean_bpd_mo),'v_middleorder')                                                               +((bpdrating(ted,ter,mean_bpd_te)-mean_bpd_te)/(max_bpd_te-min_bpd_te))*weightageDPB(mean_bpd_te,bpdrating(ted,ter,mean_bpd_te),'v_tailender')),3)
    finalperformancerating[bowlerList[b]]['total_rating']=round(((-1)*finalperformancerating[bowlerList[b]]['rating_rpb']                                                                 +(-1)*finalperformancerating[bowlerList[b]]['rating_bpw']                                                                 +(-1)*finalperformancerating[bowlerList[b]]['rating_bpd']),3)
    
    
#for bowlers in list(finalperformancerating.keys()):
#    print(bowlers,finalperformancerating[bowlers]['total_rating'])


# In[74]:


WC_totalrating=[]
for bowlers in list(finalperformancerating.keys()):
    WC_totalrating.append([bowlers,finalperformancerating[bowlers]['total_rating']])
    
WC_totalrating.sort(key=lambda x:x[1],reverse=True)
#print(WC_totalrating)


# In[75]:


performancerating={}
for i in WC_totalrating:
    performancerating[i[0]]={}
    performancerating[i[0]]['performancerating']=i[1]
#print(performancerating)


# In[76]:


scaledrating={}
Lsituation=[]
Lperformance=[]
Ldifficulty=[]

for i in list(situationindex.keys()):
    Lsituation.append(situationindex[i]['situation_rating'])
    Lperformance.append(performancerating[i]['performancerating'])
    Ldifficulty.append(bowlersballratio[i]['difficulty'])
for j in list(situationindex.keys()):
    scaledrating[j]={}
    scaledrating[j]['situation']=round(((situationindex[j]['situation_rating']-min(Lsituation))/(max(Lsituation)-min(Lsituation))),2)
    scaledrating[j]['performance']=round(((performancerating[j]['performancerating']-min(Lperformance))/(max(Lperformance)-min(Lperformance))),2)
    scaledrating[j]['difficulty']=round(((bowlersballratio[j]['difficulty']-min(Ldifficulty))/(max(Ldifficulty)-min(Ldifficulty))),2)

#print(scaledrating)


# In[77]:


totalrating={}

for i in list(scaledrating.keys()):
    totalrating[i]={}
    totalrating[i]['ratingpts']=scaledrating[i]['situation']+scaledrating[i]['performance']+scaledrating[i]['difficulty']
    totalrating[i]['overall_stats']=indexbowlerwithoutrunratechange[i]
    totalrating[i]['perball_overall_stats']=indexdatawithoutrunratechange[i]
#print(totalrating)


# In[78]:


overallrating=[]
for i in list(totalrating.keys()):
    overallrating.append([i,totalrating[i]['ratingpts'],totalrating[i]['overall_stats'],totalrating[i]['perball_overall_stats']])
    
#print(tabulate(overallrating))
#print(overallrating[1][0])


# In[79]:


overallrating.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(overallrating))


# In[80]:


import csv
filecursor=open('middleoversranking2_outfile.csv','w')
csvWriter=csv.writer(filecursor)
csvWriter.writerow(['bowler','rating','total_balls','ballsVtoporder','ballsVmiddleorder','ballsVtailender','overallRPB','RPBvtoporder','RPBvmiddleorder',                   'RPBvtailender','wickets','wktVtoporder','wktVmiddleorder','wktVtailender','overallBPW','BPWvToporder','BPWvmiddleorder','BPWvtailender',                   'performancerating','situationrating','difficultyfaced'])
for bowler in overallrating:
    csvWriter.writerow([bowler[0],totalrating[bowler[0]]['ratingpts'],indexbowlerwithoutrunratechange[bowler[0]]['balls'],                       indexbowlerwithoutrunratechange[bowler[0]]['balls_against_top_order'],indexbowlerwithoutrunratechange[bowler[0]]['balls_against_middle_order'],                      indexbowlerwithoutrunratechange[bowler[0]]['balls_against_tailender'],indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['overall'],                       indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['v_toporder'],indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['v_middleorder'],                       indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['v_tailender'],indexbowlerwithoutrunratechange[bowler[0]]['wickets'],                       indexbowlerwithoutrunratechange[bowler[0]]['wickets_toporder'],indexbowlerwithoutrunratechange[bowler[0]]['wickets_middleorder'],                        indexbowlerwithoutrunratechange[bowler[0]]['wickets_tailender'],indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['overall'],                       indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['v_toporder'],indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['v_middleorder'],                       indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['v_tailender'],scaledrating[bowler[0]]['performance'],scaledrating[bowler[0]]['situation'],                       scaledrating[bowler[0]]['difficulty']])
filecursor.close()


# In[81]:


overallratingfinal=[]
for i in list(totalrating.keys()):
    overallratingfinal.append([i,totalrating[i]['ratingpts']])
    
#print(tabulate(overallratingfinal))


# In[82]:


overallratingfinal.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(overallratingfinal))


# In[83]:


for i in overallratingfinal:
    i.append(overallratingfinal.index(i)+1)


# In[84]:


overallratingfinal.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(overallratingfinal))


# In[85]:


situation=[]
performance=[]
difficulty=[]
for i in list(situationindex.keys()):
    situation.append([i,situationindex[i]['situation_rating']])
    performance.append([i,performancerating[i]['performancerating']])
    difficulty.append([i,bowlersballratio[i]['difficulty']])
situation.sort(key=lambda x:x[1],reverse=True)
performance.sort(key=lambda x:x[1],reverse=True)
difficulty.sort(key=lambda x:x[1],reverse=True)


# In[86]:


Lsituation=[]
Lperformance=[]
Ldifficulty=[]
for x in range(len(situation)):
    Lsituation.append(situation[x])
    Lperformance.append(performance[x])
    Ldifficulty.append(difficulty[x])
    


# In[87]:


L=["NA",'NA']
#print(len(list(set(L).difference(set(['NA']))))>0)


# In[88]:


with open('bowlerdatabase__allMatches.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    allBowlersInfo = unpickler.load() 
    file.close()


# Third Index

# Power Play Overs

# In[89]:



powerplayslist=[]
for i in matches:
    thismatchdata={}
    match_key=list(i.keys())[0]
    thismatchdata[match_key]={}
    for innings in list(i[match_key].keys()):
        
        thismatchdata[match_key][innings]={}
        thismatchdata[match_key][innings]['power_plays_ballbyball']={}
        balls_key=list(i[match_key][innings].keys())
        for balls in balls_key:
            if balls==119:
                runs_before_power_plays=i[match_key][innings][balls]['score']
                Rem_wickets_before_power_plays=i[match_key][innings][balls]['Rem_wicket']
                
            if balls==max(84,min(balls_key)):
                runs_after_power_plays=i[match_key][innings][balls]['score']
                Rem_wickets_after_power_plays=i[match_key][innings][balls]['Rem_wicket']
                
            
                
            bowler=i[match_key][innings][balls]['bowler']
            score=i[match_key][innings][balls]['score']
            Rem_wicket=i[match_key][innings][balls]['Rem_wicket']
            batting_order=i[match_key][innings][balls]['batting_order_of_batsman']
            thisballrun=i[match_key][innings][balls]['thisballrun']
            if balls == 119:
                before_death_winProb_1stBatTeam=getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=84,wicketRem=Rem_wicket))
            if min(balls_key)<119:
                if balls<119 and balls>=max(84,min(balls_key)):
                    thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]={}
                    thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['runs']=score
                    thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['thisballrun']=thisballrun
                    thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['Rem_wkts']=Rem_wicket
                    thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['bowler']=bowler
                    thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['batting_order_bowled']=batting_order
                    if i[match_key][innings][balls]['bowlerwicket']=='yes':
                        thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['bowlerwicket']='yes'
                    else:
                        thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['bowlerwicket']='no'
                    thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['winProb_1stBatTeam']=                                                        getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=balls,wicketRem=Rem_wicket))
                    thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['before_death_winProb_1stBatTeam']=before_death_winProb_1stBatTeam
                    if balls==118:
                        thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']=                        round((before_death_winProb_1stBatTeam-getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=118,wicketRem=Rem_wicket))),2)
                        if thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']>=0:
                            thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                            round(((before_death_winProb_1stBatTeam-                                    getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=118,wicketRem=Rem_wicket)))*before_death_winProb_1stBatTeam),2)
                            
                        else:
                            thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                            round(((before_death_winProb_1stBatTeam-                                    getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=118,wicketRem=Rem_wicket)))*(100-before_death_winProb_1stBatTeam)),2)
                    else:
                        thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']=                            round((getWinProb(i[match_key][innings][balls+1]['score']+                                          get_OLS_ballByBallDict_catWickRem(ballRem=balls+1,wicketRem=i[match_key][innings][balls+1]['Rem_wicket']))-                                           getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=balls,wicketRem=Rem_wicket))),2)
                        if thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']>=0:
                            thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                                thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']*                                thismatchdata[match_key][innings]['power_plays_ballbyball'][balls+1]['winProb_1stBatTeam']
                        else:
                            thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']=                                thismatchdata[match_key][innings]['power_plays_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']*                                (100-thismatchdata[match_key][innings]['power_plays_ballbyball'][balls+1]['winProb_1stBatTeam'])
                            
                            
#                             round(((getWinProb(i[match_key][innings][balls+1]['score']+\
#                                            get_OLS_ballByBallDict_catWickRem(ballRem=balls+1,wicketRem=i[match_key][innings][balls+1]['Rem_wicket']))-\
#                                 getWinProb(score+get_OLS_ballByBallDict_catWickRem(ballRem=balls,wicketRem=Rem_wicket)))*((getWinProb(i[match_key][innings][balls+1]['score']+\
#                         get_OLS_ballByBallDict_catWickRem(ballRem=balls+1,wicketRem=i[match_key][innings][balls+1]['Rem_wicket'])))),2)
            else:
                thismatchdata[match_key][innings]['power_plays_ballbyball']='inngs over before 1st over'
            if balls==119:
                thismatchdata[match_key][innings]['run_rate_before_powerplays']=round((runs_before_power_plays/1),2)
        
        if min(balls_key)<119:
            if (119-min(balls_key))!=0:
                thismatchdata[match_key][innings]['run_rate_in_powerplays']=round((runs_after_power_plays-runs_before_power_plays)/(119-max(84,min(balls_key))),2)
        else:
            thismatchdata[match_key][innings]['run_rate_in_powerplays']='NA'
            
        if min(balls_key)<119:
            if (Rem_wickets_before_power_plays-Rem_wickets_after_power_plays)!=0:
                thismatchdata[match_key][innings]['runs_per_wicket_during_powerplays']=round(((runs_after_power_plays-runs_before_power_plays)/                                                                                         (Rem_wickets_before_power_plays-Rem_wickets_after_power_plays)),2)
            else:
                thismatchdata[match_key][innings]['runs_per_wicket_during_powerplays']='NA'
        else:
            thismatchdata[match_key][innings]['runs_per_wicket_during_powerplays']='NA'
            
    powerplayslist.append(thismatchdata)


# In[90]:


#powerplayslist


# In[91]:


battingorderseverity={}
battingorderseverity['top_order']={}
battingorderseverity['top_order']['balls']=0
battingorderseverity['top_order']['runs']=0
battingorderseverity['top_order']['dot_balls']=0
battingorderseverity['top_order']['wickets']=0
battingorderseverity['middle_order']={}
battingorderseverity['middle_order']['balls']=0
battingorderseverity['middle_order']['runs']=0
battingorderseverity['middle_order']['dot_balls']=0
battingorderseverity['middle_order']['wickets']=0
battingorderseverity['tail_ender']={}
battingorderseverity['tail_ender']['balls']=0
battingorderseverity['tail_ender']['runs']=0
battingorderseverity['tail_ender']['dot_balls']=0
battingorderseverity['tail_ender']['wickets']=0
for i in powerplayslist:
    match_key=list(i.keys())[0]
    for x in list(i[match_key].keys()):
    
        if i[match_key][x]['power_plays_ballbyball']=='inngs over before 1st over':
            continue
        balls_key=list(i[match_key][x]['power_plays_ballbyball'].keys())

        for balls in balls_key: 
            if i[match_key][x]['power_plays_ballbyball'][balls]['batting_order_bowled']<=3:
                battingorderseverity['top_order']['balls']+=1
                battingorderseverity['top_order']['runs']+=i[match_key][x]['power_plays_ballbyball'][balls]['thisballrun']
                battingorderseverity['top_order']['runs_per_ball']=round((battingorderseverity['top_order']['runs']/battingorderseverity['top_order']['balls']),2)
                if i[match_key][x]['power_plays_ballbyball'][balls]['thisballrun']==0:
                    battingorderseverity['top_order']['dot_balls']+=1
                if i[match_key][x]['power_plays_ballbyball'][balls]['bowlerwicket']=='yes':
                    battingorderseverity['top_order']['wickets']+=1
            if i[match_key][x]['power_plays_ballbyball'][balls]['batting_order_bowled']>=4 and i[match_key][x]['power_plays_ballbyball'][balls]['batting_order_bowled']<=7:

                battingorderseverity['middle_order']['balls']+=1
                battingorderseverity['middle_order']['runs']+=i[match_key][x]['power_plays_ballbyball'][balls]['thisballrun']
                battingorderseverity['middle_order']['runs_per_ball']=round((battingorderseverity['middle_order']['runs']/battingorderseverity['middle_order']['balls']),2)
                if i[match_key][x]['power_plays_ballbyball'][balls]['thisballrun']==0:
                    battingorderseverity['middle_order']['dot_balls']+=1
                if i[match_key][x]['power_plays_ballbyball'][balls]['bowlerwicket']=='yes':
                    battingorderseverity['middle_order']['wickets']+=1
            if i[match_key][x]['power_plays_ballbyball'][balls]['batting_order_bowled']>=8 and i[match_key][x]['power_plays_ballbyball'][balls]['batting_order_bowled']<=11:

                battingorderseverity['tail_ender']['balls']+=1
                battingorderseverity['tail_ender']['runs']+=i[match_key][x]['power_plays_ballbyball'][balls]['thisballrun']
                battingorderseverity['tail_ender']['runs_per_ball']=round((battingorderseverity['tail_ender']['runs']/battingorderseverity['tail_ender']['balls']),2)
                if i[match_key][x]['power_plays_ballbyball'][balls]['thisballrun']==0:
                    battingorderseverity['tail_ender']['dot_balls']+=1
                if i[match_key][x]['power_plays_ballbyball'][balls]['bowlerwicket']=='yes':
                    battingorderseverity['tail_ender']['wickets']+=1
#print(battingorderseverity)


# In[92]:


battingorderseverity_ratio={}
totalruns_in_death_overs=battingorderseverity['top_order']['runs']+battingorderseverity['middle_order']['runs']+battingorderseverity['tail_ender']['runs']
totalballs_in_death_overs=battingorderseverity['top_order']['balls']+battingorderseverity['middle_order']['balls']+battingorderseverity['tail_ender']['balls']
totaruns_per_ball=round((totalruns_in_death_overs/totalballs_in_death_overs),2)
battingorderseverity_ratio['top_order']=round((battingorderseverity['top_order']['runs_per_ball']/totaruns_per_ball),2)
battingorderseverity_ratio['middle_order']=round((battingorderseverity['middle_order']['runs_per_ball']/totaruns_per_ball),2)
# battingorderseverity_ratio['tail_ender']=round((battingorderseverity['tail_ender']['runs_per_ball']/totaruns_per_ball),2)

battingorderseverity_ratio['balls_per_dot_top_order']=round((battingorderseverity['top_order']['balls']/battingorderseverity['top_order']['dot_balls']),2)
battingorderseverity_ratio['balls_per_dot_middle_order']=round((battingorderseverity['middle_order']['balls']/battingorderseverity['middle_order']['dot_balls']),2)
# battingorderseverity_ratio['balls_per_dot_tail_ender']=round((battingorderseverity['tail_ender']['balls']/battingorderseverity['tail_ender']['dot_balls']),2)

battingorderseverity_ratio['balls_per_wicket_top_order']=round((battingorderseverity['top_order']['balls']/battingorderseverity['top_order']['wickets']),2)
battingorderseverity_ratio['balls_per_wicket_middle_order']=round((battingorderseverity['middle_order']['balls']/battingorderseverity['middle_order']['wickets']),2)
# battingorderseverity_ratio['balls_per_wicket_tail_ender']=round((battingorderseverity['tail_ender']['balls']/battingorderseverity['tail_ender']['wickets']),2)

total_dot_balls=battingorderseverity_ratio['balls_per_dot_top_order']+battingorderseverity_ratio['balls_per_dot_middle_order']
total_balls_per_dot_in_death=round((totalballs_in_death_overs/total_dot_balls),2)
total_balls_per_wicket=battingorderseverity_ratio['balls_per_wicket_top_order']+battingorderseverity_ratio['balls_per_wicket_middle_order']
battingorderseverity_ratio['dot_ball_ratio_top_order']=round((battingorderseverity_ratio['balls_per_dot_top_order']/total_dot_balls),2)
battingorderseverity_ratio['dot_ball_ratio_middle_order']=round((battingorderseverity_ratio['balls_per_dot_middle_order']/total_dot_balls),2)
# battingorderseverity_ratio['dot_ball_ratio_tailender']=round((battingorderseverity_ratio['balls_per_dot_tail_ender']/total_dot_balls),2)

battingorderseverity_ratio['bpw_ratio_top_order']=round((battingorderseverity_ratio['balls_per_wicket_top_order']/total_balls_per_wicket),2)
battingorderseverity_ratio['bpw_ratio_middle_order']=round((battingorderseverity_ratio['balls_per_wicket_middle_order']/total_balls_per_wicket),2)
# battingorderseverity_ratio['bpw_ratio_tailender']=round((battingorderseverity_ratio['balls_per_wicket_tail_ender']/total_balls_per_wicket),2)


#print(battingorderseverity_ratio)


# In[93]:


bowlers_in_power_plays={}
for i in powerplayslist:
    match_keys=list(i.keys())[0]
    for y in list(i[match_keys].keys()):
        if i[match_keys][y]['power_plays_ballbyball']=='inngs over before 1st over':
            continue
        balls_key=list(i[match_keys][y]['power_plays_ballbyball'].keys())

        for balls in balls_key:

            bowler=i[match_keys][y]['power_plays_ballbyball'][balls]['bowler']
            if bowler not in list(bowlers_in_power_plays.keys()):
                bowlers_in_power_plays[bowler]={}
                bowlers_in_power_plays[bowler]['balls']=0
                bowlers_in_power_plays[bowler]['change_in_WP']=0
                bowlers_in_power_plays[bowler]['weightedchange_in_WP']=0

            for bowlers in list(bowlers_in_power_plays.keys()):
                if bowlers==bowler:
                    bowlers_in_power_plays[bowler]['balls']+=1
                    bowlers_in_power_plays[bowler]['change_in_WP']+=i[match_keys][y]['power_plays_ballbyball'][balls]['thisball_change_in_winProb_1stBatTeam']
                    bowlers_in_power_plays[bowler]['weightedchange_in_WP']+=i[match_keys][y]['power_plays_ballbyball'][balls]['weighted_thisball_change_in_winProb_1stBatTeam']
                    bowlers_in_power_plays[bowler]['change_in_WP_perball']=round((bowlers_in_power_plays[bowler]['change_in_WP']/bowlers_in_power_plays[bowler]['balls']),2)
                    bowlers_in_power_plays[bowler]['weightedchange_in_WP_perball']=round((bowlers_in_power_plays[bowler]['weightedchange_in_WP']/bowlers_in_power_plays[bowler]['balls']),2)
    significantbowlers=[]
for x in list(bowlers_in_power_plays.keys()):
    if bowlers_in_power_plays[x]['balls']>=120:
        significantbowlers.append([x,bowlers_in_power_plays[x]['change_in_WP_perball'],(bowlers_in_power_plays[x]['weightedchange_in_WP_perball'])/100,bowlers_in_power_plays[x]['balls']])
        


# In[94]:


#significantbowlers


# In[95]:


filtered_significantbowlers=[i for i in significantbowlers if i[3]>=120]


# In[96]:


situationindex={}
for i in filtered_significantbowlers:
    situationindex[i[0]]={}
    situationindex[i[0]]['situation_rating']=i[2]
#print(situationindex)


# In[97]:


filtered_significantbowlers.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(filtered_significantbowlers))


# In[98]:


filtered_significantbowlers.sort(key=lambda x:x[2],reverse=True)
#print(tabulate(filtered_significantbowlers))


# In[99]:


matches_for_runrate_drop_in_middleovers_infirstinnings=[]
matches_for_runrate_drop_in_middleovers_insecondinnings=[]
for i in middleoverslist:

    for j in list(i.keys()):
        innings_key=list(i[j].keys())
        if i[j][innings_key[0]]['run_rate_in_middleovers']!='NA':
            if i[j][innings_key[0]]['run_rate_before_middleovers']>i[j][innings_key[0]]['run_rate_in_middleovers']:
                matches_for_runrate_drop_in_middleovers_infirstinnings.append(i)
            
        if i[j][innings_key[1]]['run_rate_in_middleovers']!='NA':
            if i[j][innings_key[1]]['run_rate_before_middleovers']>i[j][innings_key[1]]['run_rate_in_middleovers']:
                matches_for_runrate_drop_in_middleovers_insecondinnings.append(i)
                
runratedrop={}
runratedrop['1st innings']=matches_for_runrate_drop_in_middleovers_infirstinnings
runratedrop['2nd innings']=matches_for_runrate_drop_in_middleovers_insecondinnings


# In[100]:


#len(matches_for_runrate_drop_in_middleovers_insecondinnings)


# In[101]:


matchdataofmiddleoversfirstinnings=[]
matchdataofmiddleoverssecondinnings=[]
for i in middleoverslist:

    for j in list(i.keys()):
        innings_key=list(i[j].keys())
        if i[j][innings_key[0]]['run_rate_in_middleovers']!='NA':
            
            matchdataofmiddleoversfirstinnings.append(i)
            
        if i[j][innings_key[1]]['run_rate_in_middleovers']!='NA':
            
            matchdataofmiddleoverssecondinnings.append(i)
                
matchdata={}
matchdata['1st innings']=matchdataofmiddleoversfirstinnings
matchdata['2nd innings']=matchdataofmiddleoverssecondinnings


# In[102]:


#powerplayslist


# In[103]:


bowlersdatawithoutrunratechange={}

for i in powerplayslist:
    match_keys=list(i.keys())[0]
    for y in list(i[match_keys].keys()):
        if i[match_keys][y]['power_plays_ballbyball']=='inngs over before 1st over':
            continue
        
        balls_key=list(i[match_keys][y]['power_plays_ballbyball'].keys())

        for balls in balls_key:

            bowler=i[match_keys][y]['power_plays_ballbyball'][balls]['bowler']
  
            if bowler not in list(bowlersdatawithoutrunratechange.keys()):
                bowlersdatawithoutrunratechange[bowler]={}
                bowlersdatawithoutrunratechange[bowler]['balls']=0
                bowlersdatawithoutrunratechange[bowler]['balls_against_top_order']=0
                bowlersdatawithoutrunratechange[bowler]['balls_against_middle_order']=0
                bowlersdatawithoutrunratechange[bowler]['balls_against_tailender']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs_against_top_order']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs_against_middle_order']=0
                bowlersdatawithoutrunratechange[bowler]['dotballs_against_tailender']=0

                bowlersdatawithoutrunratechange[bowler]['runs']=0
                bowlersdatawithoutrunratechange[bowler]['runsagainst_top_order']=0
                bowlersdatawithoutrunratechange[bowler]['runsaginst_middle_order']=0
                bowlersdatawithoutrunratechange[bowler]['runsagainst_tailender']=0
                bowlersdatawithoutrunratechange[bowler]['wickets']=0
                bowlersdatawithoutrunratechange[bowler]['wickets_toporder']=0
                bowlersdatawithoutrunratechange[bowler]['wickets_middleorder']=0
                bowlersdatawithoutrunratechange[bowler]['wickets_tailender']=0
                bowlersdatawithoutrunratechange[bowler]['batting_order_dismissed']=[]

            for bowlers in list(bowlersdatawithoutrunratechange.keys()):
                if bowlers==bowler:
                    bowlersdatawithoutrunratechange[bowler]['balls']+=1
                    bowlersdatawithoutrunratechange[bowler]['runs']+=i[match_keys][y]['power_plays_ballbyball'][balls]['thisballrun']
                    if i[match_keys][y]['power_plays_ballbyball'][balls]['bowlerwicket']=='yes':
                        bowlersdatawithoutrunratechange[bowler]['wickets']+=1
                        bowlersdatawithoutrunratechange[bowler]['batting_order_dismissed'].append(i[match_keys][y]['power_plays_ballbyball'][balls]['batting_order_bowled'])
                    if i[match_keys][y]['power_plays_ballbyball'][balls]['thisballrun']==0:
                        bowlersdatawithoutrunratechange[bowler]['dotballs']+=1
                    if i[match_keys][y]['power_plays_ballbyball'][balls]['batting_order_bowled']>=1                    and i[match_keys][y]['power_plays_ballbyball'][balls]['batting_order_bowled']<=3:
                        bowlersdatawithoutrunratechange[bowler]['balls_against_top_order']+=1
                        bowlersdatawithoutrunratechange[bowler]['runsagainst_top_order']+=i[match_keys][y]['power_plays_ballbyball'][balls]['thisballrun']
                        if i[match_keys][y]['power_plays_ballbyball'][balls]['bowlerwicket']=='yes':
                            bowlersdatawithoutrunratechange[bowler]['wickets_toporder']+=1
                        if i[match_keys][y]['power_plays_ballbyball'][balls]['thisballrun']==0:
                            bowlersdatawithoutrunratechange[bowler]['dotballs_against_top_order']+=1

                    if i[match_keys][y]['power_plays_ballbyball'][balls]['batting_order_bowled']>=4                    and i[match_keys][y]['power_plays_ballbyball'][balls]['batting_order_bowled']<=7:
                        bowlersdatawithoutrunratechange[bowler]['balls_against_middle_order']+=1
                        bowlersdatawithoutrunratechange[bowler]['runsaginst_middle_order']+=i[match_keys][y]['power_plays_ballbyball'][balls]['thisballrun']
                        if i[match_keys][y]['power_plays_ballbyball'][balls]['bowlerwicket']=='yes':
                            bowlersdatawithoutrunratechange[bowler]['wickets_middleorder']+=1
                        if i[match_keys][y]['power_plays_ballbyball'][balls]['thisballrun']==0:
                            bowlersdatawithoutrunratechange[bowler]['dotballs_against_middle_order']+=1

                    if i[match_keys][y]['power_plays_ballbyball'][balls]['batting_order_bowled']>=8                    and i[match_keys][y]['power_plays_ballbyball'][balls]['batting_order_bowled']<=11:
                        bowlersdatawithoutrunratechange[bowler]['balls_against_tailender']+=1
                        bowlersdatawithoutrunratechange[bowler]['runsagainst_tailender']+=i[match_keys][y]['power_plays_ballbyball'][balls]['thisballrun']
                        if i[match_keys][y]['power_plays_ballbyball'][balls]['bowlerwicket']=='yes':
                            bowlersdatawithoutrunratechange[bowler]['wickets_tailender']+=1
                        if i[match_keys][y]['power_plays_ballbyball'][balls]['thisballrun']==0:
                            bowlersdatawithoutrunratechange[bowler]['dotballs_against_tailender']+=1
#print(bowlersdatawithoutrunratechange)


# In[104]:


indexbowlerwithoutrunratechange={}
for bowler in list(bowlersdatawithoutrunratechange.keys()):
    
    if bowlersdatawithoutrunratechange[bowler]['balls']>=120:
        indexbowlerwithoutrunratechange[bowler]={}
        indexbowlerwithoutrunratechange[bowler]=bowlersdatawithoutrunratechange[bowler]
#print(indexbowlerwithoutrunratechange)


# In[105]:


totalballs_per_bowler=[]
totalballs_per_bowler_toporder=[]
totalballs_per_bowler_middleorder=[]
totalballs_per_bowler_tailender=[]
for i in list(indexbowlerwithoutrunratechange.keys()):
    totalballs_per_bowler.append(indexbowlerwithoutrunratechange[i]['balls'])
    totalballs_per_bowler_toporder.append(indexbowlerwithoutrunratechange[i]['balls_against_top_order'])
    totalballs_per_bowler_middleorder.append(indexbowlerwithoutrunratechange[i]['balls_against_middle_order'])
    totalballs_per_bowler_tailender.append(indexbowlerwithoutrunratechange[i]['balls_against_tailender'])
#print(np.mean(totalballs_per_bowler),np.mean(totalballs_per_bowler_toporder),np.mean(totalballs_per_bowler_middleorder),np.mean(totalballs_per_bowler_tailender))
#print(totalballs_per_bowler)


# In[106]:


bowlersballratio={}
total_to_balls=sum(totalballs_per_bowler_toporder)
print(total_to_balls)
total_mo_balls=sum(totalballs_per_bowler_middleorder)
total_te_balls=sum(totalballs_per_bowler_tailender)
for i in list(indexbowlerwithoutrunratechange.keys()):
    bb_toporder=indexbowlerwithoutrunratechange[i]['balls_against_top_order']
    bb_midorder=indexbowlerwithoutrunratechange[i]['balls_against_middle_order']
    bb_tailender=indexbowlerwithoutrunratechange[i]['balls_against_tailender']
    bb_total=indexbowlerwithoutrunratechange[i]['balls']
    TO_mean=np.mean(totalballs_per_bowler_toporder)
    MO_mean=np.mean(totalballs_per_bowler_middleorder)
    TE_mean=np.mean(totalballs_per_bowler_tailender)
    TB_mean=np.mean(totalballs_per_bowler)
    bowlersballratio[i]={}
    bowlersballratio[i]['to']=(bb_toporder/total_to_balls)*(bb_toporder/bb_total)/0.86
    bowlersballratio[i]['mo']=(bb_midorder/total_mo_balls)*(bb_midorder/bb_total)/0.13
    bowlersballratio[i]['te']=0
    bowlersballratio[i]['difficulty_rpb']=0.75*(bowlersballratio[i]['to'])+0.24*bowlersballratio[i]['mo']+0.01*bowlersballratio[i]['te']
    bowlersballratio[i]['difficulty_bpw']=0.85*(bowlersballratio[i]['to'])+0.14*bowlersballratio[i]['mo']+0.01*bowlersballratio[i]['te']
    bowlersballratio[i]['difficulty_bpd']=0.87*(bowlersballratio[i]['to'])+0.12*bowlersballratio[i]['mo']+0.01*bowlersballratio[i]['te']
    bowlersballratio[i]['difficulty']=bowlersballratio[i]['difficulty_rpb']+bowlersballratio[i]['difficulty_bpw']+bowlersballratio[i]['difficulty_bpd']
#     bowlersballratio[i]['br_tor']=round(((bb_toporder-TO_mean)/(bb_total-TB_mean)),2)
#     bowlersballratio[i]['br_mor']=round(((bb_midorder-MO_mean)/(bb_total-TB_mean)),2)
#     bowlersballratio[i]['br_ter']=round(((bb_tailender-TE_mean)/(bb_total-TB_mean)),2)
#for x in list(bowlersballratio.keys()) :   
#    print(x,bowlersballratio[x])


# In[107]:


indexdatawithoutrunratechange={}
for bowlers in list(indexbowlerwithoutrunratechange.keys()):
    indexdatawithoutrunratechange[bowlers]={}
    indexdatawithoutrunratechange[bowlers]['runs_per_ball']={}
    indexdatawithoutrunratechange[bowlers]['runs_per_ball']['overall']=round((indexbowlerwithoutrunratechange[bowlers]['runs']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
    if indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']!=0:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['runsagainst_top_order']/indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_toporder']='NA'
    
    if indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']!=0:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['runsaginst_middle_order']/indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_middleorder']='NA'
        
    if indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']!=0:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['runsagainst_tailender']/indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_tailender']='NA'
        
    indexdatawithoutrunratechange[bowlers]['wickets']={}
    indexdatawithoutrunratechange[bowlers]['wickets']['overall']=indexbowlerwithoutrunratechange[bowlers]['wickets']
    indexdatawithoutrunratechange[bowlers]['wickets']['v_toporder']=indexbowlerwithoutrunratechange[bowlers]['wickets_toporder']
    indexdatawithoutrunratechange[bowlers]['wickets']['v_middleorder']=indexbowlerwithoutrunratechange[bowlers]['wickets_middleorder']
    indexdatawithoutrunratechange[bowlers]['wickets']['v_tailender']=indexbowlerwithoutrunratechange[bowlers]['wickets_tailender']
    
    indexdatawithoutrunratechange[bowlers]['balls_per_wickets']={}
    indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['overall']=round((indexbowlerwithoutrunratechange[bowlers]['balls']/indexbowlerwithoutrunratechange[bowlers]['wickets']),2)
    if indexbowlerwithoutrunratechange[bowlers]['wickets_toporder']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']/indexbowlerwithoutrunratechange[bowlers]['wickets_toporder']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_toporder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['wickets_middleorder']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']/indexbowlerwithoutrunratechange[bowlers]['wickets_middleorder']),2)
    else:   
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_middleorder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['wickets_tailender']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']/indexbowlerwithoutrunratechange[bowlers]['wickets_tailender']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_tailender']='NA'
    
    indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']={}
    if indexbowlerwithoutrunratechange[bowlers]['dotballs']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['overall']=round((indexbowlerwithoutrunratechange[bowlers]['balls']/indexbowlerwithoutrunratechange[bowlers]['dotballs']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['overall']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['dotballs_against_top_order']!=0:   
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']/indexbowlerwithoutrunratechange[bowlers]['dotballs_against_top_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_toporder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['dotballs_against_middle_order']!=0:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']/indexbowlerwithoutrunratechange[bowlers]['dotballs_against_middle_order']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_middleorder']='NA'
    if indexbowlerwithoutrunratechange[bowlers]['dotballs_against_tailender']!=0:    
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']/indexbowlerwithoutrunratechange[bowlers]['dotballs_against_tailender']),2)
    else:
        indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_tailender']='NA'
    
    indexdatawithoutrunratechange[bowlers]['balls_ratio']={}
    indexdatawithoutrunratechange[bowlers]['balls_ratio']['v_toporder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_top_order']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
    indexdatawithoutrunratechange[bowlers]['balls_ratio']['v_middleorder']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_middle_order']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
    indexdatawithoutrunratechange[bowlers]['balls_ratio']['v_tailender']=round((indexbowlerwithoutrunratechange[bowlers]['balls_against_tailender']/indexbowlerwithoutrunratechange[bowlers]['balls']),2)
#for x in list(indexdatawithoutrunratechange.keys()):
    
#    print(x,indexdatawithoutrunratechange[x])


# In[108]:


def rpbrating(rpb,mean_rpb):
    if rpb=='NA':
        return(mean_rpb)
    else:
        return(rpb)
    
def bpwrating(bpw,rpb,mean_bpw):
    if bpw=='NA' and rpb=='NA':
        return(mean_bpw)
    elif bpw=='NA':
        return(2*mean_bpw)
    else:
        return(bpw)
    
def bpdrating(bpd,rpb,mean_bpd):
    if bpd=='NA'and rpb=='NA':
        return(mean_bpd)
    elif bpd=='NA':
        return(2*mean_bpd)
    else:
        return(bpd)


# In[109]:


def f_rpb(rpb,order):
    if order=='v_toporder':
        b_rpb_to.append(rpb)
    elif order=='v_middleorder':
        b_rpb_mo.append(rpb)
    elif order=='v_tailender':
        b_rpb_te.append(rpb)
        
def f_bpw(bpw,order):
    if order=='v_toporder':
        b_bpw_to.append(bpw)
    elif order=='v_middleorder':
        b_bpw_mo.append(bpw)
    elif order=='v_tailender':
        b_bpw_te.append(bpw)
        
def f_bpd(bpd,order):
    if order=='v_toporder':
        b_bpd_to.append(bpd)
    elif order=='v_middleorder':
        b_bpd_mo.append(bpd)
    elif order=='v_tailender':
        b_bpd_te.append(bpd)




rpb_to=0.54
rpb_mo=0.44
rpb_te=0.02
bpw_to=0.52
bpw_mo=0.47
bpw_te=0.01
bpd_to=0.57
bpd_mo=0.42
bpd_te=0.01

b_rpb_to=[]
b_rpb_mo=[]
b_rpb_te=[]
b_bpw_to=[]
b_bpw_mo=[]
b_bpw_te=[]
b_bpd_to=[]
b_bpd_mo=[]
b_bpd_te=[]

bowlerList=list(indexdatawithoutrunratechange.keys())
for bowlers in bowlerList:
    to=bowlersballratio[bowlers]['to']
    mo=bowlersballratio[bowlers]['mo']
#     te=bowlersballratio[bowlers]['te']
    
    brpb_to=indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_toporder']
    
    
    brpb_mo=indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_middleorder']
#     brpb_te=indexdatawithoutrunratechange[bowlers]['runs_per_ball']['v_tailender']
    
    bbpw_to=indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_toporder']
    bbpw_mo=indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_middleorder']
#     bbpw_te=indexdatawithoutrunratechange[bowlers]['balls_per_wickets']['v_tailender']
    
    bbpd_to=indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_toporder']
    bbpd_mo=indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_middleorder']
#     bbpd_te=indexdatawithoutrunratechange[bowlers]['balls_per_dotballs']['v_tailender']
    
    f_rpb(brpb_to,'v_toporder')
    f_rpb(brpb_mo,'v_middleorder')
#     f_rpb(brpb_te,'v_tailender')
    
    f_bpw(bbpw_to,'v_toporder')
    f_bpw(bbpw_mo,'v_middleorder')
#     f_bpw(bbpw_te,'v_tailender')
    
    f_bpd(bbpd_to,'v_toporder')
    f_bpd(bbpd_mo,'v_middleorder')
#     f_bpd(bbpd_te,'v_tailender')
    
#     print(b_bpw_te)
    
    mean_rpb_to=np.mean([val for val in b_rpb_to if val!='NA'])
    mean_rpb_mo=np.mean([val for val in b_rpb_mo if val!='NA'])
#     mean_rpb_te=np.mean([val for val in b_rpb_te if val!='NA'])
    
    mean_bpw_to=np.mean([val for val in b_bpw_to if val!='NA'])
    mean_bpw_mo=np.mean([val for val in b_bpw_mo if val!='NA'])
#     print([val for val in b_bpw_te if val!='NA'])
#     mean_bpw_te=np.mean([val for val in b_bpw_te if val!='NA'])
    
    mean_bpd_to=np.mean([val for val in b_bpd_to if val!='NA'])
    mean_bpd_mo=np.mean([val for val in b_bpd_mo if val!='NA'])
#     mean_bpd_te=np.mean([val for val in b_bpd_te if val!='NA'])
    
    max_rpb_to=max([val for val in b_rpb_to if val!='NA'])
    max_rpb_mo=max([val for val in b_rpb_mo if val!='NA'])
#     max_rpb_te=max([val for val in b_rpb_te if val!='NA'])
    
    max_bpw_to=max([val for val in b_bpw_to if val!='NA'])
    if len(list(set(b_bpw_mo).difference(set(['NA']))))>0:
        max_bpw_mo=max([val for val in b_bpw_mo if val!='NA'])
#     if len(list(set(b_bpw_te).difference(set(['NA']))))>0:
#         max_bpw_te=max([val for val in b_bpw_te if val!='NA'])
    
    max_bpd_to=max([val for val in b_bpd_to if val!='NA'])
    max_bpd_mo=max([val for val in b_bpd_mo if val!='NA'])
#     max_bpd_te=max([val for val in b_bpd_te if val!='NA'])
    
    min_rpb_to=min([val for val in b_rpb_to if val!='NA'])
    min_rpb_mo=min([val for val in b_rpb_mo if val!='NA'])
#     min_rpb_te=min([val for val in b_rpb_te if val!='NA'])
    
    min_bpw_to=min([val for val in b_bpw_to if val!='NA'])
    if len(list(set(b_bpw_mo).difference(set(['NA']))))>0:
        min_bpw_mo=min([val for val in b_bpw_mo if val!='NA'])
#     if len(list(set(b_bpw_te).difference(set(['NA']))))>0:
#         min_bpw_te=min([val for val in b_bpw_te if val!='NA'])
    
    min_bpd_to=min([val for val in b_bpd_to if val!='NA'])
    min_bpd_mo=min([val for val in b_bpd_mo if val!='NA'])
#     min_bpd_te=min([val for val in b_bpd_te if val!='NA'])
    
#     finalperformancerating[bowlers]={}
#     finalperformancerating[bowlers]['rating_rpb']=round((rpbrating(brpb_to)*rpb_to*to+rpbrating(brpb_mo)*rpb_mo*mo+rpbrating(brpb_te)*rpb_te*te),3)
#     finalperformancerating[bowlers]['rating_bpw']=round((bpwrating(bbpw_to)*bpw_to*to+bpwrating(bbpw_mo)*bpw_mo*mo+bpwrating(bbpw_te)*bpw_te*te),3)
#     finalperformancerating[bowlers]['rating_bpd']=round((bpdrating(bbpd_to)*bpd_to*to+rpbrating(bbpd_mo)*bpd_mo*mo+rpbrating(bbpd_te)*bpd_te*te),3)
#     finalperformancerating[bowlers]['total_rating']=round((finalperformancerating[bowlers]['rating_rpb']+finalperformancerating[bowlers]['rating_bpw']+finalperformancerating[bowlers]['rating_bpd']),3)

    

#print(mean_bpw_to,mean_bpw_mo)


# In[110]:


def weightageRPB(mean_rpb,bowler_rpb,order):
    if order=='v_toporder':
        if bowler_rpb<=mean_rpb:
            return(0.75)
        else:
            return(0.25)
        
    elif order=='v_middleorder':
        if bowler_rpb<=mean_rpb:
            return(0.24)
        else:
            return(0.76)
        
    elif order=='v_tailender':
        if bowler_rpb<=mean_rpb:
            return(0.01)
        else:
            return(0.99)
    
def weightageBPW(mean_bpw,bowler_bpw,order):
    if order=='v_toporder':
        if bowler_bpw<=mean_bpw:
            return(0.88)
        else:
            return(0.12)
        
    elif order=='v_middleorder':
        if bowler_bpw<=mean_bpw:
            return(0.11)
        else:
            return(0.89)
        
    elif order=='v_tailender':
        if bowler_bpw<=mean_bpw:
            return(0.01)
        else:
            return(0.99)
    
def weightageDPB(mean_dpb,bowler_dpb,order):
    if order=='v_toporder':
        if bowler_dpb<=mean_dpb:
            return(0.70)
        else:
            return(0.30)
        
    elif order=='v_middleorder':
        if bowler_dpb<=mean_dpb:
            return(0.29)
        else:
            return(0.71)
        
    elif order=='v_tailender':
        if bowler_dpb<=mean_dpb:
            return(0.01)
        else:
            return(0.99)


# In[111]:


finalperformancerating={}
for b in range(len(bowlerList)):
    
    tor=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_toporder']
    mor=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_middleorder']
    ter=indexdatawithoutrunratechange[bowlerList[b]]['runs_per_ball']['v_tailender']
    
    tow=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_toporder']
    mow=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_middleorder']
    tew=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_wickets']['v_tailender']
    
    tod=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_toporder']
    mod=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_middleorder']
    ted=indexdatawithoutrunratechange[bowlerList[b]]['balls_per_dotballs']['v_tailender']
    
    finalperformancerating[bowlerList[b]]={}
    finalperformancerating[bowlerList[b]]['rating_rpb']=round((((rpbrating(tor,mean_rpb_to)-mean_rpb_to)/(max_rpb_to-min_rpb_to))*weightageRPB(mean_rpb_to,rpbrating(tor,mean_rpb_to),'v_toporder')                                                               +((rpbrating(mor,mean_rpb_mo)-mean_rpb_mo)/(max_rpb_mo-min_rpb_mo))*weightageRPB(mean_rpb_mo,rpbrating(mor,mean_rpb_mo),'v_middleorder'))                                                               ,3)
    finalperformancerating[bowlerList[b]]['rating_bpw']=round((((bpwrating(tow,tor,mean_bpw_to)-mean_bpw_to)/(max_bpw_to-min_bpw_to))*weightageBPW(mean_bpw_to,bpwrating(tow,tor,mean_bpw_to),'v_toporder')                                                               +((bpwrating(mow,mor,mean_bpw_mo)-mean_bpw_mo)/(max_bpw_mo-min_bpw_mo))*weightageBPW(mean_bpw_mo,bpwrating(mow,mor,mean_bpw_mo),'v_middleorder'))                                                               ,3)
    finalperformancerating[bowlerList[b]]['rating_bpd']=round((((bpdrating(tod,tor,mean_bpd_to)-mean_bpd_to)/(max_bpd_to-min_bpd_to))*weightageDPB(mean_bpd_to,bpdrating(tod,tor,mean_bpd_to),'v_toporder')                                                               +((bpdrating(mod,mor,mean_bpd_mo)-mean_bpd_mo)/(max_bpd_mo-min_bpd_mo))*weightageDPB(mean_bpd_mo,bpdrating(mod,mor,mean_bpd_mo),'v_middleorder'))                                                               ,3)
    finalperformancerating[bowlerList[b]]['total_rating']=round(((-1)*finalperformancerating[bowlerList[b]]['rating_rpb']                                                                 +(-1)*finalperformancerating[bowlerList[b]]['rating_bpw']                                                                 +(-1)*finalperformancerating[bowlerList[b]]['rating_bpd']),3)
    
    
#for bowlers in list(finalperformancerating.keys()):
#    print(bowlers,finalperformancerating[bowlers]['total_rating'])


# In[112]:


WC_totalrating=[]
for bowlers in list(finalperformancerating.keys()):
    WC_totalrating.append([bowlers,finalperformancerating[bowlers]['total_rating']])
    
WC_totalrating.sort(key=lambda x:x[1],reverse=True)
#print(WC_totalrating)


# In[113]:


performancerating={}
for i in WC_totalrating:
    performancerating[i[0]]={}
    performancerating[i[0]]['performancerating']=i[1]
#print(performancerating)


# In[114]:


scaledrating={}
Lsituation=[]
Lperformance=[]
Ldifficulty=[]

for i in list(situationindex.keys()):
    Lsituation.append(situationindex[i]['situation_rating'])
    Lperformance.append(performancerating[i]['performancerating'])
    Ldifficulty.append(bowlersballratio[i]['difficulty'])
for j in list(situationindex.keys()):
    scaledrating[j]={}
    scaledrating[j]['situation']=round(((situationindex[j]['situation_rating']-min(Lsituation))/(max(Lsituation)-min(Lsituation))),2)
    scaledrating[j]['performance']=round(((performancerating[j]['performancerating']-min(Lperformance))/(max(Lperformance)-min(Lperformance))),2)
    scaledrating[j]['difficulty']=round(((bowlersballratio[j]['difficulty']-min(Ldifficulty))/(max(Ldifficulty)-min(Ldifficulty))),2)

#print(scaledrating)


# In[115]:


totalrating={}

for i in list(scaledrating.keys()):
    totalrating[i]={}
    totalrating[i]['ratingpts']=(0.25)*scaledrating[i]['situation']+(0.5)*scaledrating[i]['performance']+(0.25)*scaledrating[i]['difficulty']
    totalrating[i]['overall_stats']=indexbowlerwithoutrunratechange[i]
    totalrating[i]['perball_overall_stats']=indexdatawithoutrunratechange[i]
#print(totalrating)


# In[116]:


overallrating=[]
for i in list(totalrating.keys()):
    overallrating.append([i,totalrating[i]['ratingpts'],totalrating[i]['overall_stats'],totalrating[i]['perball_overall_stats']])
    
#print(tabulate(overallrating))
#print(overallrating[1][0])


# In[117]:


overallrating.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(overallrating))


# In[118]:


import csv
filecursor=open('poweplays2_outfile.csv','w')
csvWriter=csv.writer(filecursor)
csvWriter.writerow(['bowler','rating','total_balls','ballsVtoporder','ballsVmiddleorder','ballsVtailender','overallRPB','RPBvtoporder','RPBvmiddleorder',                   'RPBvtailender','wickets','wktVtoporder','wktVmiddleorder','wktVtailender','overallBPW','BPWvToporder','BPWvmiddleorder','BPWvtailender',                   'performancerating','situationrating','difficultyfaced'])
for bowler in overallrating:
    csvWriter.writerow([bowler[0],totalrating[bowler[0]]['ratingpts'],indexbowlerwithoutrunratechange[bowler[0]]['balls'],                       indexbowlerwithoutrunratechange[bowler[0]]['balls_against_top_order'],indexbowlerwithoutrunratechange[bowler[0]]['balls_against_middle_order'],                      indexbowlerwithoutrunratechange[bowler[0]]['balls_against_tailender'],indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['overall'],                       indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['v_toporder'],indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['v_middleorder'],                       indexdatawithoutrunratechange[bowler[0]]['runs_per_ball']['v_tailender'],indexbowlerwithoutrunratechange[bowler[0]]['wickets'],                       indexbowlerwithoutrunratechange[bowler[0]]['wickets_toporder'],indexbowlerwithoutrunratechange[bowler[0]]['wickets_middleorder'],                        indexbowlerwithoutrunratechange[bowler[0]]['wickets_tailender'],indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['overall'],                       indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['v_toporder'],indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['v_middleorder'],                       indexdatawithoutrunratechange[bowler[0]]['balls_per_wickets']['v_tailender'],scaledrating[bowler[0]]['performance'],scaledrating[bowler[0]]['situation'],                       scaledrating[bowler[0]]['difficulty']])
filecursor.close()


# In[119]:


overallratingfinal=[]
for i in list(totalrating.keys()):
    overallratingfinal.append([i,totalrating[i]['ratingpts']])
    
#print(tabulate(overallratingfinal))


# In[120]:


overallratingfinal.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(overallratingfinal))


# In[121]:


for i in overallratingfinal:
    i.append(overallratingfinal.index(i)+1)


# In[122]:


overallratingfinal.sort(key=lambda x:x[1],reverse=True)
#print(tabulate(overallratingfinal))


# In[123]:


situation=[]
performance=[]
difficulty=[]
for i in list(situationindex.keys()):
    situation.append([i,situationindex[i]['situation_rating']])
    performance.append([i,performancerating[i]['performancerating']])
    difficulty.append([i,bowlersballratio[i]['difficulty']])
situation.sort(key=lambda x:x[1],reverse=True)
performance.sort(key=lambda x:x[1],reverse=True)
difficulty.sort(key=lambda x:x[1],reverse=True)


# In[124]:


Lsituation=[]
Lperformance=[]
Ldifficulty=[]
for x in range(len(situation)):
    Lsituation.append(situation[x])
    Lperformance.append(performance[x])
    Ldifficulty.append(difficulty[x])
    


# In[125]:


#print(tabulate(situation),tabulate(performance),tabulate(difficulty))


# In[126]:


L=["NA",'NA']
#print(len(list(set(L).difference(set(['NA']))))>0)


# In[127]:


with open('bowlerdatabase__allMatches.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    allBowlersInfo = unpickler.load() 
    file.close()

