from math import erf, sqrt, inf

import os
import datetime
import numpy as np
import csv
import pickle
 
import statistics 
# from IPython.core.display import display, HTML
# display(HTML("<style>.container { width:90% !important; }</style>"))
import math

import statsmodels.api as sm
import scipy

# from sklearn.svm import SVR
# from sklearn.pipeline import make_pipeline
# from sklearn.preprocessing import StandardScaler

# import tensorflow as tf
import joblib

fitModel=joblib.load('ols1stInngs.z')

def getWickRemCategorical(w):
    x=[0]*11
    x[w]=1
    return x
def get_OLS_ballByBallDict_catWickRem(ballRem=119,wicketRem=10):
    x=getWickRemCategorical(wicketRem)
    x.insert(0,1)
    return round(fitModel[ballRem].predict(x)[0],0)

winLossDistribution1stInngs=joblib.load('winLossDistribution1stInngs.z')
winDistribution,lossDistribution=winLossDistribution1stInngs[0],winLossDistribution1stInngs[1]

def getWinProb(run):
    return round((winDistribution.pdf(run)/(winDistribution.pdf(run)+lossDistribution.pdf(run)))*100,1)


# first innings total run prediction
# used for 2nd innings prediction too

ols1stInng=joblib.load('ols1stInngs.z')

def getWickRemCategorical(w):
    x=[0]*11
    x[w]=1
    return x
def get_OLS_ballByBallDict_catWickRem(ballRem=119,wicketRem=10):
    x=getWickRemCategorical(wicketRem)
    x.insert(0,1)
    return round(ols1stInng[ballRem].predict(x)[0],0)

#=============================================================================================================

with open('secondInngdata.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    secondInngsdata = unpickler.load() 
    file.close()
        
# 2nd innings run prediction model

for i in secondInngsdata:
    for j in list(i['BallbyBall'].keys()):
        if j!=0:
            i['BallbyBall'][j]['required_run_rate']=round(((i['target']-i['BallbyBall'][j]['inningstotal'])/j),2)
        else:
            i['BallbyBall'][j]['required_run_rate']=0
            
regressionDataX=[] #added3006
regressionDataY=[] #added3006

for i in secondInngsdata:
    
    start_date='2008-01-01'
    end_date='2020-12-31'
    if i['date']>datetime.datetime.strptime(end_date, '%Y-%m-%d')\
    or i['date']<datetime.datetime.strptime(start_date, '%Y-%m-%d'):
        continue
        
    ballRem=list(i['BallbyBall'].keys())
    for j in ballRem:

        l=getWickRemCategorical(10-i['BallbyBall'][j]['wickets'])

        l.append(i['BallbyBall'][j]['required_run_rate'])

        l.append(j) #added3006
        regressionDataX.append(l) #added3006
        

        if i['win/loss']=='loss':

            regressionDataY.append(i['BallbyBall'][min(ballRem)]['inningstotal']-i['BallbyBall'][j]['inningstotal']) #added3006
        else:
            predicted=get_OLS_ballByBallDict_catWickRem(ballRem=min(ballRem),wicketRem=10-i['BallbyBall'][min(ballRem)]['wickets'])+\
                        (i['BallbyBall'][min(ballRem)]['inningstotal']-i['BallbyBall'][j]['inningstotal'])

            regressionDataY.append(predicted) #added3006
        

for j in range(120):

    ConstAdded=sm.add_constant(regressionDataX) #added3006
    model = sm.OLS(regressionDataY,ConstAdded) #added3006
   
    fitModel2=model.fit()  #added3006 
    


win_loss_dict={}
win_loss_dict['win']={}
win_loss_dict['loss']={}
winningmatches={}
losingmatches={}
avg_deviation_per_ball={}
for b in range(119,-1,-1):
    win_loss_dict['win'][b]={}
    win_loss_dict['loss'][b]={}
    win_loss_dict['win'][b]['target']=[]
    win_loss_dict['loss'][b]['target']=[]
    win_loss_dict['win'][b]['predicted_run']=[]
    win_loss_dict['loss'][b]['predicted_run']=[]
    win_loss_dict['win'][b]['actual_run']=[]
    win_loss_dict['loss'][b]['actual_run']=[]
    win_loss_dict['win'][b]['deviation_from_target']=[]
    win_loss_dict['loss'][b]['deviation_from_target']=[]
    losingmatches[b]={}
    winningmatches[b]={}
    if b>0:
        avg_deviation_per_ball[b]={}
    


loss_deviation=[]
win_deviation=[]

for i in secondInngsdata:
    start_date='2008-01-01'
    end_date='2020-12-31'
    if i['date']>datetime.datetime.strptime(end_date, '%Y-%m-%d')\
    or i['date']<datetime.datetime.strptime(start_date, '%Y-%m-%d'):
        continue
    ballsrem_inthisinng=list(i['BallbyBall'].keys())
    
    if i['win/loss']=='loss':
        
        run_dev_loss=i['BallbyBall'][min(ballsrem_inthisinng)]['inningstotal']-i['target']
        loss_deviation.append(run_dev_loss)
        
    else:
        run_dev_win=get_OLS_ballByBallDict_catWickRem(ballRem=min(ballsrem_inthisinng),wicketRem=10-i['BallbyBall'][min(ballsrem_inthisinng)]['wickets'])+\
        i['BallbyBall'][min(ballsrem_inthisinng)]['inningstotal']-i['target']
        
        win_deviation.append(run_dev_win)
        
# mu,sig=np.median(loss_deviation+win_deviation),np.std(loss_deviation+win_deviation)
mu1,sig1=np.median(loss_deviation),np.std(loss_deviation)
mu2,sig2=np.median(win_deviation),np.std(win_deviation)
def getWinProb2ndInng_2(run):
    return round((((.5+erf( (run-mu2) / (sig2*sqrt(2)) )/2)+(.5+erf( (run-mu1) / (sig1*sqrt(2)) )/2))/2)*100,2)


import yaml

with open('data_Of_allMatches.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    allmatchInfo = unpickler.load() 
    file.close()

def getTarget(x):
    for i in allmatchInfo:
        if i['matchId']==x:
            for j in range(len(i['innings'])):
                inningstotal=0
                if j <=1:
                    inningsKey=list(i['innings'][j].keys())[0]
                    for balls in i['innings'][j][inningsKey]['deliveries']:
                        balls_key=list(balls.keys())[0]
                        inningstotal+=balls[balls_key]['runs']['total']
                        if inningsKey=='1st innings':
                            if i['innings'][j][inningsKey]['deliveries'].index(balls)==len(i['innings'][j][inningsKey]['deliveries'])-1:
                                target=inningstotal+1
                                
                                return target

matches=[]

for i in allmatchInfo:
    

    matchNo=i['matchId']
    if matchNo=='1136566.yaml':
        continue
    if matchNo=='1136592.yaml':
        continue
    if matchNo=='1178424.yaml':
        continue
    if matchNo=='1237182.yaml':
        continue
        
#     if matchNo!='1082648.yaml':
#         continue

#     print(matchNo,type(matchNo))
    
    if isinstance(i['info']['dates'][0],str):
        thisMatchDate=datetime.datetime.strptime(i['info']['dates'][0], '%Y-%m-%d')
    else:
        thisMatchDate=datetime.datetime.strptime(\
                            datetime.datetime.strftime(i['info']['dates'][0], '%Y-%m-%d'),\
                            '%Y-%m-%d')
    if thisMatchDate.year>=2017 and thisMatchDate.year<=2021:
        thismatchdata={}
        thismatchdata[matchNo]={}
        thismatchdata[matchNo]['date']=thisMatchDate
        
        for j in range(len(i['innings'])):
            ballIndex=120
            inningstotal=0
            wickets=10
            batsmaninthisinng=[]
            if j<=1:
                innings_key=list(i['innings'][j].keys())[0]
                thismatchdata[matchNo][innings_key]={}
                
                balindexChange=False
                for balls in i['innings'][j][innings_key]['deliveries']:
                    thisBallInfo={}
                    balls_key=list(balls.keys())[0]
                    inningstotal+=(balls[balls_key]['runs']['batsman']+balls[balls_key]['runs']['extras'])
                    bowler=balls[balls_key]['bowler']
                    batsman=balls[balls_key]['batsman']
                    thisBallInfo['score']=inningstotal
                    thisBallInfo['batsman']=batsman
                    thisBallInfo['bowler']=bowler
                    if 'wicket' in list(balls[balls_key].keys()):
                        wickets=wickets-1
                        if balls[balls_key]['wicket']['kind']!='runout':
                            thisBallInfo['bowlerwicket']='yes'
                        else:
                            thisBallInfo['bowlerwicket']='no'
                            
                    else:
                        thisBallInfo['bowlerwicket']='no'
                        
                    thisBallInfo['Rem_wicket']=wickets
                    
                    if batsman not in batsmaninthisinng:
                        batsmaninthisinng.append(batsman)
                    thisBallInfo['batting_order_of_batsman']=batsmaninthisinng.index(batsman)+1
                    
                    
                    validBall=True
                    if 'extras' in list(balls[balls_key].keys()):
                        if 'wides' in list(balls[balls_key]['extras'].keys()) or \
                        'noballs' in list(balls[balls_key]['extras'].keys()):
                            if not balindexChange:
                                ballIndex=ballIndex-1
                                thismatchdata[matchNo][innings_key][ballIndex]=[]
                                balindexChange=True
                            validBall=False
                                
                    if validBall:
                        if not balindexChange:
                            ballIndex=ballIndex-1
                            thismatchdata[matchNo][innings_key][ballIndex]=[]
                        balindexChange=False
                        
                        
                        
                        thisBallInfo['thisballrun']=balls[balls_key]['runs']['batsman']
                        thisBallInfo['thisballbatsmanrun']=balls[balls_key]['runs']['batsman']
                        
                        if innings_key=='1st innings':
                            if ballIndex==120:
                                continue
                            thisBallInfo['thisballwinprob_batting']=getWinProb(inningstotal+\
                                                                               get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))
                            thisBallInfo['thisballwinprob_bowling']=100-getWinProb(inningstotal+\
                                                                                       get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))
                    

                        elif innings_key=='2nd innings':
                            if ballIndex==120:
                                continue
                            if ballIndex==0:
                                continue

                            wicketRem=thisBallInfo['Rem_wicket']
                            runRem=getTarget(matchNo)-thisBallInfo['score']

                            l=getWickRemCategorical(wicketRem)
                            l.append(runRem/ballIndex)
                            l.append(ballIndex)
                            l.insert(0,1)
    #                             run=int(fitModel2[ballIndex].predict(l)[0])-runRem
                            run=int(fitModel2.predict(l)[0])-runRem

    #                             print(ballIndex,wicketRem,runRem,getWinProb2ndInng_2(run))
                            thisBallInfo['thisballwinprob_batting']=getWinProb2ndInng_2(run)
                            thisBallInfo['thisballwinprob_bowling']=100-thisBallInfo['thisballwinprob_batting']
    #                   
                        thismatchdata[matchNo][innings_key][ballIndex].append(thisBallInfo)
                        
                    elif 'extras' in list(balls[balls_key].keys()):
                        if 'legbyes' not in list(balls[balls_key]['extras'].keys()) or \
                        'byes' not in list(balls[balls_key]['extras'].keys()):
#                             if ballIndex==120:
#                                 continue
                            
                            thisBallInfo['thisballrun']=(balls[balls_key]['runs']['batsman']+balls[balls_key]['runs']['extras'])
                            
                            thisBallInfo['thisballbatsmanrun']=balls[balls_key]['runs']['batsman']
                                
                                
                            if innings_key=='1st innings':
                                if ballIndex==120:
                                    continue
                                thisBallInfo['thisballwinprob_batting']=getWinProb(inningstotal+\
                                                                                   get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))
                                thisBallInfo['thisballwinprob_bowling']=100-getWinProb(inningstotal+\
                                                                                           get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))

                            elif innings_key=='2nd innings':
                                if ballIndex==120:
                                    continue
                                if ballIndex==0:
                                    continue

                                wicketRem=thisBallInfo['Rem_wicket']
                                runRem=getTarget(matchNo)-thisBallInfo['score']

                                l=getWickRemCategorical(wicketRem)
                                l.append(runRem/ballIndex)
                                l.append(ballIndex)
                                l.insert(0,1)
        #                             run=int(fitModel2[ballIndex].predict(l)[0])-runRem
                                run=int(fitModel2.predict(l)[0])-runRem

        #                             print(ballIndex,wicketRem,runRem,getWinProb2ndInng_2(run))
                                thisBallInfo['thisballwinprob_batting']=getWinProb2ndInng_2(run)
                                thisBallInfo['thisballwinprob_bowling']=100-thisBallInfo['thisballwinprob_batting']
#                   
                            thismatchdata[matchNo][innings_key][ballIndex].append(thisBallInfo)
        
        matches.append(thismatchdata)   
#         break 


matchwise_players=[]
for i in matches:
    thismatch_players={}
    matchkeys=list(i.keys())[0]
    thismatch_players[matchkeys]={}
    for j in list(i[matchkeys].keys())[1:]:
        balls_keys=list(i[matchkeys][j].keys())
        for balls in balls_keys:
            if balls!=120 and balls!=0:
                for info in range(len(i[matchkeys][j][balls])):
                    batsman=i[matchkeys][j][balls][info]['batsman']
                    bowler=i[matchkeys][j][balls][info]['bowler']
                    if batsman not in list(thismatch_players[matchkeys].keys()):
                        if j == '1st innings':
    #                         print(batsman)
                            thismatch_players[matchkeys][batsman]={}
                            thismatch_players[matchkeys][batsman]['bat']={}
    #                         print(balls)
        #                     thismatch_players[matchkeys][batsman]['bowl']={}
                            
                            thismatch_players[matchkeys][batsman]['bat'][balls]=[]
        #                     thismatch_players[matchkeys][batsman]['bowl'][balls]={}
                            thismatch_players[matchkeys][batsman]['bat'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_batting'],j,info])
                            
                        else:
    #                         print(batsman)
                            thismatch_players[matchkeys][batsman]={}
                            thismatch_players[matchkeys][batsman]['bat']={}
                            thismatch_players[matchkeys][batsman]['bat'][balls]=[]
                            thismatch_players[matchkeys][batsman]['bat'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_batting'],j,info])
                        

                    else:
                        if j == '1st innings':
                            thismatch_players[matchkeys][batsman]['bat'][balls]=[]


                            thismatch_players[matchkeys][batsman]['bat'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_batting'],j,info])
                        else:
                            if 'bat' not in list(thismatch_players[matchkeys][batsman].keys()):
                                thismatch_players[matchkeys][batsman]['bat']={}
                                thismatch_players[matchkeys][batsman]['bat'][balls]=[]
                                thismatch_players[matchkeys][batsman]['bat'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_batting'],j,info])
                            else:
                                thismatch_players[matchkeys][batsman]['bat'][balls]=[]
                                thismatch_players[matchkeys][batsman]['bat'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_batting'],j,info])


                    if bowler not in list(thismatch_players[matchkeys].keys()):
                        if j =='1st innings':
                            thismatch_players[matchkeys][bowler]={}
                            thismatch_players[matchkeys][bowler]['bowl']={}
                            
                            thismatch_players[matchkeys][bowler]['bowl'][balls]=[]
                            thismatch_players[matchkeys][bowler]['bowl'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_bowling'],j,info])
                        else:
                            thismatch_players[matchkeys][bowler]={}
                            thismatch_players[matchkeys][bowler]['bowl']={}
                            thismatch_players[matchkeys][bowler]['bowl'][balls]=[]
                            thismatch_players[matchkeys][bowler]['bowl'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_bowling'],j,info])

                    else:
                        if j == '1st innings':
                            thismatch_players[matchkeys][bowler]['bowl'][balls]=[]
                            thismatch_players[matchkeys][bowler]['bowl'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_bowling'],j,info])
                        else:
                            if 'bowl' not in list(thismatch_players[matchkeys][bowler].keys()):
                                thismatch_players[matchkeys][bowler]['bowl']={}
                                thismatch_players[matchkeys][bowler]['bowl'][balls]=[]
                                thismatch_players[matchkeys][bowler]['bowl'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_bowling'],j,info])
                            else:
                                thismatch_players[matchkeys][bowler]['bowl'][balls]=[]
                                thismatch_players[matchkeys][bowler]['bowl'][balls].append([i[matchkeys][j][balls][info]['thisballwinprob_bowling'],j,info])
    matchwise_players.append(thismatch_players)                        
#     break  
    
    
# print(matchwise_players)
            

def know_whether_inngs_is_effective(x,y,z):
    for i in matchwise_players:
        matchID=list(i.keys())[0]
        if y==matchID:
            for effective_players in list(i[matchID].keys()):
                if effective_players==z:
                    for j in list(i[matchID][effective_players].keys()):
                        Rem_balls=list(i[matchID][effective_players][j].keys())
                        all_balls_win_prob=[]
                        batsman_per_ball_Win_prob=[]
                        for k in range(len(Rem_balls)):
                            if j=='bat':
                                batsman_per_ball_Win_prob.append(i[matchID][effective_players][j][Rem_balls[k]][0][0])
#                                     if i[matchID][effective_players][j][Rem_balls[0]][0][0]<=35 and i[matchID][effective_players][j][Rem_balls[-4]][0][0]>=i[matchID][effective_players][j][Rem_balls[0]][0][0]+15:
#                                         return 'bat_positive'

                            else:

#                                 if Rem_balls[k]==118:
#                                     all_balls_win_prob.append([119,i[matchID][effective_players][j][118][0][0]])
#                                     all_balls_win_prob.append([Rem_balls[k],i[matchID][effective_players][j][Rem_balls[k]][0][0]])
#                                 elif i[matchID][effective_players][j][Rem_balls[k]][0][2]==1:
#                                     all_balls_win_prob.append()
#                                 else:
#                                 if i[matchID][effective_players][j][Rem_balls[k]][0][2]==1:
#                                     continue
                                all_balls_win_prob.append([Rem_balls[k],i[matchID][effective_players][j][Rem_balls[k]][0][0]])
#                                 print(all_balls_win_prob)

                        if j =='bowl':

                            capture_effective_overs=[]
                            overDict={}
                            for aBall in all_balls_win_prob:
                                if overDict.get(int((119-aBall[0])/6)) is None:
                                    overDict[int((119-aBall[0])/6)]=[]
                                overDict[int((119-aBall[0])/6)].append(aBall)
                            all_overs_win_prob=[]
                            for anOver in list(overDict.keys()):
                                all_overs_win_prob.append(overDict[anOver])
                            
                            for overs in all_overs_win_prob:
                                
                                check_overs=[balls for balls in overs] 
                                compute_overs=[check_overs[n][1] for n in range(len(check_overs))]
                                
                                if compute_overs[0]<=30 and compute_overs[-1]>=compute_overs[0]+20:########## correct the winP % to 30 for bowlers
                                    capture_effective_overs.append([compute_overs,matchID])
                                    
                            if len(capture_effective_overs)>0:
                                return [len(capture_effective_overs),matchID,capture_effective_overs]
                                
                        else:
                            if len(batsman_per_ball_Win_prob)>=10:
                                if batsman_per_ball_Win_prob[0]<=25 and batsman_per_ball_Win_prob[-1]>=batsman_per_ball_Win_prob[0]+40:
                                    return ['bat_positive',matchID]



effectively_unpredictable={}
effective_bat_innings=0
effective_bowl_overs=0
for i in matchwise_players:
    matchID=list(i.keys())[0]
    for effective_players in list(i[matchID].keys()):
        
        if effective_players not in list(effectively_unpredictable.keys()):
            effectively_unpredictable[effective_players]={}
            effectively_unpredictable[effective_players]['bat_effective_inngs']=0
            effectively_unpredictable[effective_players]['bowl_effective_overs']=0
            
            effectively_unpredictable[effective_players]['bat_effective_inngs_list']=[]
            effectively_unpredictable[effective_players]['bowl_effective_overs_list']=[]
            
            
            
            for j in list(i[matchID][effective_players].keys()):
                if know_whether_inngs_is_effective(j,matchID,effective_players) is not None:
                    if know_whether_inngs_is_effective(j,matchID,effective_players)[0]=='bat_positive':
#                         effectively_unpredictable[effective_players]['bat_effective_inngs']+=1
                        effectively_unpredictable[effective_players]['bat_effective_inngs_list'].append(matchID)

                    elif know_whether_inngs_is_effective(j,matchID,effective_players)[0]>0:
                        effectively_unpredictable[effective_players]['bowl_effective_overs']=know_whether_inngs_is_effective(j,matchID,effective_players)[0]
                        effectively_unpredictable[effective_players]['bowl_effective_overs_list'].append(know_whether_inngs_is_effective(j,matchID,effective_players)[1])

                    else:
                        effectively_unpredictable[effective_players]['bat_effective_inngs']+=0
                        effectively_unpredictable[effective_players]['bowl_effective_overs']+=0
                    
        else:
            
            for j in list(i[matchID][effective_players].keys()):
                if know_whether_inngs_is_effective(j,matchID,effective_players) is not None:
                    if know_whether_inngs_is_effective(j,matchID,effective_players)[0]=='bat_positive':
#                         effectively_unpredictable[effective_players]['bat_effective_inngs']+=1
                        effectively_unpredictable[effective_players]['bat_effective_inngs_list'].append(matchID)

                    elif know_whether_inngs_is_effective(j,matchID,effective_players)[0]>0:
                        if matchID not in effectively_unpredictable[effective_players]['bowl_effective_overs_list']:
                        
                            effectively_unpredictable[effective_players]['bowl_effective_overs']+=know_whether_inngs_is_effective(j,matchID,effective_players)[0]
                            effectively_unpredictable[effective_players]['bowl_effective_overs_list'].append(know_whether_inngs_is_effective(j,matchID,effective_players)[1])

                    else:
                        effectively_unpredictable[effective_players]['bat_effective_inngs']+=0
                        effectively_unpredictable[effective_players]['bowl_effective_overs']+=0

                    
        effectively_unpredictable[effective_players]['bat_effective_inngs']=len(list(set(effectively_unpredictable[effective_players]['bat_effective_inngs_list'])))   

# print(effectively_unpredictable)        

matchwise_rating={}
for i in matchwise_players:
    matchID=list(i.keys())[0]
    matchwise_rating[matchID]={}
    relative_total_winPchange=[]
    for player in list(i[matchID].keys()):
        
        matchwise_rating[matchID][player]={}
        matchwise_rating[matchID][player]['batting_winP_change']=0
        matchwise_rating[matchID][player]['bowling_winP_change']=0
        batcountball=0
        bowlcountball=0
        
        for j in list(i[matchID][player].keys()):
            balls_involved=list(i[matchID][player][j].keys())
            
            for balls in range(len(balls_involved)):
                if j =='bat':
                    batcountball+=1
                    if i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0]>0:
                        matchwise_rating[matchID][player]['batting_winP_change']+=(100-i[matchID][player][j][balls_involved[balls]][0][0])*(i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0])
                    elif i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0]<0:
                        matchwise_rating[matchID][player]['batting_winP_change']+=i[matchID][player][j][balls_involved[balls]][0][0]*(i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0])
#                     matchwise_rating[matchID][player]['batting_winP_change']+=i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0]
                    else:
                        if i[matchID][player][j][balls_involved[balls]][0][0]>=60:
                            matchwise_rating[matchID][player]['batting_winP_change']+=(100-i[matchID][player][j][balls_involved[balls]][0][0])*0.5
                        else:
                            matchwise_rating[matchID][player]['batting_winP_change']+=0
                    if batcountball>=6:      
                        matchwise_rating[matchID][player]['batting_winP_change_perball'] =round((matchwise_rating[matchID][player]['batting_winP_change']/batcountball),2) 
                        matchwise_rating[matchID][player]['balls_involved_batting']=batcountball
                    else:
                        matchwise_rating[matchID][player]['batting_winP_change_perball']=0
                        matchwise_rating[matchID][player]['balls_involved_batting']=batcountball
                        
                else:
                    bowlcountball+=1
#                     matchwise_rating[matchID][player]['bowling_winP_change']+=i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0]
                    if i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0]>0:
                        matchwise_rating[matchID][player]['bowling_winP_change']+=(100-i[matchID][player][j][balls_involved[balls]][0][0])*(i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0])
                    elif i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0]<0:
                        matchwise_rating[matchID][player]['bowling_winP_change']+=i[matchID][player][j][balls_involved[balls]][0][0]*(i[matchID][player][j][balls_involved[min(len(balls_involved)-1,balls+1)]][0][0]-i[matchID][player][j][balls_involved[balls]][0][0])
                        
                    else:
                        if i[matchID][player][j][balls_involved[balls]][0][0]>=60:
                            matchwise_rating[matchID][player]['bowling_winP_change']+=(100-i[matchID][player][j][balls_involved[balls]][0][0])*0.5
                        else:
                            matchwise_rating[matchID][player]['bowling_winP_change']+=0
                      
                    if bowlcountball>=6:
                        matchwise_rating[matchID][player]['bowling_winP_change_perball']=round((matchwise_rating[matchID][player]['bowling_winP_change']/bowlcountball),2)
                        matchwise_rating[matchID][player]['balls_involved_bowling']=bowlcountball
                    else:
                        matchwise_rating[matchID][player]['bowling_winP_change_perball']=0
                        matchwise_rating[matchID][player]['balls_involved_bowling']=bowlcountball
        if 'batting_winP_change_perball' not in list(matchwise_rating[matchID][player].keys()) :          
            matchwise_rating[matchID][player]['total_winP_change']=round((matchwise_rating[matchID][player]['bowling_winP_change_perball']),2)
        elif 'bowling_winP_change_perball' not in list(matchwise_rating[matchID][player].keys()):
            matchwise_rating[matchID][player]['total_winP_change']=round((matchwise_rating[matchID][player]['batting_winP_change_perball']),2)
        else:
            matchwise_rating[matchID][player]['total_winP_change']=round((matchwise_rating[matchID][player]['batting_winP_change_perball']+matchwise_rating[matchID][player]['bowling_winP_change_perball']),2)
    
        
        relative_total_winPchange.append(matchwise_rating[matchID][player]['total_winP_change'])
    highest_winP_change=max(relative_total_winPchange)
    lowest_winP_change=min(relative_total_winPchange)
    mean_winP_change=np.mean(relative_total_winPchange)
    std_winP_change=np.mean(relative_total_winPchange)
    median_winP_change=np.median(relative_total_winPchange)
    for x in list(i[matchID].keys()):
        matchwise_rating[matchID][x]['match_rating']=round((matchwise_rating[matchID][x]['total_winP_change']-lowest_winP_change)/(highest_winP_change-lowest_winP_change),2)
        matchwise_rating[matchID][x]['scaled_rating']=round((matchwise_rating[matchID][x]['total_winP_change']-median_winP_change)/(highest_winP_change-lowest_winP_change),2)
#     break
# for z in list(matchwise_rating['1082591.yaml'].keys()):    
#     print(z,matchwise_rating['1082591.yaml'][z])    
    

unpredictability_players={}
for matches in list(matchwise_rating.keys()):
    for players in list(matchwise_rating[matches].keys()):
        if players not in list(unpredictability_players.keys()):
            unpredictability_players[players]={}
            unpredictability_players[players][matches]={}
            unpredictability_players[players][matches]['a']=matchwise_rating[matches][players]['match_rating']
            unpredictability_players[players][matches]['b']=matchwise_rating[matches][players]['scaled_rating']
        else:
            unpredictability_players[players][matches]={}
            unpredictability_players[players][matches]['a']=matchwise_rating[matches][players]['match_rating']
            unpredictability_players[players][matches]['b']=(matchwise_rating[matches][players]['scaled_rating'])
for element in list(unpredictability_players.keys()):
    total_matches=0
    cum_rating=0
    scaled_cum_rating=0
    for i in list(unpredictability_players[element].keys()):
        total_matches+=1
        cum_rating+=unpredictability_players[element][i]['a']
        scaled_cum_rating+=unpredictability_players[element][i]['b']
    unpredictability_players[element]['total_matches']=total_matches
    unpredictability_players[element]['total_rating']=cum_rating
    unpredictability_players[element]['total_scaled_rating']=scaled_cum_rating
# print(unpredictability_players)

final_evaluation={}
for players in list(unpredictability_players.keys()):
    if unpredictability_players[players]['total_matches']>=10:
        final_evaluation[players]={}
        count=0
        for games in list(unpredictability_players[players].keys()):
            if games[-4:]!='yaml':
                continue
                
            if unpredictability_players[players][games]['b']>=0:
                count+=1
        highest_effective_ratio=round(count/unpredictability_players[players]['total_matches'],2)
        final_evaluation[players]['ER']=highest_effective_ratio 
        final_evaluation[players]['total_effective_knocks']=effectively_unpredictable[players]['bat_effective_inngs']+effectively_unpredictable[players]['bowl_effective_overs']
        final_evaluation[players]['situation_rating']=unpredictability_players[players]['total_rating']
        final_evaluation[players]['scaled_situation_rating']=unpredictability_players[players]['total_scaled_rating']

# print(final_evaluation)

final=[]
for i in list(final_evaluation.keys()):
    final.append([i,round((final_evaluation[i]['ER']*.2+final_evaluation[i]['total_effective_knocks']*.8)*50,2),final_evaluation[i]['total_effective_knocks'],final_evaluation[i]['ER'],final_evaluation[i]['situation_rating'],effectively_unpredictable[i]['bat_effective_inngs_list'],effectively_unpredictable[i]['bowl_effective_overs_list']])
#     final.append([i,round(final_evaluation[i]['ER']*final_evaluation[i]['situation_rating']+final_evaluation[i]['total_effective_knocks'],2)])
    
final.sort(key=lambda x:x[1], reverse=True )

CSV=[]
for i in final:
    CSV.append([i[0],i[1]])
# print(CSV)

import csv
filecursor=open('most_unpredictable_players_in_ipl.csv','w',newline='')
csvWriter=csv.writer(filecursor)
csvWriter.writerow(['player','rating'])
for i in CSV:
    csvWriter.writerow([i[0],i[1]])
filecursor.close()