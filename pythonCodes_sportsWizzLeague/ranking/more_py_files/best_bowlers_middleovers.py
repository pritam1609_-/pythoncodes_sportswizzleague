from math import erf, sqrt, inf

import os
import datetime
import numpy as np
import csv
import pickle
 
import statistics 
# from IPython.core.display import display, HTML
# display(HTML("<style>.container { width:90% !important; }</style>"))
import math

import statsmodels.api as sm
import scipy

# from sklearn.svm import SVR
# from sklearn.pipeline import make_pipeline
# from sklearn.preprocessing import StandardScaler

# import tensorflow as tf
import joblib

fitModel=joblib.load('ols1stInngs.z')

def getWickRemCategorical(w):
    x=[0]*11
    x[w]=1
    return x
def get_OLS_ballByBallDict_catWickRem(ballRem=119,wicketRem=10):
    x=getWickRemCategorical(wicketRem)
    x.insert(0,1)
    return round(fitModel[ballRem].predict(x)[0],0)

winLossDistribution1stInngs=joblib.load('winLossDistribution1stInngs.z')
winDistribution,lossDistribution=winLossDistribution1stInngs[0],winLossDistribution1stInngs[1]

def getWinProb(run):
    return round((winDistribution.pdf(run)/(winDistribution.pdf(run)+lossDistribution.pdf(run)))*100,1)


# first innings total run prediction
# used for 2nd innings prediction too

ols1stInng=joblib.load('ols1stInngs.z')

def getWickRemCategorical(w):
    x=[0]*11
    x[w]=1
    return x
def get_OLS_ballByBallDict_catWickRem(ballRem=119,wicketRem=10):
    x=getWickRemCategorical(wicketRem)
    x.insert(0,1)
    return round(ols1stInng[ballRem].predict(x)[0],0)

#=============================================================================================================

with open('secondInngdata.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    secondInngsdata = unpickler.load() 
    file.close()
        
# 2nd innings run prediction model

for i in secondInngsdata:
    for j in list(i['BallbyBall'].keys()):
        if j!=0:
            i['BallbyBall'][j]['required_run_rate']=round(((i['target']-i['BallbyBall'][j]['inningstotal'])/j),2)
        else:
            i['BallbyBall'][j]['required_run_rate']=0
            
regressionDataX=[] #added3006
regressionDataY=[] #added3006

for i in secondInngsdata:
    
    start_date='2008-01-01'
    end_date='2020-12-31'
    if i['date']>datetime.datetime.strptime(end_date, '%Y-%m-%d')\
    or i['date']<datetime.datetime.strptime(start_date, '%Y-%m-%d'):
        continue
        
    ballRem=list(i['BallbyBall'].keys())
    for j in ballRem:

        l=getWickRemCategorical(10-i['BallbyBall'][j]['wickets'])

        l.append(i['BallbyBall'][j]['required_run_rate'])

        l.append(j) #added3006
        regressionDataX.append(l) #added3006
        

        if i['win/loss']=='loss':

            regressionDataY.append(i['BallbyBall'][min(ballRem)]['inningstotal']-i['BallbyBall'][j]['inningstotal']) #added3006
        else:
            predicted=get_OLS_ballByBallDict_catWickRem(ballRem=min(ballRem),wicketRem=10-i['BallbyBall'][min(ballRem)]['wickets'])+\
                        (i['BallbyBall'][min(ballRem)]['inningstotal']-i['BallbyBall'][j]['inningstotal'])

            regressionDataY.append(predicted) #added3006
        

for j in range(120):

    ConstAdded=sm.add_constant(regressionDataX) #added3006
    model = sm.OLS(regressionDataY,ConstAdded) #added3006
   
    fitModel2=model.fit()  #added3006 
    


win_loss_dict={}
win_loss_dict['win']={}
win_loss_dict['loss']={}
winningmatches={}
losingmatches={}
avg_deviation_per_ball={}
for b in range(119,-1,-1):
    win_loss_dict['win'][b]={}
    win_loss_dict['loss'][b]={}
    win_loss_dict['win'][b]['target']=[]
    win_loss_dict['loss'][b]['target']=[]
    win_loss_dict['win'][b]['predicted_run']=[]
    win_loss_dict['loss'][b]['predicted_run']=[]
    win_loss_dict['win'][b]['actual_run']=[]
    win_loss_dict['loss'][b]['actual_run']=[]
    win_loss_dict['win'][b]['deviation_from_target']=[]
    win_loss_dict['loss'][b]['deviation_from_target']=[]
    losingmatches[b]={}
    winningmatches[b]={}
    if b>0:
        avg_deviation_per_ball[b]={}
    


loss_deviation=[]
win_deviation=[]

for i in secondInngsdata:
    start_date='2008-01-01'
    end_date='2020-12-31'
    if i['date']>datetime.datetime.strptime(end_date, '%Y-%m-%d')\
    or i['date']<datetime.datetime.strptime(start_date, '%Y-%m-%d'):
        continue
    ballsrem_inthisinng=list(i['BallbyBall'].keys())
    
    if i['win/loss']=='loss':
        
        run_dev_loss=i['BallbyBall'][min(ballsrem_inthisinng)]['inningstotal']-i['target']
        loss_deviation.append(run_dev_loss)
        
    else:
        run_dev_win=get_OLS_ballByBallDict_catWickRem(ballRem=min(ballsrem_inthisinng),wicketRem=10-i['BallbyBall'][min(ballsrem_inthisinng)]['wickets'])+\
        i['BallbyBall'][min(ballsrem_inthisinng)]['inningstotal']-i['target']
        
        win_deviation.append(run_dev_win)
        
# mu,sig=np.median(loss_deviation+win_deviation),np.std(loss_deviation+win_deviation)
mu1,sig1=np.median(loss_deviation),np.std(loss_deviation)
mu2,sig2=np.median(win_deviation),np.std(win_deviation)
def getWinProb2ndInng_2(run):
    return round((((.5+erf( (run-mu2) / (sig2*sqrt(2)) )/2)+(.5+erf( (run-mu1) / (sig1*sqrt(2)) )/2))/2)*100,2)


import yaml

with open('data_Of_allMatches.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    allmatchInfo = unpickler.load() 
    file.close()

def getTarget(x):
    for i in allmatchInfo:
        if i['matchId']==x:
            for j in range(len(i['innings'])):
                inningstotal=0
                if j <=1:
                    inningsKey=list(i['innings'][j].keys())[0]
                    for balls in i['innings'][j][inningsKey]['deliveries']:
                        balls_key=list(balls.keys())[0]
                        inningstotal+=balls[balls_key]['runs']['total']
                        if inningsKey=='1st innings':
                            if i['innings'][j][inningsKey]['deliveries'].index(balls)==len(i['innings'][j][inningsKey]['deliveries'])-1:
                                target=inningstotal+1
                                
                                return target

matches=[]

for i in allmatchInfo:
    

    matchNo=i['matchId']
    if matchNo=='1136566.yaml':
        continue
    if matchNo=='1136592.yaml':
        continue
    if matchNo=='1178424.yaml':
        continue
    if matchNo=='1237182.yaml':
        continue
        
#     if matchNo!='1082648.yaml':
#         continue

#     print(matchNo,type(matchNo))
    
    if isinstance(i['info']['dates'][0],str):
        thisMatchDate=datetime.datetime.strptime(i['info']['dates'][0], '%Y-%m-%d')
    else:
        thisMatchDate=datetime.datetime.strptime(\
                            datetime.datetime.strftime(i['info']['dates'][0], '%Y-%m-%d'),\
                            '%Y-%m-%d')
    if thisMatchDate.year>=2017 and thisMatchDate.year<=2021:
        thismatchdata={}
        thismatchdata[matchNo]={}
        thismatchdata[matchNo]['date']=thisMatchDate
        
        for j in range(len(i['innings'])):
            ballIndex=120
            inningstotal=0
            wickets=10
            batsmaninthisinng=[]
            if j<=1:
                innings_key=list(i['innings'][j].keys())[0]
                thismatchdata[matchNo][innings_key]={}
                
                balindexChange=False
                for balls in i['innings'][j][innings_key]['deliveries']:
                    thisBallInfo={}
                    balls_key=list(balls.keys())[0]
                    inningstotal+=(balls[balls_key]['runs']['batsman']+balls[balls_key]['runs']['extras'])
                    bowler=balls[balls_key]['bowler']
                    batsman=balls[balls_key]['batsman']
                    thisBallInfo['score']=inningstotal
                    thisBallInfo['batsman']=batsman
                    thisBallInfo['bowler']=bowler
                    if 'wicket' in list(balls[balls_key].keys()):
                        wickets=wickets-1
                        if balls[balls_key]['wicket']['kind']!='runout':
                            thisBallInfo['bowlerwicket']='yes'
                        else:
                            thisBallInfo['bowlerwicket']='no'
                            
                    else:
                        thisBallInfo['bowlerwicket']='no'
                        
                    thisBallInfo['Rem_wicket']=wickets
                    
                    if batsman not in batsmaninthisinng:
                        batsmaninthisinng.append(batsman)
                    thisBallInfo['batting_order_of_batsman']=batsmaninthisinng.index(batsman)+1
                    
                    
                    validBall=True
                    if 'extras' in list(balls[balls_key].keys()):
                        if 'wides' in list(balls[balls_key]['extras'].keys()) or \
                        'noballs' in list(balls[balls_key]['extras'].keys()):
                            if not balindexChange:
                                ballIndex=ballIndex-1
                                thismatchdata[matchNo][innings_key][ballIndex]=[]
                                balindexChange=True
                            validBall=False
                                
                    if validBall:
                        if not balindexChange:
                            ballIndex=ballIndex-1
                            thismatchdata[matchNo][innings_key][ballIndex]=[]
                        balindexChange=False
                        
                        
                        
                        thisBallInfo['thisballrun']=balls[balls_key]['runs']['batsman']
                        thisBallInfo['thisballbatsmanrun']=balls[balls_key]['runs']['batsman']
                        
                        if innings_key=='1st innings':
                            if ballIndex==120:
                                continue
                            thisBallInfo['thisballwinprob_batting']=getWinProb(inningstotal+\
                                                                               get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))
                            thisBallInfo['thisballwinprob_bowling']=100-getWinProb(inningstotal+\
                                                                                       get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))
                    

                        elif innings_key=='2nd innings':
                            if ballIndex==120:
                                continue
                            if ballIndex==0:
                                continue

                            wicketRem=thisBallInfo['Rem_wicket']
                            runRem=getTarget(matchNo)-thisBallInfo['score']

                            l=getWickRemCategorical(wicketRem)
                            l.append(runRem/ballIndex)
                            l.append(ballIndex)
                            l.insert(0,1)
    #                             run=int(fitModel2[ballIndex].predict(l)[0])-runRem
                            run=int(fitModel2.predict(l)[0])-runRem

    #                             print(ballIndex,wicketRem,runRem,getWinProb2ndInng_2(run))
                            thisBallInfo['thisballwinprob_batting']=getWinProb2ndInng_2(run)
                            thisBallInfo['thisballwinprob_bowling']=100-thisBallInfo['thisballwinprob_batting']
    #                   
                        thismatchdata[matchNo][innings_key][ballIndex].append(thisBallInfo)
                        
                    elif 'extras' in list(balls[balls_key].keys()):
                        if 'legbyes' not in list(balls[balls_key]['extras'].keys()) or \
                        'byes' not in list(balls[balls_key]['extras'].keys()):
#                             if ballIndex==120:
#                                 continue
                            
                            thisBallInfo['thisballrun']=(balls[balls_key]['runs']['batsman']+balls[balls_key]['runs']['extras'])
                            
                            thisBallInfo['thisballbatsmanrun']=balls[balls_key]['runs']['batsman']
                                
                                
                            if innings_key=='1st innings':
                                if ballIndex==120:
                                    continue
                                thisBallInfo['thisballwinprob_batting']=getWinProb(inningstotal+\
                                                                                   get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))
                                thisBallInfo['thisballwinprob_bowling']=100-getWinProb(inningstotal+\
                                                                                           get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))

                            elif innings_key=='2nd innings':
                                if ballIndex==120:
                                    continue
                                if ballIndex==0:
                                    continue

                                wicketRem=thisBallInfo['Rem_wicket']
                                runRem=getTarget(matchNo)-thisBallInfo['score']

                                l=getWickRemCategorical(wicketRem)
                                l.append(runRem/ballIndex)
                                l.append(ballIndex)
                                l.insert(0,1)
        #                             run=int(fitModel2[ballIndex].predict(l)[0])-runRem
                                run=int(fitModel2.predict(l)[0])-runRem

        #                             print(ballIndex,wicketRem,runRem,getWinProb2ndInng_2(run))
                                thisBallInfo['thisballwinprob_batting']=getWinProb2ndInng_2(run)
                                thisBallInfo['thisballwinprob_bowling']=100-thisBallInfo['thisballwinprob_batting']
#                   
                            thismatchdata[matchNo][innings_key][ballIndex].append(thisBallInfo)
        
        matches.append(thismatchdata)   
#         break 


batsmenIn2021=[]
for i in matches:
    match_key=list(i.keys())[0]
    thismaatchdate=i[match_key]['date']
#     if thismaatchdate.year==2021:
    for j in list(i[match_key].keys())[1:]:
        ballRem=list(i[match_key][j].keys())
        for balls in ballRem:
            for info in range(len(i[match_key][j][balls])):
                

                if i[match_key][j][balls][info]['batting_order_of_batsman']>=1 and i[match_key][j][balls][info]['batting_order_of_batsman']<=8:
                    batsman=i[match_key][j][balls][info]['batsman']

            if batsman not in batsmenIn2021:
                batsmenIn2021.append(batsman)

    

        
#     break


bowlersIn2021=[]
for i in matches:
    match_key=list(i.keys())[0]
    thismaatchdate=i[match_key]['date']
#     if thismaatchdate.year==2021:
    for j in list(i[match_key].keys())[1:]:
        ballRem=list(i[match_key][j].keys())
        for balls in ballRem:
            for info in range(len(i[match_key][j][balls])):
                bowler=i[match_key][j][balls][info]['bowler']

            if bowler not in bowlersIn2021:
                bowlersIn2021.append(bowler)


def get_matchwise_batsmandata(x):
    batsmandata={}
    for i in matches:
        match_key=list(i.keys())[0]
        if match_key=='1082648.yaml':
            continue
        thismaatchdate=i[match_key]['date']
        
        for j in list(i[match_key].keys())[1:]:
            
            ballRem=list(i[match_key][j].keys())
#             print(ballRem,match_key,j)
            
            middle_over_runs=i[match_key][j][max(24,ballRem[-1])][0]['score']-i[match_key][j][83][0]['score']
            inningsBalls=83-max(24,ballRem[-1])
                       
            for balls in ballRem:
                if balls>83 or balls<24:
                    continue
                for info in range(len(i[match_key][j][balls])):
                    if i[match_key][j][balls][info]['batting_order_of_batsman']>=1 and i[match_key][j][balls][info]['batting_order_of_batsman']<=8:
                        batsman=i[match_key][j][balls][info]['batsman']
                    if batsman==x:  
                        if batsman not in list(batsmandata.keys()):
                            batsmandata[batsman]={}
                            batsmandata[batsman][match_key]={}
                            if info==0:
                                batsmandata[batsman][match_key]['balls']=1
                            else:
                                batsmandata[batsman][match_key]['balls']=0
                            batsmandata[batsman][match_key]['runs']=i[match_key][j][balls][info]['thisballbatsmanrun']
                            batsmandata[batsman][match_key]['ballyball']={}
                            batsmandata[batsman][match_key]['ballyball'][balls]=i[match_key][j][balls][info]['thisballwinprob_batting']

                        else:

                            if match_key not in list(batsmandata[batsman].keys()):
                                batsmandata[batsman][match_key]={}
                                if info==0:
                                    batsmandata[batsman][match_key]['balls']=1
                                else:
                                    batsmandata[batsman][match_key]['balls']=0
                                batsmandata[batsman][match_key]['runs']=i[match_key][j][balls][info]['thisballbatsmanrun']
                                batsmandata[batsman][match_key]['ballyball']={}
                                batsmandata[batsman][match_key]['ballyball'][balls]=i[match_key][j][balls][info]['thisballwinprob_batting']

                            else:
                                if info == 0:
                                    batsmandata[batsman][match_key]['balls']+=1
                                else:
                                    batsmandata[batsman][match_key]['balls']+=0
                                batsmandata[batsman][match_key]['runs']+=i[match_key][j][balls][info]['thisballbatsmanrun']

                                batsmandata[batsman][match_key]['ballyball'][balls]=i[match_key][j][balls][info]['thisballwinprob_batting']

    #                         batsmandata[batsman][match_key]['balls']+=1
    #                         batsmandata[batsman][match_key]['runs']+=i[match_key][j][balls]['thisballbatsmanrun']

    #                         batsmandata[batsman][match_key]['ballyball'][balls]=i[match_key][j][balls]['thisballwinprob_batting']
                        if batsmandata[batsman][match_key]['balls']!=0:          
                            batsmandata[batsman][match_key]['batsman_RPB']=round((batsmandata[batsman][match_key]['runs']/batsmandata[batsman][match_key]['balls']),2)
                        else:
                            batsmandata[batsman][match_key]['batsman_RPB']='NA'

                        if inningsBalls!=0:
                            batsmandata[batsman][match_key]['innings_RPB']=round((middle_over_runs/inningsBalls),2)
                            batsmandata[batsman][match_key]['innings_Runs']=middle_over_runs
                        else:
                            batsmandata[batsman][match_key]['innings_RPB']=0
                            batsmandata[batsman][match_key]['innings_Runs']=0
                        if batsmandata[batsman][match_key]['runs']!=0:
                            batsmandata[batsman][match_key]['relative_runbybatsman']=round((middle_over_runs/batsmandata[batsman][match_key]['runs']),2)
                            if batsmandata[batsman][match_key]['batsman_RPB']!='NA':
                                batsmandata[batsman][match_key]['relative_RPB']=round((batsmandata[batsman][match_key]['innings_RPB']/batsmandata[batsman][match_key]['batsman_RPB']),2)
                            else:
                                batsmandata[batsman][match_key]['relative_RPB']='na'
                                
                                
                        else:
                            batsmandata[batsman][match_key]['relative_runbybatsman']='na'
                            batsmandata[batsman][match_key]['relative_RPB']='na'
                            
    return(batsmandata)
        



batsman_details=[]
for j in batsmenIn2021:
    
    batsman_details.append(get_matchwise_batsmandata(j))
    
# print(batsman_details)


newdata={}
for i in batsman_details:
    if len(list(i.keys()))==0:
        continue
#     print(list(i.keys())) 
    batsman=list(i.keys())[0]
    newdata[batsman]={}
    newdata[batsman]['effective_count']=[]
    newdata[batsman]['inningscount']=0
    for j in list(i[batsman].keys()):
        newdata[batsman]['inningscount']+=1
        for balls in list(i[batsman][j]['ballyball'].keys()):
            if i[batsman][j]['ballyball'][list(i[batsman][j]['ballyball'].keys())[0]]<=30 and\
                    i[batsman][j]['ballyball'][list(i[batsman][j]['ballyball'].keys())[-1]]>=50:
                newdata[batsman]['effective_count'].append(j)
#     print(batsman,len(list(set(newdata[batsman]['effective_count']))),i[batsman][j]['relative_runbybatsman'])


def getbatsman_cum_stat(x):
    for i in batsman_details:
        
        if len(list(i.keys()))==0:
            continue 
        batsman=list(i.keys())[0]
        if batsman==x:
            RPB=[]
            Runcontribution=[]
            effective_count=[]
            innings_count=0
            batsman_career={}
            batsman_career[batsman]={}
            batsman_career[batsman]['balls']=0
            batsman_career[batsman]['runs']=0
            for z in list(i[batsman].keys()):
                innings_count+=1
                batsman_career[batsman]['balls']+=i[batsman][z]['balls']
                batsman_career[batsman]['runs']+=i[batsman][z]['runs']
                if i[batsman][z]['relative_RPB']!='na' and i[batsman][z]['relative_RPB']!=0:
                    RPB.append(1/i[batsman][z]['relative_RPB'])
                else:
                    RPB.append(0)
                if i[batsman][z]['relative_runbybatsman']!='na' and i[batsman][z]['relative_runbybatsman']!=0:
                    Runcontribution.append(1/i[batsman][z]['relative_runbybatsman'])
                else:
                    Runcontribution.append(0)
                for balls in list(i[batsman][z]['ballyball'].keys()):
                    if i[batsman][z]['ballyball'][list(i[batsman][z]['ballyball'].keys())[0]]<=30 and\
                    i[batsman][z]['ballyball'][list(i[batsman][z]['ballyball'].keys())[-1]]>=40:
                        effective_count.append(z)
                        
            batsman_career[batsman]['innings']=innings_count
            batsman_career[batsman]['effective_inng']=len(list(set(effective_count)))
            batsman_career[batsman]['career_strike_rate']=round(((batsman_career[batsman]['runs']/batsman_career[batsman]['balls'])*100),2)
            batsman_career[batsman]['avg_relative_RPB']=np.mean(RPB)
            batsman_career[batsman]['avg_runContribution']=np.mean(Runcontribution)
            batsman_career[batsman]['effective_inng_portion']=round((batsman_career[batsman]['effective_inng']/innings_count),2)
            batsman_career[batsman]['runs_per_innings']=round((batsman_career[batsman]['runs']/innings_count),2)
            
            if batsman_career[batsman]['balls']>=100:
                return batsman_career


batsman_rating_stat=[]
for j in batsmenIn2021:
    if getbatsman_cum_stat(j)!=None:
        batsman_rating_stat.append(getbatsman_cum_stat(j))


totalrating={}
for i in batsman_rating_stat:
    batsman=list(i.keys())[0]
    totalrating[batsman]={}
    totalrating[batsman]['perfrmance_score']=round(((i[batsman]['runs_per_innings']*i[batsman]['career_strike_rate'])/50),2)
    totalrating[batsman]['rel_contri_score']=round((i[batsman]['avg_runContribution']+i[batsman]['avg_relative_RPB']),2)
    totalrating[batsman]['situation_score']=i[batsman]['effective_inng_portion']
    totalrating[batsman]['total_score']=((0.5)*totalrating[batsman]['perfrmance_score']+(0.5)*(totalrating[batsman]['rel_contri_score']+totalrating[batsman]['situation_score']))*(.6)+(0.4)*i[batsman]['innings']


bestbatsmanlist=[]
for i in list(totalrating.keys()):
    bestbatsmanlist.append([i,totalrating[i]['total_score']])
bestbatsmanlist.sort(key=lambda x:x[1], reverse=True )
# from tabulate import tabulate
# print(tabulate(bestbatsmanlist))
# print(len(bestbatsmanlist))

total=[]
for batsman in list(totalrating.keys()):
    total.append([batsman,totalrating[batsman]['total_score']])
    total.sort(key=lambda x:x[1],reverse=True)
L=[total[i*10:(i+1)*10] for i in range(8)]
batsman_Class_Weightage={}
for l in range(len(L)):
    for ll in L[l]:
        batsman_Class_Weightage[ll[0]]={}
        batsman_Class_Weightage[ll[0]]['class']=l+1
        batsman_Class_Weightage[ll[0]]['weightage']=0.5*(.50202**l)

def batsman_class_weightage(batsman_name):
    if batsman_Class_Weightage.get(batsman_name) is not None:
        return [batsman_Class_Weightage[batsman_name]['weightage'],batsman_Class_Weightage[batsman_name]['class']]
    else:
        return [0.5*(.50202**8),9]


def get_weightage_from_category(x):
    return 0.5*(.50202**x)

matchwise_bowlers=[]
for i in matches:
    thismatch_bowlers={}
    matchkeys=list(i.keys())[0]
    if matchkeys=='1082648.yaml':
        continue
    thismatch_bowlers[matchkeys]={}
    for j in list(i[matchkeys].keys())[1:]:
        balls_keys=list(i[matchkeys][j].keys())
#         print(balls_keys,j,matchkeys)
        
#         middle_over_runs=i[matchkeys][j][max(24,ballRem[-1])][0]['score']-i[matchkeys][j][84][0]['score']
        for balls in balls_keys:
            if balls>84 or balls<24:
                continue
            for info in range(len(i[matchkeys][j][balls])):
                batsman=i[matchkeys][j][balls][info]['batsman']
                bowler=i[matchkeys][j][balls][info]['bowler']
                
                if bowler not in list(thismatch_bowlers[matchkeys].keys()):
                    thismatch_bowlers[matchkeys][bowler]={}
                    thismatch_bowlers[matchkeys][bowler]['wkts_types']={}
                    thismatch_bowlers[matchkeys][bowler]['run_types']={}
                    thismatch_bowlers[matchkeys][bowler]['ballbyball']={}
                    thismatch_bowlers[matchkeys][bowler]['balltypes']={}
                    for wt in range(1,10):
                        thismatch_bowlers[matchkeys][bowler]['wkts_types'][wt]=0
                        thismatch_bowlers[matchkeys][bowler]['run_types'][wt]=0
                        thismatch_bowlers[matchkeys][bowler]['balltypes'][wt]=0
                    if info==0:
                        thismatch_bowlers[matchkeys][bowler]['balls']=1
                        thismatch_bowlers[matchkeys][bowler]['balltypes'][batsman_class_weightage(batsman)[1]]=1
                    else:
                        thismatch_bowlers[matchkeys][bowler]['balls']=0
                        thismatch_bowlers[matchkeys][bowler]['balltypes'][batsman_class_weightage(batsman)[1]]=0
                        
                    thismatch_bowlers[matchkeys][bowler]['runs']=i[matchkeys][j][balls][info]['thisballrun']
                    thismatch_bowlers[matchkeys][bowler]['run_types'][batsman_class_weightage(batsman)[1]]= i[matchkeys][j][balls][info]['thisballbatsmanrun']
                    
                    if i[matchkeys][j][balls][info]['bowlerwicket']=='yes':
                        thismatch_bowlers[matchkeys][bowler]['wickets']=1
                        thismatch_bowlers[matchkeys][bowler]['wkts_types'][batsman_class_weightage(batsman)[1]]=1
                    else:
                        thismatch_bowlers[matchkeys][bowler]['wickets']=0
                        
                    if balls!=120:
                        thismatch_bowlers[matchkeys][bowler]['ballbyball'][balls]=i[matchkeys][j][balls][info]['thisballwinprob_bowling']
                        
                else:
                    if info==0:
                        thismatch_bowlers[matchkeys][bowler]['balls']+=1
                        thismatch_bowlers[matchkeys][bowler]['balltypes'][batsman_class_weightage(batsman)[1]]+=1
                    else:
                        thismatch_bowlers[matchkeys][bowler]['balls']+=0
                        thismatch_bowlers[matchkeys][bowler]['balltypes'][batsman_class_weightage(batsman)[1]]+=0
                        
                    thismatch_bowlers[matchkeys][bowler]['runs']+=i[matchkeys][j][balls][info]['thisballrun']
                    thismatch_bowlers[matchkeys][bowler]['run_types'][batsman_class_weightage(batsman)[1]] += i[matchkeys][j][balls][info]['thisballbatsmanrun']
                        
                        
                    if i[matchkeys][j][balls][info]['bowlerwicket']=='yes':
                        thismatch_bowlers[matchkeys][bowler]['wickets']+=1
                        thismatch_bowlers[matchkeys][bowler]['wkts_types'][batsman_class_weightage(batsman)[1]]+=1
                    else:
                        thismatch_bowlers[matchkeys][bowler]['wickets']+=0
                        
                    if balls!=120:
                        thismatch_bowlers[matchkeys][bowler]['ballbyball'][balls]=i[matchkeys][j][balls][info]['thisballwinprob_bowling']
                if thismatch_bowlers[matchkeys][bowler]['balls']!=0:
                    thismatch_bowlers[matchkeys][bowler]['RPB']=round((thismatch_bowlers[matchkeys][bowler]['runs']/thismatch_bowlers[matchkeys][bowler]['balls']),2)

                if thismatch_bowlers[matchkeys][bowler]['wickets']!=0:
                    thismatch_bowlers[matchkeys][bowler]['BPW']=round((thismatch_bowlers[matchkeys][bowler]['balls']/thismatch_bowlers[matchkeys][bowler]['wickets']),2)
                else:
                    thismatch_bowlers[matchkeys][bowler]['BPW']='na'
                    

        
    matchwise_bowlers.append(thismatch_bowlers)
# print(matchwise_players)middle_over_runs=i[match_key][j][max(24,ballRem[-1])][0]['score']-i[match_key][j][84][0]['score']
            

def get_Bowler_BPW_weightedValues(which_match,bowler_name):
    for i in matchwise_bowlers:
        matchwise_balls=[]
        bowlers_with_no_wickets=[]
        matchmarker=list(i.keys())[0]
        if matchmarker!=which_match:
            continue
        for players in list(i[matchmarker].keys()):
            if i[matchmarker][players]['wickets']!=0:
                continue
            matchwise_balls.append(i[matchmarker][players]['balls'])
            bowlers_with_no_wickets.append(players)
        minval=min(matchwise_balls)
        
        ball_diff=[matchwise_balls[j]-minval for j in range(len(matchwise_balls))]
        maxval=max(ball_diff)
        
        for z in bowlers_with_no_wickets:
#             print(bowlers_with_no_wickets)
            if z!=bowler_name:
                continue
            if maxval!=0:
                return 25+round((i[matchmarker][bowler_name]['balls']-minval)/maxval,2)
            else:
                return 25


matchwise_bowlers_rating=[]
for i in matchwise_bowlers:
    matchNo=list(i.keys())[0]
    
    bowlers_RPB=[]
    bowlers_BPW=[]
    perball_situation_change_bowler=[]
    per_bowler_difficulty=[]
    thisMatchRating={}
    thisMatchRating[matchNo]={}
    for z in list(i[matchNo].keys()):
        ballsbybowler=[]
        difficulty_faced=0
        situation_change=0
        if i[matchNo][z]['BPW']=='na':
            bowlers_BPW.append(get_Bowler_BPW_weightedValues(matchNo,z))
        else:
            bowlers_BPW.append(i[matchNo][z]['BPW'])
            
        bowlers_RPB.append(i[matchNo][z]['RPB'])
        for balls in list(i[matchNo][z]['ballbyball'].keys()):
            ballsbybowler.append([balls,i[matchNo][z]['ballbyball'][balls]])
        ballsbybowler_per_over=[ballsbybowler[p*6:(p+1)*6] for p in range(int(len(ballsbybowler)/6))]   
        
        for overs in range(len(ballsbybowler_per_over)):
            for j in range(len(ballsbybowler_per_over[overs])):
                if j ==(len(ballsbybowler_per_over[overs])-1):
                    break
                if ballsbybowler_per_over[overs][j][1]-ballsbybowler_per_over[overs][j+1][1]>=0:
                    situation_change+=(ballsbybowler_per_over[overs][j][1]-ballsbybowler_per_over[overs][j+1][1])*(100-ballsbybowler_per_over[overs][j][1])
                else:
                    situation_change+=(ballsbybowler_per_over[overs][j][1]-ballsbybowler_per_over[overs][j+1][1])*ballsbybowler_per_over[overs][j][1]
                    
        perball_situation_change_bowler.append([round(situation_change/len(ballsbybowler),2),z])
            
        for types in list(i[matchNo][z]['ballbyball'].keys()):
            difficulty_faced+=(i[matchNo][z]['ballbyball'][types]/len(ballsbybowler))*get_weightage_from_category(types)
           
        per_bowler_difficulty.append([difficulty_faced,z])
        
    mean_RPB=np.mean(bowlers_RPB)
    std_RPB=np.std(bowlers_RPB)
    median_RPB=np.median(bowlers_RPB)
    max_RPB=max(bowlers_RPB)
    min_RPB=min(bowlers_RPB)
    
    mean_BPW=np.mean(bowlers_BPW)
    std_BPW=np.std(bowlers_BPW)
    median_BPW=np.median(bowlers_BPW)
    max_BPW=max(bowlers_BPW)
    min_BPW=min(bowlers_BPW)
    
    situation=[u[0] for u in perball_situation_change_bowler]
    mean_situation=np.mean(situation)
    std_situation=np.std(situation)
    median_situation=np.median(situation)
    max_situation=max(situation)
    min_situation=min(situation)
    
    difficult=[d[0] for d in per_bowler_difficulty]
    mean_difficulty=np.mean(difficult)
    std_difficulty=np.std(difficult)
    median_difficulty=np.median(difficult)
    max_difficulty=max(difficult)
    min_difficulty=min(difficult)
    
    for x in list(i[matchNo].keys()):
        thisMatchRating[matchNo][x]={}
        thisMatchRating[matchNo][x]['rpb_rating']=round((i[matchNo][x]['RPB']-median_RPB)/(max_RPB-min_RPB),2)
        if i[matchNo][x]['BPW']=='na':
            
            
            thisMatchRating[matchNo][x]['bpw_rating']=round((get_Bowler_BPW_weightedValues(matchNo,x)-median_BPW)/(max_BPW-min_BPW),2)
            
        else:
            thisMatchRating[matchNo][x]['bpw_rating']=round((i[matchNo][x]['BPW']-median_BPW)/(max_BPW-min_BPW),2)
            
                
        thisMatchRating[matchNo][x]['total_rating_performance']=(-1)*round(thisMatchRating[matchNo][x]['rpb_rating']+thisMatchRating[matchNo][x]['bpw_rating'],2)
        for element in perball_situation_change_bowler:
            if x == element[1]:
                thisMatchRating[matchNo][x]['total_situation_rating']=round((element[0]-median_situation)/(max_situation-min_situation),2)
                
        for chunk in per_bowler_difficulty:
            if x== chunk[1]:
                thisMatchRating[matchNo][x]['total_difficulty_rating']=round((chunk[0]-median_difficulty)/(max_difficulty-min_difficulty),2)
                
        thisMatchRating[matchNo][x]['total_matchrating']=round((.70*thisMatchRating[matchNo][x]['total_rating_performance']+.15*thisMatchRating[matchNo][x]['total_situation_rating']+.15*thisMatchRating[matchNo][x]['total_difficulty_rating'])*.75+0.25,2)
    matchwise_bowlers_rating.append(thisMatchRating)    

players_total_rating={}
for i in matchwise_bowlers_rating:
    match_identifier=list(i.keys())[0]
    for players in list(i[match_identifier].keys()):
        if players not in list(players_total_rating.keys()):
            players_total_rating[players]={}
            players_total_rating[players]['inngs']=1
            players_total_rating[players]['total_rating_performance']=i[match_identifier][players]['total_rating_performance']
            players_total_rating[players]['total_situation_rating']=i[match_identifier][players]['total_situation_rating']
            players_total_rating[players]['total_difficulty_rating']=i[match_identifier][players]['total_difficulty_rating']
            players_total_rating[players]['overall']=i[match_identifier][players]['total_matchrating']
            if i[match_identifier][players]['total_rating_performance']>0:
                players_total_rating[players]['effective_inngs']=1
            else:
                players_total_rating[players]['effective_inngs']=0
        else:
            players_total_rating[players]['inngs']+=1
            players_total_rating[players]['total_rating_performance']+=i[match_identifier][players]['total_rating_performance']
            players_total_rating[players]['total_situation_rating']+=i[match_identifier][players]['total_situation_rating']
            players_total_rating[players]['total_difficulty_rating']+=i[match_identifier][players]['total_difficulty_rating']
            players_total_rating[players]['overall']+=i[match_identifier][players]['total_matchrating']
            if i[match_identifier][players]['total_rating_performance']>0:
                players_total_rating[players]['effective_inngs']+=1
            else:
                players_total_rating[players]['effective_inngs']+=0
# print(players_total_rating)


final={}
display_final=[]
for x in list(players_total_rating.keys()):
    if players_total_rating[x]['inngs']>=8:
        final[x]={}
        final[x]['performance']=round(players_total_rating[x]['total_rating_performance'],2)
        final[x]['situation']=round(players_total_rating[x]['total_situation_rating'],2)
        final[x]['difficulty']=round(players_total_rating[x]['total_difficulty_rating'],2)
        final[x]['consistency']=round(players_total_rating[x]['effective_inngs']/players_total_rating[x]['inngs'],2)
        final[x]['overall']=round(players_total_rating[x]['overall'])
#         print(final[x]['overall'],x)
        final[x]['total']=round(1.5*final[x]['situation']+7.5*final[x]['performance']*final[x]['consistency']+1*final[x]['difficulty']+50,2)
        display_final.append([x,final[x]['total'],players_total_rating[x]['inngs'],final[x]['consistency'],final[x]['performance'],final[x]['situation'],final[x]['difficulty'],final[x]['overall'],(20+round(final[x]['overall']*final[x]['consistency'],1))*10])
display_final.sort(key=lambda x:x[8], reverse=True )
# from tabulate import tabulate
# print(tabulate(display_final))

CSV=[]
for i in display_final:
    CSV.append([i[0],i[8]])
# print(CSV)

import csv
filecursor=open('best_bowlers_middleovers.csv','w',newline='')
csvWriter=csv.writer(filecursor)
csvWriter.writerow(['player','rating'])
for i in CSV:
    csvWriter.writerow([i[0],i[1]])
filecursor.close()