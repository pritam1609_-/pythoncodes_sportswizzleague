# from math import erf, sqrt, inf

import os
import datetime
import numpy as np
import csv
import pickle
from matplotlib import pyplot as plt 
import statistics 
from IPython.core.display import display, HTML
display(HTML("<style>.container { width:90% !important; }</style>"))
import math

# import statsmodels.api as sm
# import scipy

from sklearn.svm import SVR
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler

# import tensorflow as tf
import joblib
import yaml

import warnings
warnings.filterwarnings("ignore")

from iccRating import get_per_ball_info,getMatchData
from performenceRating import getBowlersPerformence, getBowlerRating, getBatsmanPerformence, getBatsmanRating

#===================================================================================================================

def generate_matchwise_csv(input_reqd_dict,filename,bat_or_bowl):
    playersInFile={}
    fileCursor=open(filename+'.csv','r')
    csvReader=csv.reader(fileCursor)
    for row in csvReader:
        playersInFile[row[0]]=float(row[1])
    fileCursor.close()
    
    for i in list(playersInFile.keys()):
        playersInFile[i]-=.1
        
    if bat_or_bowl=='bowl':
        for i in list(input_reqd_dict.keys()):
            if playersInFile.get(i) is None:
                playersInFile[i]=input_reqd_dict[i]['total_matchrating']
            else:
                playersInFile[i]+=input_reqd_dict[i]['total_matchrating']
    else:
        for j in list(input_reqd_dict.keys()):
            if playersInFile.get(j) is None:
                playersInFile[j]=input_reqd_dict[j]['total_rating']
            else:
                playersInFile[j]+=input_reqd_dict[j]['total_rating']
        
    playersNewList=[]
    for i in list(playersInFile.keys()):
        playersNewList.append([i,playersInFile[i]])
    playersNewList.sort(key=lambda x: x[1], reverse=True)
    
    
    fileCursor=open(filename+'.csv','w',newline='')
    csvWriter=csv.writer(fileCursor)
    for info in playersNewList:
        csvWriter.writerow(info)
    fileCursor.close()

#===================================================================================================================

filenames_of_yamlfiles=os.listdir('yamlFiles')
filenames_of_csv_of_all_t20i=os.listdir('csv_of_all_t20i')
processedMatches=[]
with open('processedMatches.csv','r') as csv_file:
    reader=csv.reader(csv_file)
    for row in reader:
        processedMatches.append(row[0])
    csv_file.close()  
    
    
teams_to_analyze=['Australia','Sri Lanka','India','Bangladesh','New Zealand','South Africa','West Indies','Afghanistan','Pakistan','England','Ireland','Zimbabwe','Scotland']
generateCSVlist=[]
addInPprocessedMatches=[]
for fname in  filenames_of_yamlfiles:
    if fname[:-5] not in [f[:-13] for f in filenames_of_csv_of_all_t20i] and fname[:-5] not in processedMatches:
        filedataofThismatch =yaml.safe_load(open('yamlFiles/'+fname,'r'))
        if filedataofThismatch['info']['gender']=='female':
            addInPprocessedMatches.append(fname[:-5])
            continue        
        if len([val for val in filedataofThismatch['info']['teams'] if val in teams_to_analyze])<2:
            addInPprocessedMatches.append(fname[:-5])
            continue
                
                
        if isinstance(filedataofThismatch['info']['dates'][0],str):
            thisMatchDate=datetime.datetime.strptime(filedataofThismatch['info']['dates'][0], '%Y-%m-%d')
        else:
            thisMatchDate=datetime.datetime.strptime(\
                                datetime.datetime.strftime(filedataofThismatch['info']['dates'][0], '%Y-%m-%d'),\
                                '%Y-%m-%d')   
        
        if thisMatchDate.year<2017:
            addInPprocessedMatches.append(fname[:-5])
            continue
        
        generateCSVlist.append([filedataofThismatch,thisMatchDate,fname])
        
filecursor1=open('processedMatches.csv','a',newline='\n')
csvWriter1=csv.writer(filecursor1)
for i in addInPprocessedMatches:
    csvWriter1.writerow([i])
filecursor1.close()
    
    
if len(generateCSVlist)>0:
    generateCSVlist.sort(key=lambda x:x[1])
    for match in generateCSVlist:
        per_ball_info= get_per_ball_info(match[0])

        all_the_headings=['inngs','deliveries','non_striker','bowler','batsman','totalruns','batsman_runs','extra_runs',\
                         'legbyes','byes','wides','noballs','penalty','wicket_kind','fielder1','fielder2','player_out']
        filecursor=open('csv_of_all_t20i/'+match[2].split('.')[0]+'_'+datetime.datetime.strftime(match[1],'%Y%m%d')+'.csv','w',newline='')
        csvWriter=csv.writer(filecursor)
        csvWriter.writerow([val for val in all_the_headings])
        for element in per_ball_info:
            csvWriter.writerow(element)
        filecursor.close()

matchesToProcess=[]
filenames_of_csv_of_all_t20i=os.listdir('csv_of_all_t20i')
for fname in  filenames_of_csv_of_all_t20i:
    if fname[:-13] not in processedMatches:
        matchesToProcess.append([fname,fname.split('.')[0].split('_')[1]])
matchesToProcess.sort(key=lambda x:x[1])        

for fname in matchesToProcess:
    per_ball_info=[]
    with open('csv_of_all_t20i/'+fname[0],'r') as csv_file:
        reader=csv.reader(csv_file)
        lineNo=0
        for row in reader:
            lineNo+=1
            if lineNo==1:
                continue
            thisRow=[]
            for r in range(len(row)):
                try:
                    elem=float(row[r])
                except:
                    elem=row[r]
                thisRow.append(elem)
            per_ball_info.append(thisRow)
        csv_file.close() 


    matchData=getMatchData(per_ball_info)

    bowlerPerfPP=getBowlersPerformence(matchData,119,84)
    if len(list(bowlerPerfPP.keys()))>0:
        bowlerRatingPP=getBowlerRating(bowlerPerfPP,'PP')
        generate_matchwise_csv(bowlerRatingPP,'bowler_PP_rating','bowl')

    bowlerPerfMO=getBowlersPerformence(matchData,83,24)
    if len(list(bowlerPerfMO.keys()))>0:
        bowlerRatingMO=getBowlerRating(bowlerPerfMO,'MO')
        generate_matchwise_csv(bowlerRatingMO,'bowler_MO_rating','bowl')

    bowlerPerfDO=getBowlersPerformence(matchData,23,0)
    if len(list(bowlerPerfDO.keys()))>0:
        bowlerRatingDO=getBowlerRating(bowlerPerfDO,'DO')
        generate_matchwise_csv(bowlerRatingDO,'bowler_DO_rating','bowl')

    bowlerPerfOA=getBowlersPerformence(matchData,119,0)
    if len(list(bowlerPerfOA.keys()))>0:
        bowlerRatingOA=getBowlerRating(bowlerPerfOA,'OA')
        generate_matchwise_csv(bowlerRatingOA,'bowler_OA_rating','bowl')

    batsmanPerfPP=getBatsmanPerformence(matchData,119,84)
    if len(list(batsmanPerfPP.keys()))>0:
        batsmanRatingPP=getBatsmanRating(batsmanPerfPP)
        generate_matchwise_csv(batsmanRatingPP,'batsman_PP_rating','bat')

    batsmanPerfMO=getBatsmanPerformence(matchData,83,24)
    if len(list(batsmanPerfMO.keys()))>0:
        batsmanRatingMO=getBatsmanRating(batsmanPerfMO)
        generate_matchwise_csv(batsmanRatingMO,'batsman_MO_rating','bat')

    batsmanPerfDO=getBatsmanPerformence(matchData,23,0)
    if len(list(batsmanPerfDO.keys()))>0:
        batsmanRatingDO=getBatsmanRating(batsmanPerfDO)
        doBreak=False
        for b in list(batsmanRatingDO.keys()):
            if math.isnan(batsmanRatingDO[b]['total_rating']):
                doBreak=True
        if doBreak:
            break
        generate_matchwise_csv(batsmanRatingDO,'batsman_DO_rating','bat')

    batsmanPerfOA=getBatsmanPerformence(matchData,119,0)
    if len(list(batsmanPerfOA.keys()))>0:
        batsmanRatingOA=getBatsmanRating(batsmanPerfOA)
        generate_matchwise_csv(batsmanRatingOA,'batsman_OA_rating','bat')


    filecursor1=open('processedMatches.csv','a',newline='\n')
    csvWriter1=csv.writer(filecursor1)
    csvWriter1.writerow([fname[0][:-13]])
    filecursor1.close()