import pickle
import joblib
import datetime
import statsmodels.api as sm
import numpy as np
from math import erf, sqrt, inf

# first innings total run prediction
# used for 2nd innings prediction too

ols1stInng=joblib.load('ols1stInngs_iccT20i.z')

def getWickRemCategorical(w):
    x=[0]*11
    x[w]=1
    return x
def get_OLS_ballByBallDict_catWickRem(ballRem=119,wicketRem=10):
    if ballRem<0:
        return None
    x=getWickRemCategorical(wicketRem)
    x.insert(0,1)
    return round(ols1stInng[ballRem].predict(x)[0],0)
    
winLossDistribution1stInngs=joblib.load('winLossDistribution1stInngs_icct20i.z')
winDistribution,lossDistribution=winLossDistribution1stInngs[0],winLossDistribution1stInngs[1]

def getWinProb(run):
    return round((winDistribution.pdf(run)/(winDistribution.pdf(run)+lossDistribution.pdf(run)))*100,1)
#=============================================================================================================

with open('secondInngdata_iccT20i.pkl', 'rb') as file: 
    unpickler = pickle.Unpickler(file)
    secondInngsdata = unpickler.load() 
    file.close()
        
# 2nd innings run prediction model

for i in secondInngsdata:
    for j in list(i['BallbyBall'].keys()):
        if j!=0:
            i['BallbyBall'][j]['required_run_rate']=round(((i['target']-i['BallbyBall'][j]['inningstotal'])/j),2)
        else:
            i['BallbyBall'][j]['required_run_rate']=0
            
regressionDataX=[] #added3006
regressionDataY=[] #added3006

for i in secondInngsdata:
    
    start_date='2008-01-01'
    end_date='2020-12-31'
    if i['date']>datetime.datetime.strptime(end_date, '%Y-%m-%d')\
    or i['date']<datetime.datetime.strptime(start_date, '%Y-%m-%d'):
        continue
        
    ballRem=list(i['BallbyBall'].keys())
    for j in ballRem:

        l=getWickRemCategorical(10-i['BallbyBall'][j]['wickets'])

        l.append(i['BallbyBall'][j]['required_run_rate'])

        l.append(j) #added3006
        regressionDataX.append(l) #added3006
        

        if i['win/loss']=='loss':

            regressionDataY.append(i['BallbyBall'][min(ballRem)]['inningstotal']-i['BallbyBall'][j]['inningstotal']) #added3006
        else:
            predicted=get_OLS_ballByBallDict_catWickRem(ballRem=min(ballRem),wicketRem=10-i['BallbyBall'][min(ballRem)]['wickets'])+\
                        (i['BallbyBall'][min(ballRem)]['inningstotal']-i['BallbyBall'][j]['inningstotal'])

            regressionDataY.append(predicted) #added3006
        

for j in range(120):

    ConstAdded=sm.add_constant(regressionDataX) #added3006
    model = sm.OLS(regressionDataY,ConstAdded) #added3006
   
    fitModel2=model.fit()  #added3006 
    

win_loss_dict={}
win_loss_dict['win']={}
win_loss_dict['loss']={}
winningmatches={}
losingmatches={}
avg_deviation_per_ball={}
for b in range(119,-1,-1):
    win_loss_dict['win'][b]={}
    win_loss_dict['loss'][b]={}
    win_loss_dict['win'][b]['target']=[]
    win_loss_dict['loss'][b]['target']=[]
    win_loss_dict['win'][b]['predicted_run']=[]
    win_loss_dict['loss'][b]['predicted_run']=[]
    win_loss_dict['win'][b]['actual_run']=[]
    win_loss_dict['loss'][b]['actual_run']=[]
    win_loss_dict['win'][b]['deviation_from_target']=[]
    win_loss_dict['loss'][b]['deviation_from_target']=[]
    losingmatches[b]={}
    winningmatches[b]={}
    if b>0:
        avg_deviation_per_ball[b]={}
    


loss_deviation=[]
win_deviation=[]

for i in secondInngsdata:
    start_date='2008-01-01'
    end_date='2020-12-31'
    if i['date']>datetime.datetime.strptime(end_date, '%Y-%m-%d')\
    or i['date']<datetime.datetime.strptime(start_date, '%Y-%m-%d'):
        continue
    ballsrem_inthisinng=list(i['BallbyBall'].keys())
    
    if i['win/loss']=='loss':
        
        run_dev_loss=i['BallbyBall'][min(ballsrem_inthisinng)]['inningstotal']-i['target']
        loss_deviation.append(run_dev_loss)
        
    else:
        run_dev_win=get_OLS_ballByBallDict_catWickRem(ballRem=min(ballsrem_inthisinng),wicketRem=10-i['BallbyBall'][min(ballsrem_inthisinng)]['wickets'])+\
        i['BallbyBall'][min(ballsrem_inthisinng)]['inningstotal']-i['target']
        
        win_deviation.append(run_dev_win)
        
# mu,sig=np.median(loss_deviation+win_deviation),np.std(loss_deviation+win_deviation)
mu1,sig1=np.median(loss_deviation),np.std(loss_deviation)
mu2,sig2=np.median(win_deviation),np.std(win_deviation)
def getWinProb2ndInng_2(run):
    return round((((.5+erf( (run-mu2) / (sig2*sqrt(2)) )/2)+(.5+erf( (run-mu1) / (sig1*sqrt(2)) )/2))/2)*100,2)




def get_per_ball_info(yamldataofThismatch):
    info_perBall=[]
    extras_kind=['legbyes','byes','wides','noballs','penalty']
    for i in range(len(yamldataofThismatch['innings'])):
        if i<=1:
            innings_key=list(yamldataofThismatch['innings'][i].keys())[0]
            team_batting_first=yamldataofThismatch['innings'][0]['1st innings']['team']
            for j in yamldataofThismatch['innings'][i][innings_key]['deliveries']:
                balls_key=list(j.keys())[0]

                if 'extras' not in list(j[balls_key].keys())\
                and 'wicket' not in list(j[balls_key].keys()):
                    info_perBall.append([innings_key,balls_key,j[balls_key]['non_striker'],j[balls_key]['bowler'],\
                                          j[balls_key]['batsman'],j[balls_key]['runs']['total'],j[balls_key]['runs']['batsman'],\
                                          j[balls_key]['runs']['extras'],'na','na','na','na','na','na','na','na','na'])

                elif 'extras' in list(j[balls_key].keys()):
                    extra_kind=list(j[balls_key]['extras'].keys())[0]
                    position_of_thisball_extra=extras_kind.index(extra_kind)
                    value_of_this_ball_extra=j[balls_key]['extras'][extra_kind]
                    L1=[innings_key,balls_key,j[balls_key]['non_striker'],j[balls_key]['bowler'],\
                                          j[balls_key]['batsman'],j[balls_key]['runs']['total'],j[balls_key]['runs']['batsman'],\
                                          j[balls_key]['runs']['extras']]

                    for x in range(len(extras_kind)):
                        if x != position_of_thisball_extra:
                            L1.append('na')
                        else:
                            L1.append(value_of_this_ball_extra)

                    for y in range(0,4):
                        L1.append('na')

                    info_perBall.append(L1)

                elif 'wicket' in list(j[balls_key].keys()):

                    L2=[innings_key,balls_key,j[balls_key]['non_striker'],j[balls_key]['bowler'],\
                                          j[balls_key]['batsman'],j[balls_key]['runs']['total'],j[balls_key]['runs']['batsman'],\
                                          j[balls_key]['runs']['extras'],'na','na','na','na','na',j[balls_key]['wicket']['kind']]

                    if 'fielders' in list(j[balls_key]['wicket'].keys()):
                        if len(j[balls_key]['wicket']['fielders'])==2:
                            for z in range(2):
                                L2.append(j[balls_key]['wicket']['fielders'][i])
                        else:
                            L2.append(j[balls_key]['wicket']['fielders'][0])
                            L2.append('na')
                        L2.append(j[balls_key]['wicket']['player_out'])  

                    else:
                        for z in range(2):
                            L2.append('na')

                        L2.append(j[balls_key]['wicket']['player_out'])



                    info_perBall.append(L2)            

                elif 'extras' in list(j[balls_key].keys())\
                and 'wicket' in list(j[balls_key].keys()):

                    L3=[innings_key,balls_key,j[balls_key]['non_striker'],j[balls_key]['bowler'],\
                          j[balls_key]['batsman'],j[balls_key]['runs']['total'],j[balls_key]['runs']['batsman'],\
                          j[balls_key]['extras']]


                    for x in range(len(extras_kind)):
                        if x == position_of_thisball_extra:
                            L3.append(value_of_this_ball_extra)
                        else:
                            L3.append('na')

                    L3.append(j[balls_key]['wicket']['kind'])

                    if 'fielders' in list(j[balls_key]['wicket'].keys()):
                        if len(j[balls_key]['wicket']['fielders'])==2:
                            for z in range(2):
                                L3.append(j[balls_key]['wicket']['fielders'][i])
                        else:
                            L3.append(j[balls_key]['wicket']['fielders'][0])
                            L3.append('na')
                        L3.append(j[balls_key]['wicket']['player_out'])

                    info_perBall.append(L3)
    return info_perBall

def getMatchData(info_perBall):
    thismatchdata={}
    inningsStart=True
    for b in range(len(info_perBall)):
        if inningsStart:
            ballIndex=120
            inningstotal=0
            wickets=10
            batsmaninthisinng=[]
            innings_key=info_perBall[b][0]
            thismatchdata[innings_key]={}
            inningsStart=False


#         thismatchdata[innings_key][ballIndex]=[]

        thisballinfo={}
        #print(info_perBall[b])
        inningstotal+=info_perBall[b][5]
        bowler=info_perBall[b][3]
        batsman=info_perBall[b][4]
        thisballinfo['score']=inningstotal
        thisballinfo['batsman']=batsman
        thisballinfo['bowler']=bowler


        thisballinfo['bowlerwicket']='no'
        if info_perBall[b][13]!='na':
            wickets=wickets-1
            if info_perBall[b][13]!='runout':
                thisballinfo['bowlerwicket']='yes'


        thisballinfo['Rem_wicket']=wickets
        if batsman not in batsmaninthisinng:
            batsmaninthisinng.append(batsman)
        thisballinfo['batting_order_of_batsman']=batsmaninthisinng.index(batsman)+1

    #                     print(ballIndex)
        validBall=True
        if info_perBall[b][10]!='na' or info_perBall[b][11]!='na':
            validBall=False

        if validBall:
            ballIndex=ballIndex-1
            thismatchdata[innings_key][ballIndex]=[]


            thisballinfo['thisballrun']=info_perBall[b][5]
            thisballinfo['thisballbatsmanrun']=info_perBall[b][6]

            if innings_key=='1st innings':
                if ballIndex!=120 and get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets) is not None:
                    thisballinfo['thisballwinprob_batting']=getWinProb(inningstotal+\
                                                                       get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))
                    thisballinfo['thisballwinprob_bowling']=100-getWinProb(inningstotal+\
                                                                               get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))


            elif innings_key=='2nd innings':
                if ballIndex!=120 and ballIndex!=0:

                    wicketRem=thisballinfo['Rem_wicket']
                    runRem=target-thisballinfo['score']

                    l=getWickRemCategorical(wicketRem)
                    l.append(runRem/ballIndex)
                    l.append(ballIndex)
                    l.insert(0,1)
        #                             run=int(fitModel2[ballIndex].predict(l)[0])-runRem
                    run=int(fitModel2.predict(l)[0])-runRem

        #                             print(ballIndex,wicketRem,runRem,getWinProb2ndInng_2(run))
                    thisballinfo['thisballwinprob_batting']=getWinProb2ndInng_2(run)
                    thisballinfo['thisballwinprob_bowling']=100-thisballinfo['thisballwinprob_batting']
    #                   
            thismatchdata[innings_key][ballIndex].append(thisballinfo)



        else:


                thisballinfo['thisballrun']=(info_perBall[b][6]+info_perBall[b][7])

                thisballinfo['thisballbatsmanrun']=info_perBall[b][6]
                if ballIndex==120:
                    thismatchdata[innings_key][ballIndex]=[]
                    thismatchdata[innings_key][ballIndex].append(thisballinfo)
                else:


                    if innings_key=='1st innings':
                        if ballIndex!=120 and get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets) is not None:
                            thisballinfo['thisballwinprob_batting']=getWinProb(inningstotal+\
                                                                               get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))
                            thisballinfo['thisballwinprob_bowling']=100-getWinProb(inningstotal+\
                                                                                       get_OLS_ballByBallDict_catWickRem(ballRem=ballIndex,wicketRem=wickets))

                    elif innings_key=='2nd innings':
                        if ballIndex!=120 or ballIndex!=0:

                            wicketRem=thisballinfo['Rem_wicket']
                            runRem=target-thisballinfo['score']

                            l=getWickRemCategorical(wicketRem)
                            l.append(runRem/ballIndex)
                            l.append(ballIndex)
                            l.insert(0,1)
        #                             run=int(fitModel2[ballIndex].predict(l)[0])-runRem
                            run=int(fitModel2.predict(l)[0])-runRem

        #                             print(ballIndex,wicketRem,runRem,getWinProb2ndInng_2(run))
                            thisballinfo['thisballwinprob_batting']=getWinProb2ndInng_2(run)
                            thisballinfo['thisballwinprob_bowling']=100-thisballinfo['thisballwinprob_batting']
    #                   
                    thismatchdata[innings_key][ballIndex].append(thisballinfo)
        
        if b<len(info_perBall)-1 and info_perBall[b][0]!=info_perBall[b+1][0]:
            inningsStart=True
            target=inningstotal+1
    return thismatchdata