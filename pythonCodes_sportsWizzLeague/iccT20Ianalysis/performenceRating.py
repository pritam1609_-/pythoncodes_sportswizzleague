import numpy as np
import pickle
import csv
import math
            
total=[]
with open('batsman_OA_rating.csv','r') as csv_file:
    reader=csv.reader(csv_file)
    for row in reader:
        total.append([row[0]])
    csv_file.close()        

L=[total[i*10:(i+1)*10] for i in range(int(len(total)/10))]
batsman_Class_Weightage={}
for l in range(len(L)):
    for ll in L[l]:
        batsman_Class_Weightage[ll]={}
        batsman_Class_Weightage[ll]['class']=l+1
        batsman_Class_Weightage[ll]['weightage']=0.5*(.90202**l)

def batsman_class_weightage(batsman_name):
    if batsman_Class_Weightage.get(batsman_name) is not None:
        return [batsman_Class_Weightage[batsman_name]['weightage'],batsman_Class_Weightage[batsman_name]['class']]
    else:
        return [0.5*(.90202**int(len(total)/10)),int(len(total)/10)+1]

total_bowlers=[]
with open('bowler_OA_rating.csv','r') as csv_file:
    reader=csv.reader(csv_file)
    for row in reader:
        total_bowlers.append([row[0]])
    csv_file.close()
    
L11=[total_bowlers[i*10:(i+1)*10] for i in range(int(len(total_bowlers)/10))]
bowler_Class_Weightage={}
for l in range(len(L11)):
    for ll in L11[l]:
        bowler_Class_Weightage[ll]={}
        bowler_Class_Weightage[ll]['class']=l+1
        bowler_Class_Weightage[ll]['weightage']=0.5*(.90202**l)

def bowler_class_Weightage(bowler_name):
    if bowler_Class_Weightage.get(bowler_name) is not None:
        return [bowler_Class_Weightage[bowler_name]['weightage'],bowler_Class_Weightage[bowler_name]['class']]
    else:
        return [0.5*(.40202**int(len(total_bowlers)/10)),int(len(total_bowlers)/10)+1]

def get_weightage_from_category(x):
    return 0.5*(.90202**x)


def get_weightage_from_category_bowlers(x):
    return 0.5*(.90202**x)

#==============================================================================================================================

def getBowlersPerformence(mData,startBallRem,endBallRem):
    mBowlerDict={}
    for i in list(mData.keys()):
        balls_keys=list(mData[i].keys())

    #     phase_total=mData[i][84][-1]['score']

        for balls in balls_keys:
            if balls<=startBallRem and balls>=endBallRem:
                if i =='2nd innings' and balls==0:
                    continue

                for info in range(len(mData[i][balls])):
                    batsman=mData[i][balls][info]['batsman']
                    bowler=mData[i][balls][info]['bowler']
#                     print(balls,batsman,bowler)

                    if bowler not in list(mBowlerDict.keys()):
#                         print("@@@@")
                        mBowlerDict[bowler]={}
                        mBowlerDict[bowler]['wkts_types']={}
                        mBowlerDict[bowler]['run_types']={}
                        mBowlerDict[bowler]['ballbyball']={}
                        mBowlerDict[bowler]['balltypes']={}
                        mBowlerDict[bowler]['wickets']=0
                        mBowlerDict[bowler]['balls']=0
                        mBowlerDict[bowler]['runs']=0
                        for wt in range(1,int(len(total)/10)+2):
                            mBowlerDict[bowler]['wkts_types'][wt]=0
                            mBowlerDict[bowler]['run_types'][wt]=0
                            mBowlerDict[bowler]['balltypes'][wt]=0



                    if info==0:
                        mBowlerDict[bowler]['balls']+=1

                    mBowlerDict[bowler]['balltypes'][batsman_class_weightage(batsman)[1]]=0

                    mBowlerDict[bowler]['runs']+=mData[i][balls][info]['thisballrun']
                    mBowlerDict[bowler]['run_types'][batsman_class_weightage(batsman)[1]]+=mData[i][balls][info]['thisballbatsmanrun']

#                     print(mData[i][balls][info]['bowlerwicket'])
                    if mData[i][balls][info]['bowlerwicket']=='yes':
#                         print("wwww")
                        mBowlerDict[bowler]['wickets']+=1
                        mBowlerDict[bowler]['wkts_types'][batsman_class_weightage(batsman)[1]]=1

                    if balls!=120:
                        mBowlerDict[bowler]['ballbyball'][balls]=mData[i][balls][info]['thisballwinprob_bowling']



                    if mBowlerDict[bowler]['balls']!=0:
                        mBowlerDict[bowler]['RPB']=round(mBowlerDict[bowler]['runs']/mBowlerDict[bowler]['balls'],2)

                    if mBowlerDict[bowler]['wickets']!=0:
                        mBowlerDict[bowler]['BPW']=round(mBowlerDict[bowler]['balls']/mBowlerDict[bowler]['wickets'],2)
                    else:
                        mBowlerDict[bowler]['BPW']='na'
#     print("----",mBowlerDict)
    return mBowlerDict

def get_Bowler_BPW_weightedValues(bowler_name,inputDict,phase):
    if phase=='MO': val=25
    if phase=='OA': val=25
    if phase=='PP': val=19
    if phase=='DO': val=13
    matchwise_balls=[]
    bowlers_with_no_wickets=[]

    for i in list(inputDict.keys()):
        if inputDict[i]['wickets']!=0:
            continue
        matchwise_balls.append(inputDict[i]['balls'])
        bowlers_with_no_wickets.append(i)
    minval=min(matchwise_balls)

    ball_diff=[matchwise_balls[j]-minval for j in range(len(matchwise_balls))]
    maxval=max(ball_diff)

    for z in bowlers_with_no_wickets:
#             print(bowlers_with_no_wickets)
        if z!=bowler_name:
            continue
        if maxval!=0:
            return val+round((inputDict[i]['balls']-minval)/maxval,2)
        else:
            return val

def getBowlerRating(mBowlerDict,phase) : 
    bowlers_BPW=[]
    bowlers_RPB=[]
    perball_situation_change_bowler=[]
    per_bowler_difficulty=[]
    bowlerRating={}

    for i in list(mBowlerDict.keys()):
#         print(i)
        ballsbybowler=[]
        difficulty_faced=0
        situation_change=0
        if mBowlerDict[i]['BPW']=='na':
            bowlers_BPW.append(get_Bowler_BPW_weightedValues(i,mBowlerDict,phase))
        else:
            bowlers_BPW.append(mBowlerDict[i]['BPW'])

        if mBowlerDict[i].get('RPB') is None:
            mBowlerDict[i]['RPB']=1.25 #check
        bowlers_RPB.append(mBowlerDict[i]['RPB'])
        for balls in list(mBowlerDict[i]['ballbyball'].keys()):
            ballsbybowler.append([balls,mBowlerDict[i]['ballbyball'][balls]])
        ballsbybowler_per_over=[ballsbybowler[p*6:(p+1)*6] for p in range(int(len(ballsbybowler)/6))]   

        for overs in range(len(ballsbybowler_per_over)):
            for j in range(len(ballsbybowler_per_over[overs])):
                if j ==(len(ballsbybowler_per_over[overs])-1):
                    break
                if ballsbybowler_per_over[overs][j][1]-ballsbybowler_per_over[overs][j+1][1]>=0:
                    situation_change+=(ballsbybowler_per_over[overs][j][1]-ballsbybowler_per_over[overs][j+1][1])*(100-ballsbybowler_per_over[overs][j][1])
                else:
                    situation_change+=(ballsbybowler_per_over[overs][j][1]-ballsbybowler_per_over[overs][j+1][1])*ballsbybowler_per_over[overs][j][1]

        perball_situation_change_bowler.append([round(situation_change/len(ballsbybowler),2),i])

        for types in list(mBowlerDict[i]['ballbyball'].keys()):
            difficulty_faced+=(mBowlerDict[i]['ballbyball'][types]/len(ballsbybowler))*get_weightage_from_category(types)

        per_bowler_difficulty.append([difficulty_faced,i])

    mean_RPB=np.mean(bowlers_RPB)
    std_RPB=np.std(bowlers_RPB)
    median_RPB=np.median(bowlers_RPB)
    max_RPB=max(bowlers_RPB)
    min_RPB=min(bowlers_RPB)

    mean_BPW=np.mean(bowlers_BPW)
    std_BPW=np.std(bowlers_BPW)
    median_BPW=np.median(bowlers_BPW)
    max_BPW=max(bowlers_BPW)
    min_BPW=min(bowlers_BPW)

    situation=[u[0] for u in perball_situation_change_bowler]
    mean_situation=np.mean(situation)
    std_situation=np.std(situation)
    median_situation=np.median(situation)
    max_situation=max(situation)
    min_situation=min(situation)

    difficult=[d[0] for d in per_bowler_difficulty]
    mean_difficulty=np.mean(difficult)
    std_difficulty=np.std(difficult)
    median_difficulty=np.median(difficult)
    max_difficulty=max(difficult)
    min_difficulty=min(difficult)

    for x in list(mBowlerDict.keys()):
        bowlerRating[x]={}
        bowlerRating[x]['rpb_rating']=round((mBowlerDict[x]['RPB']-median_RPB)/(max_RPB-min_RPB),2)
        if math.isnan(bowlerRating[x]['rpb_rating']):
            bowlerRating[x]['rpb_rating']=0
            
        if mBowlerDict[x]['BPW']=='na':
            bowlerRating[x]['bpw_rating']=round((get_Bowler_BPW_weightedValues(x,mBowlerDict,phase)-median_BPW)/(max_BPW-min_BPW),2)
        else:
            bowlerRating[x]['bpw_rating']=round((mBowlerDict[x]['BPW']-median_BPW)/(max_BPW-min_BPW),2)
        if math.isnan(bowlerRating[x]['bpw_rating']):
            bowlerRating[x]['bpw_rating']=0
            
        bowlerRating[x]['total_rating_performance']=(-1)*round(bowlerRating[x]['rpb_rating']+bowlerRating[x]['bpw_rating'],2)
        
        
        for element in perball_situation_change_bowler:
            if x == element[1]:
                bowlerRating[x]['total_situation_rating']=round((element[0]-median_situation)/(max_situation-min_situation),2)

        for chunk in per_bowler_difficulty:
            if x== chunk[1]:
                bowlerRating[x]['total_difficulty_rating']=round((chunk[0]-median_difficulty)/(max_difficulty-min_difficulty),2)
                
        if math.isnan(bowlerRating[x]['total_situation_rating']):
            bowlerRating[x]['total_situation_rating']=0
        if math.isnan(bowlerRating[x]['total_difficulty_rating']):
            bowlerRating[x]['total_difficulty_rating']=0

        bowlerRating[x]['total_matchrating']=round(((.90*bowlerRating[x]['total_rating_performance']+.05*bowlerRating[x]['total_situation_rating']+.05*bowlerRating[x]['total_difficulty_rating'])*.75+0.25)*4,2)
    return bowlerRating 

#==============================================================================================================================
def getBatsmanPerformence(mData,startBallRem,endBallRem):
    mBatsmanDict={}
    for i in list(mData.keys()):
        balls_keys=list(mData[i].keys())
        balls_keys.sort(reverse=True)
        if i=='2nd innings' and balls_keys[-1]==0:
            innings_total=mData[i][balls_keys[-2]][-1]['score']
        else:
            innings_total=mData[i][balls_keys[-1]][-1]['score']

        for balls in balls_keys:
            if balls<=startBallRem and balls>=endBallRem:
                if i =='2nd innings' and balls==0:
                    continue
                for info in range(len(mData[i][balls])):
                    batsman=mData[i][balls][info]['batsman']
                    bowler=mData[i][balls][info]['bowler']

                    if batsman not in list(mBatsmanDict.keys()):
                        mBatsmanDict[batsman]={}

                        mBatsmanDict[batsman]['run_types']={}
                        mBatsmanDict[batsman]['ballbyball']={}
                        mBatsmanDict[batsman]['balltypes']={}
                        mBatsmanDict[batsman]['balls']=0
                        mBatsmanDict[batsman]['runs']=0
                        for wt in range(1,int(len(total_bowlers)/10)+2):
                            mBatsmanDict[batsman]['run_types'][wt]=0
                            mBatsmanDict[batsman]['balltypes'][wt]=0



                    if info==0:
                        mBatsmanDict[batsman]['balls']+=1
                        mBatsmanDict[batsman]['balltypes'][bowler_class_Weightage(bowler)[1]]+=1


                    mBatsmanDict[batsman]['runs']+=mData[i][balls][info]['thisballbatsmanrun']
                    mBatsmanDict[batsman]['run_types'][bowler_class_Weightage(bowler)[1]]+= mData[i][balls][info]['thisballbatsmanrun']


                    if balls!=120:
                        mBatsmanDict[batsman]['ballbyball'][balls]=mData[i][balls][info]['thisballwinprob_batting']


                    if mBatsmanDict[batsman]['balls']!=0:
                        mBatsmanDict[batsman]['RPB']=round((mBatsmanDict[batsman]['runs']/mBatsmanDict[batsman]['balls']),2)
                        mBatsmanDict[batsman]['Rel_run']=round(mBatsmanDict[batsman]['runs']/innings_total,2)
                        mBatsmanDict[batsman]['contribution']=round(mBatsmanDict[batsman]['RPB']*mBatsmanDict[batsman]['Rel_run'],2)
        #                 if thismatch_batsmen[matchkeys][batsman]['wickets']!=0:
        #                     thismatch_batsmen[matchkeys][batsman]['BPW']=round((thismatch_batsmen[matchkeys][bowler]['balls']/thismatch_bowlers[matchkeys][bowler]['wickets']),2)
        #                 else:
        #                     thismatch_batsmen[matchkeys][batsman]['BPW']='na'

    return mBatsmanDict

def getBatsmanRating(mBatsmanDict) : 
    batsman_contribution=[]
    perball_situation_change_batsman=[]
    per_batsman_difficulty=[]
    batsmanRating={}
    this_match_player_for_primary_use={}
    for z in list(mBatsmanDict.keys()):
        if mBatsmanDict[z].get('contribution') is None:
            continue
        this_match_player_for_primary_use[z]={}
        ballsbybatsman=[]
        difficulty_faced=0
        situation_change=0
        effective_inng=0
        batsman_contribution.append([mBatsmanDict[z]['contribution'],z])

        for x in list(mBatsmanDict[z]['ballbyball'].keys()):
            ballsbybatsman.append(mBatsmanDict[z]['ballbyball'][x])

    #         print(len(ballsbybatsman),matchNo)
        if len(ballsbybatsman)>1:
            for y in range(len(ballsbybatsman)-1):
    #                 print(y)
                if ballsbybatsman[y+1]-ballsbybatsman[y]>=0:
                    situation_change+=(ballsbybatsman[y+1]-ballsbybatsman[y])*(100-ballsbybatsman[y])
                else:
                    situation_change+=(ballsbybatsman[y+1]-ballsbybatsman[y])*ballsbybatsman[y]

        else:
            situation_change=0


        for t in list(mBatsmanDict[z]['balltypes'].keys()):
            difficulty_faced+=(mBatsmanDict[z]['balltypes'][t]/len(ballsbybatsman))*get_weightage_from_category_bowlers(t)


        if len(ballsbybatsman)>=10:
            if ballsbybatsman[0]<=25 and ballsbybatsman[-1]>=50:
                effective_inng+=1

        this_match_player_for_primary_use[z]['situation']=situation_change/100
        perball_situation_change_batsman.append([situation_change/100,z])

        this_match_player_for_primary_use[z]['difficulty']=difficulty_faced
        per_batsman_difficulty.append([difficulty_faced,z])

    #     this_match_player_for_primary_use[z]['eff_inng']=effective_inng

#     print(batsman_contribution)
    mean_contri=np.mean([val[0] for val in batsman_contribution])
    max_contri=max([val[0] for val in batsman_contribution])
    min_contri=min([val[0] for val in batsman_contribution])



    mean_situation_change=np.mean([val[0] for val in perball_situation_change_batsman])
    max_situation_change=max([val[0] for val in perball_situation_change_batsman])
    min_situation_change=min([val[0] for val in perball_situation_change_batsman])



    mean_difficulty=np.mean([val[0] for val in per_batsman_difficulty])
    max_difficulty=max([val[0] for val in per_batsman_difficulty])
    min_difficulty=min([val[0] for val in perball_situation_change_batsman])

    for batsmen in list(mBatsmanDict.keys()):
        if mBatsmanDict[batsmen].get('contribution') is None:
            continue
        batsmanRating[batsmen]={}
        batsmanRating[batsmen]['performance_score']=round((mBatsmanDict[batsmen]['contribution']-mean_contri)/(max_contri-min_contri),2)
        if math.isnan(batsmanRating[batsmen]['performance_score']):
            batsmanRating[batsmen]['performance_score']=0
        batsmanRating[batsmen]['situation_score']=round((this_match_player_for_primary_use[batsmen]['situation']-mean_situation_change)/(max_situation_change-min_situation_change),2)
        if math.isnan(batsmanRating[batsmen]['situation_score']):
            batsmanRating[batsmen]['situation_score']=0
        batsmanRating[batsmen]['difficulty_score']=round((this_match_player_for_primary_use[batsmen]['difficulty']-mean_difficulty)/(max_difficulty-min_difficulty)*100,2)
        if math.isnan(batsmanRating[batsmen]['difficulty_score']):
            batsmanRating[batsmen]['difficulty_score']=0

        batsmanRating[batsmen]['total_rating']=round(((0.9*batsmanRating[batsmen]['performance_score']+\
                                                                 .05*batsmanRating[batsmen]['situation_score']+.05*batsmanRating[batsmen]['difficulty_score'])*.75+.25)*4,2)
    return batsmanRating